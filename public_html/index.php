<?php
/*
if (strpos($_SERVER['HTTP_HOST'], 'localhost') === TRUE) {
    if (substr($_SERVER['HTTP_HOST'], 0, 4) !== 'www.') {

        $protocol = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
        header('Location: ' . $protocol . 'www.' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);

        exit;
    }
}*/
function endsWith($haystack, $needle)
{
    return $needle === "" || substr($haystack, -strlen($needle)) === $needle;
}

if (endsWith($_SERVER['REQUEST_URI'], '.jpg'))
{
    return false;
}
if (endsWith($_SERVER['REQUEST_URI'], '.png'))
{
    return false;
}
if (endsWith($_SERVER['REQUEST_URI'], '.php'))
{
    return false;
}
if (endsWith($_SERVER['REQUEST_URI'], '.mpo'))
{
    return false;
}
if (endsWith($_SERVER['REQUEST_URI'], '.gif'))
{
    return false;
}
 if (endsWith($_SERVER['REQUEST_URI'], '.js'))
{
    return false;
}
//Om te voorkomen dat bijv. css files niet door Silex geroute worden


$filename = __DIR__.preg_replace('#(\?.*)$#', '', $_SERVER['REQUEST_URI']);
if (php_sapi_name() === 'cli-server' && is_file($filename)) {
    return false;
}
require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'app.php';
$app->run();


        