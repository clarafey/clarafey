/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function multiSelect_GetSelectedItems(multiSelectName,valueField)
{
        var multiselect = $(multiSelectName).data("kendoMultiSelect");
        var dataItem = multiselect.dataItems();
        var kommaSepString = "";
        for (var i in dataItem) 
        {
           kommaSepString = kommaSepString + "," + dataItem[i][valueField];
        } 
        
        return kommaSepString;
  
}
