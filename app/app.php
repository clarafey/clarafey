<?php
require __DIR__ . DIRECTORY_SEPARATOR . 'bootstrap.php';

//Config Data (DB Settings,...)
require __DIR__ . DIRECTORY_SEPARATOR . '../config/config.php';
require __DIR__ . DIRECTORY_SEPARATOR . '../config/configDB.php';

if (RELEASE_MODE=='Y')
{
    error_reporting(E_ALL);
    ini_set('display_errors', 'Off');
}
else
{
     error_reporting(E_ALL);
     ini_set('display_errors', 'On');
}    
$app->before(function ($request) {
    $request->getSession()->start();
});
$app->error(function (\Exception $e, $code) {
	if ($code == 404) {
		return '404 - Not Found! // ' . $e->getMessage();
	} else {
		return 'Er is een fout opgetreden :  ' . $e->getMessage();
	}
});
$app->mount('/', new clarafey\Provider\Controller\indexController());
$app->mount('/registratie/', new clarafey\Provider\Controller\adminLogonController());
$app->mount('/externallogon/', new clarafey\Provider\Controller\externalLogonController());
$app->mount('/smm/gebouwenbeheer/', new clarafey\Provider\Controller\gebouwbeheerLocatieController());
$app->mount('/smm/gebruikersgroepen/', new clarafey\Provider\Controller\gebruikersGroepenController());
$app->mount('/smm/groepenrechten/', new clarafey\Provider\Controller\groepenRechtenController());
$app->mount('/smm/groepenlocaties/', new clarafey\Provider\Controller\groepenLocatiesController());
$app->mount('/smm/functie/', new clarafey\Provider\Controller\functieOverzichtController());
$app->mount('/smm/inventaris/merk/', new clarafey\Provider\Controller\inventarisMerkController());
$app->mount('/smm/inventaris/toestel/', new clarafey\Provider\Controller\inventarisToestelController());
$app->mount('/smm/inventaris/leverancier/', new clarafey\Provider\Controller\inventarisLeverancierController());
$app->mount('/inventaris/', new clarafey\Provider\Controller\inventarisController());
$app->mount('/dashboard/', new clarafey\Provider\Controller\dashboardController());
$app->mount('/kalender/', new clarafey\Provider\Controller\kalenderController());
$app->mount('/info/telefoonboek/', new clarafey\Provider\Controller\telefoonBoekController());
$app->mount('/popup/locatie/', new clarafey\Provider\Controller\popupController());
$app->mount('/registratie/accountgegevens/', new clarafey\Provider\Controller\adminAccountGegevensController());
$app->mount('/registratie/mijnrechten/', new clarafey\Provider\Controller\mijnRechtenController());
$app->mount('/fout/', new clarafey\Provider\Controller\foutController());
$app->mount('/webservice/inventaris/', new clarafey\Provider\Controller\webserviceInventarisController());

$app->mount('/webservice/gebruikersrechten/', new clarafey\Provider\Controller\webserviceGebruikersrechtenController());
$app->mount('/webservice/gebouwenbeheer/', new clarafey\Provider\Controller\webserviceGebouwenbeheerController());
$app->mount('/webservice/werkbonnenbeheer/', new clarafey\Provider\Controller\WebserviceWerkbonnenBeheerController());
$app->mount('/webservice/kalender/', new clarafey\Provider\Controller\webserviceKalenderController());
$app->mount('/werkbonnenbeheer/', new clarafey\Provider\Controller\WerkbonnenBeheerController());
$app->mount('/werkbonnenbeheer/test/', new clarafey\Provider\UnitTests\unitTestWerkbonnenBeheerController());

