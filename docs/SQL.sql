
create table werkopdracht_type
(
  wot_id int IDENTITY(1,1) PRIMARY KEY,
  wot_naam nvarchar(64)

)
insert into werkopdracht_type values ('storing-defect');
insert into werkopdracht_type values ('renovatie');
insert into werkopdracht_type values ('aankoop-investering');

create table werkopdracht
(
  wo_id int IDENTITY(1,1) PRIMARY KEY not null,
  wo_parent_id int,
  wo_titel  nvarchar(255) not null,
  wo_beschrijving nvarchar(max) not null,
  wo_wot_id int not null,
  wo_loc_id int not null,
  wo_wos_id int,
  wo_wos_geb_id int,
  wo_wos_datum datetime,
  wo_woss_id int,
  wo_woss_geb_id int,
  wo_woss_datum datetime,
  wo_wop_id  int not null,
  wo_geb_id_td int,
  wo_begindatum datetime,
  wo_einddatum  datetime,
  wo_creatiedatum datetime not null,
  wo_geb_id_creator int not null,
  wo_modificatiedatum datetime not null,
  wo_geb_id_modificator int not null

)

ALTER TABLE werkopdracht
ADD CONSTRAINT fk_wo_wot_id
FOREIGN KEY (wo_wot_id)
REFERENCES werkopdracht_type(wot_id)

ALTER TABLE werkopdracht
ADD CONSTRAINT fk_wo_geb_id_creator
FOREIGN KEY (wo_geb_id_creator)
REFERENCES gebruiker(geb_id)

ALTER TABLE werkopdracht
ADD CONSTRAINT fk_wo_geb_id_modificator
FOREIGN KEY (wo_geb_id_modificator)
REFERENCES gebruiker(geb_id)

ALTER TABLE werkopdracht
ADD CONSTRAINT fk_wo_loc_id
FOREIGN KEY (wo_loc_id)
REFERENCES locatie(loc_id)





CREATE INDEX ind_wo_wot_id
ON werkopdracht (wo_wot_id)

CREATE INDEX ind_wo_loc_id
ON werkopdracht (wo_loc_id)

create table werkopdracht_status
(
  wos_id int IDENTITY(1,1) PRIMARY KEY,
  wos_naam nvarchar(64)
)

ALTER TABLE werkopdracht
ADD CONSTRAINT fk_wo_wos_id
FOREIGN KEY (wo_wos_id)
REFERENCES werkopdracht_status(wos_id)


CREATE INDEX ind_wo_wos_id
ON werkopdracht (wo_wos_id)


 create table werkopdracht_prioriteit
(
  wop_id int IDENTITY(1,1) PRIMARY KEY,
  wop_naam nvarchar(64)

)
insert into werkopdracht_prioriteit values ('laag');
insert into werkopdracht_prioriteit values ('hoog');
insert into werkopdracht_prioriteit values ('gevaar');


ALTER TABLE werkopdracht
ADD CONSTRAINT fk_wo_wop_id
FOREIGN KEY (wo_wop_id)
REFERENCES werkopdracht_prioriteit(wop_id)


CREATE INDEX ind_wo_wop_id
ON werkopdracht (wo_wop_id)
ALTER TABLE werkopdracht


ADD CONSTRAINT fk_wo_parent_id
FOREIGN KEY (wo_parent_id)
REFERENCES werkopdracht(wo_id)

CREATE INDEX ind_wo_parent_id
ON werkopdracht (wo_parent_id)


insert into werkopdracht_status values ('in afwachting van');
insert into werkopdracht_status values ('materiaal besteld');
insert into werkopdracht_status values ('in uitvoering');
insert into werkopdracht_status values ('afgewerkt');


ALTER TABLE werkopdracht
ADD CONSTRAINT fk_wo_wos_id
FOREIGN KEY (wo_wos_id)
REFERENCES werkopdracht_status(wos_id)


CREATE INDEX ind_wo_wos_id
ON werkopdracht (wo_wos_id)



create table werkopdracht_supstatus
(
  woss_id int IDENTITY(1,1) PRIMARY KEY,
  woss_naam nvarchar(64)
)

insert into werkopdracht_supstatus  values ('in afwachting');
insert into werkopdracht_supstatus  values ('goedgekeurd');
insert into werkopdracht_supstatus  values ('afgewezen');
insert into werkopdracht_supstatus  values ('gesloten');

ALTER TABLE werkopdracht
ADD CONSTRAINT fk_wo_woss_id
FOREIGN KEY (wo_woss_id)
REFERENCES werkopdracht_supstatus(woss_id)


CREATE INDEX ind_wo_woss_id
ON werkopdracht (wo_woss_id)

















create table rechten_functie
(
  func_code nvarchar(3) PRIMARY KEY,
  func_naam nvarchar(64) not null,
  func_opm nvarchar(max)
);

insert into rechten_functie  values ('105','gebouwenbeheer','' ); 
insert into rechten_functie  values ('110','gebruikersgroepen','' ); 
insert into rechten_functie  values ('115','groepenrechten','' ); 
insert into rechten_functie  values ('200','inventaris','' );
insert into rechten_functie  values ('205','inventarisoverzicht','' );
insert into rechten_functie  values ('300','werkonnenheheer','' );
insert into rechten_functie  values ('305','werkaanvragen indienen van type herstelling','' );
insert into rechten_functie  values ('310','werkaanvragen indienen van type renovatie','' );
insert into rechten_functie  values ('315','werkaanvragen indienen van type investering','' );
insert into rechten_functie  values ('320','werkaanvragen indienen van type periodiek onderhoud','');
insert into rechten_functie  values ('325','overzicht mijn werkbonnen (technische dienst)','');
insert into rechten_functie  values ('330','werkaanvragen goedkeuren en toewijzen aan techn. dienst','');

create table rechten_groep
(
  groep_code nvarchar(12) PRIMARY KEY,
  groep_naam nvarchar(64) not null

);

insert into rechten_groep (groep_code,groep_naam) select VSUBDNSCOD,VSUBDNSOMS from orbis.dbo.BPPSUBDNS ;

create table rechten_groepengebruikers
(
  groepgeb_groep_code nvarchar(12) not null,
  groepgeb_geb_id int not null,
  
primary key (groepgeb_geb_id,groepgeb_groep_code)
 
) ;
ALTER TABLE rechten_groepengebruikers
ADD CONSTRAINT fk_groepgeb_groep_code
FOREIGN KEY (groepgeb_groep_code)
REFERENCES rechten_groep(groep_code);
ALTER TABLE rechten_groepengebruikers
ADD CONSTRAINT fk_groepgeb_geb_id
FOREIGN KEY (groepgeb_geb_id)
REFERENCES gebruiker(geb_id);

/*
100	gebouwenbeheer		SMM
105	gebruikersgroepen		SMM
110	groepenrechten		SMM
115	groepenlocaties		SMM
200	Inventaris		inventaris
300	werkaanvragen herstelling		werkonnenheheer
305	werkaanvragen renovatie		werkonnenheheer
310	werkaanvragen investering		werkonnenheheer
315	werkaanvragen indienen periodiek onderhoud		werkonnenheheer
320	overzicht 'mijn werkbonnen' (technische dienst)		werkonnenheheer
325	werkaanvragen goedkeuren en toewijzen aan techn. dienst		werkonnenheheer

*/





create table rechten_groepenfuncties
(
  groepfunc_groep_code nvarchar(12) not null,
  groepfunc_func_code nvarchar(3) not null,
  groepfunc_rights nvarchar(5)
  
primary key (groepfunc_groep_code,groepfunc_func_code)
 
) ;

 ALTER TABLE rechten_groepenfuncties
ADD CONSTRAINT fk_groepfunc_groep_code
FOREIGN KEY (groepfunc_groep_code)
REFERENCES rechten_groep(groep_code);

ALTER TABLE rechten_groepenfuncties
ADD CONSTRAINT fk_groepfunc_func_code
FOREIGN KEY (groepfunc_func_code)
REFERENCES rechten_functie(func_code);




create table rechten_groepenlocaties
(
  groeploc_groep_code nvarchar(12) not null,
  groeploc_loc_id in not null
  
primary key (groeploc_groep_code,groeploc_loc_id)
 
);



 ALTER TABLE rechten_groepenlocaties
ADD CONSTRAINT fk_groeploc_groep_code
FOREIGN KEY (groeploc_groep_code)
REFERENCES rechten_groep(groep_code);

ALTER TABLE rechten_groepenlocaties
ADD CONSTRAINT fk_groeploc_loc_id
FOREIGN KEY (groeploc_loc_id)
REFERENCES locatie(loc_id);


 
 
--update rechten_groepenfuncties set rechten_groepenfuncties.groepfunc_recht = 'schrijven' where groepfunc_groep_code in ('KKINF','KKDIR','KKLAS')
--and groepfunc_func_code = '100';


-- rechten gebruikersgroepen
-- gebouwenbeheer
insert into rechten_groepenfuncties
select groep_code,'100','schrijven' from rechten_groep where groep_code in ('KKINF','KKDIR','KKLAS');
-- gebruikersbeheer
insert into rechten_groepenfuncties
select groep_code,'105','schrijven' from rechten_groep where groep_code in ('KKINF','KKDIR');
-- inventaris SMM
insert into rechten_groepenfuncties
select groep_code,'110','schrijven' from rechten_groep where groep_code in ('KKINF','KKDIR','KKLAS','KKBOE');
insert into rechten_groepenfuncties
select groep_code,'200','schrijven' from rechten_groep where groep_code in ('KKINF','KKDIR','KKLAS','KKBOE');
-- WA
-- WA
insert into rechten_groepenfuncties
select groep_code,'300','schrijven' from rechten_groep where groep_code in ('KKINF','KKDIR','KKLAS');
insert into rechten_groepenfuncties
select groep_code,'305','schrijven' from rechten_groep where groep_code in ('KKINF','KKDIR','KKLAS');
insert into rechten_groepenfuncties
select groep_code,'310','schrijven' from rechten_groep where groep_code in ('KKINF','KKDIR','KKLAS');
insert into rechten_groepenfuncties
select groep_code,'315','schrijven' from rechten_groep where groep_code in ('KKINF','KKDIR','KKLAS');
insert into rechten_groepenfuncties
select groep_code,'320','schrijven' from rechten_groep where groep_code in ('TECHD','KKINF');
insert into rechten_groepenfuncties
select groep_code,'325','schrijven' from rechten_groep where groep_code in ('KKINF','KKDIR','KKLAS');

--kalender
--laptops
insert into rechten_groepenfuncties
select groep_code,'400','schrijven' from rechten_groep where groep_code in ('KKINF','KKDIR','DIRME','KKREC');
--activieteit
insert into rechten_groepenfuncties
select groep_code,'405','schrijven' from rechten_groep where groep_code in ('KKINF','KKDIR','DIRME','COBJ','CODS','COHC','COVD','COVW','KKREC');
--voertuig
insert into rechten_groepenfuncties
select groep_code,'410','schrijven' from rechten_groep where groep_code in ('KKINF','KKDIR','DIRME','COBJ','CODS','COHC','COVD','COVW','KKREC');
--zaal
insert into rechten_groepenfuncties
select groep_code,'415','schrijven' from rechten_groep where groep_code in ('KKINF','KKDIR','DIRME','COBJ','CODS','COHC','COVD','COVW','KKREC');
--extras koffie,broodjes
insert into rechten_groepenfuncties
select groep_code,'420','schrijven' from rechten_groep where groep_code in ('KKINF','KKDIR','DIRME','KKREC');
-- alles wijzigen recht
insert into rechten_groepenfuncties
select groep_code,'425','schrijven' from rechten_groep where groep_code in ('KKINF','KKDIR','DIRME','KKREC');


-- kalender zaal
insert into rechten_groepenfuncties 
select groep_code,'415','schrijven' from rechten_groep where groep_code in ('LFG11');
--werkaanvragen herstelling
insert into rechten_groepenfuncties
select groep_code,'300','schrijven' from rechten_groep where groep_code in ('LFG11');




-- Locatie Campus Kristus Koning
insert into rechten_groepenlocaties values ('KKDIR',98);
insert into rechten_groepenlocaties values ('KKINF',98);
insert into rechten_groepenlocaties values ('KKLAS',98);
insert into rechten_groepenlocaties values ('COBJ',98);
insert into rechten_groepenlocaties values ('CODS',98);
insert into rechten_groepenlocaties values ('COHC',98);
insert into rechten_groepenlocaties values ('COVD',98);
insert into rechten_groepenlocaties values ('COVW',98);
 


-- Locatie PAV 1
insert into rechten_groepenlocaties values ('LFG11',175);
insert into rechten_groepenlocaties values ('LFG12',175);
insert into rechten_groepenlocaties values ('LFG13',175);
insert into rechten_groepenlocaties values ('LFG14',175);

/**/


ALTER TABLE kalender_reservatietype
ADD CONSTRAINT fk_kalrt_kalrtc_code
FOREIGN KEY (kalrt_kalrtc_code)
REFERENCES kalender_reservatietypecat(kalrtc_code)

CREATE INDEX ind_kalrt_kalrtc_code
ON kalender_reservatietype (kalrt_kalrtc_code)
 
 
create table werkopdracht_groepen (
wogroep_wo_id int NOT NULL,
wogroep_groep_code varchar(12) NOT NULL,
wogroep_mode varchar(10) NOT NULL,
CONSTRAINT pk_werkopdracht_groepen PRIMARY KEY (wogroep_wo_id,wogroep_groep_code,wogroep_mode),
CONSTRAINT chk_wogroep_mode CHECK (wogroep_mode in ('SUPERVISOR','GEBRUIKER'))
)

ALTER TABLE werkopdracht_groepen
ADD CONSTRAINT fk_wogroep_groep_code
FOREIGN KEY (wogroep_groep_code)
REFERENCES rechten_groep(groep_code)

ALTER TABLE werkopdracht_groepen
ADD CONSTRAINT fk_wogroep_wo_id
FOREIGN KEY (wogroep_wo_id)
REFERENCES werkopdracht(wo_id)


create table werkopdracht_gebruikers (
wogeb_wo_id int NOT NULL,
wogeb_geb_id int NOT NULL,
wogeb_mode varchar(10) NOT NULL,
CONSTRAINT pk_werkopdracht_gebruikers PRIMARY KEY (wogeb_wo_id,wogeb_geb_id,wogeb_mode),
CONSTRAINT chk_val CHECK (wogeb_mode in ('SUPERVISOR','GEBRUIKER'))
)


ALTER TABLE werkopdracht_gebruikers
ADD CONSTRAINT fk_wogeb_geb_id
FOREIGN KEY (wogeb_geb_id)
REFERENCES gebruiker(geb_id)

ALTER TABLE werkopdracht_gebruikers
ADD CONSTRAINT fk_wogeb_wo_id
FOREIGN KEY (wogeb_wo_id)
REFERENCES werkopdracht(wo_id)


/*Historiek*/
create table  werkopdracht_status_historiek
(
wosh_id int IDENTITY(1,1),
wosh_wo_id int,
wosh_wos_id int,
wosh_datum datetime,
wosh_geb_id int,
wosh_opmerking varchar(max),
CONSTRAINT [pk_wosh_id] PRIMARY KEY 
(
    wosh_id
)

)

ALTER TABLE werkopdracht_status_historiek
ADD CONSTRAINT fk_wosh_wo_id
FOREIGN KEY (wosh_wo_id)
REFERENCES werkopdracht(wo_id)

ALTER TABLE werkopdracht_status_historiek
ADD CONSTRAINT fk_wosh_wos_id
FOREIGN KEY (wosh_wos_id)
REFERENCES werkopdracht_status(wos_id)


ALTER TABLE werkopdracht_status_historiek
ADD CONSTRAINT fk_wosh_geb_id
FOREIGN KEY (wosh_geb_id)
REFERENCES gebruiker (geb_id)


delete from werkopdracht_status_historiek
delete from werkopdracht_groepen
delete from werkopdracht_gebruikers
delete from werkopdracht



ALTER TABLE werkopdracht
ADD CONSTRAINT fk_wo_wosh_id
FOREIGN KEY (wo_wosh_id)
REFERENCES werkopdracht_status_historiek (wosh_id)



CREATE INDEX ind_wo_wosh_id
ON werkopdracht (wo_wosh_id)