<?php

namespace clarafey\Provider\Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Silex\ControllerCollection;
use Silex\Provider\FormServiceProvider;

require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'BLL' . DIRECTORY_SEPARATOR . 'globalData.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . 'mainController.php';

class adminLogonController implements ControllerProviderInterface {

    protected $data;

    function __construct() {
        
    }

    public function connect(Application $app) {
        $controllers = $app['controllers_factory'];
        // Bind sub-routes
        $controllers->get('/', array($this, 'overview'));
        $controllers->post('/', array($this, 'overview'));
        return $controllers;
    }

    public function overview(Application $app) {

        $registrationInst = \clarafey\BLL\gebruikerBLL\registrationClass::getInstance($app);


        $data = \MainData::Process($app);


        $errormessages = array();
        $succesmessages = array();
        $request = $app['request'];
        $btnLogout = $request->request->get('form_login_btn_logout');
        $linkLogout = $request->query->get('dologoff'); 

        //Wanneer gebruiker is ingelogged via external logon (WPF) en probeert daarna een 
        //een andere URL, moet hij terug opnieuw inloggen. 
        //want verschilleden modes hebben verschillende mogelijkheden
        if ($btnLogout == "1" || $linkLogout || ($registrationInst->UserIsLoggedOn() && $registrationInst->getLogonMode()!="normalmode")) {
            \clarafey\BLL\gebruikerBLL\registrationClass::getInstance($app)->doLogOff();
   
        return $app->redirect($app['request']->getBaseUrl() . '/');
        }else
        {
            
            if ($registrationInst->UserIsLoggedOn())  
                        return $app->redirect($app['request']->getBaseUrl() . '/dashboard/');
            
        }
        $fdata = array(
            'personeelsnummer' => '',
            'wachtwoord' => '',
        );
        //csrf_protection uit voor live. Op test werkt deze protectie, niet op live
        $form = $app['form.factory']->createBuilder('form', $fdata, array('csrf_protection' => false))
                ->add('personeelsnummer', 'text', array('label' => 'personeelsnummer',
                    'attr' => array('placeholder' => 'Personeelnummer')))
                ->add('wachtwoord', 'password', array('label' => 'wachtwoord',
                    'attr' => array('placeholder' => 'Wachtwoord')))
                ->getForm();
   
        if ('POST' == $request->getMethod() && $request->request->get('form_login_btn_login') == 1) {
            $form->bind($request);
            if ($form->isValid()) {

                $fdata = $form->getData();
                $registrationInst->doLogon($fdata["personeelsnummer"], $fdata["wachtwoord"],"normalmode");
                $errorMessage = $registrationInst->getErrorMessage();
                if (empty($errorMessage)) {

                    if ($registrationInst->getIsRegistered() == false || $registrationInst->needNewPassword()) {
                        return $app->redirect($app['request']->getBaseUrl() . '/registratie/accountgegevens/');
                    } else if ($registrationInst->UserIsLoggedOn()) {
                        return $app->redirect($app['request']->getBaseUrl() . '/dashboard/');
                    }
                   
                } else {
                    $errormessages[] = $errorMessage;
                }
            }
        }
    
        $data['form'] = $form->createView();
        $data["errormessages"] = $errormessages;
        $data["succesmessages"] = $succesmessages;
        $data["breadcrump"] = null;
        $data["meta_title"] = "inloggen";
        $data["meta_descr"] = "inlog-scherm";
        $data["meta_keywords"] = "inloggen, registreren";
        return $app['twig']->render('adminLogon.twig', $data);
    }

}
