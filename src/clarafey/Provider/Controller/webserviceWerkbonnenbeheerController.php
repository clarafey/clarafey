<?php

namespace clarafey\Provider\Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Silex\ControllerCollection;
use Silex\Provider\FormServiceProvider;

require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'BLL' . DIRECTORY_SEPARATOR . 'inventarisBLL.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'BLL' . DIRECTORY_SEPARATOR . 'gebouwenbeheerBLL.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'BLL' . DIRECTORY_SEPARATOR . 'werkbonnenBeheerBLL.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'BLL' . DIRECTORY_SEPARATOR . 'kalenderBLL.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . 'mainController.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'BLL' . DIRECTORY_SEPARATOR . 'functions.php';

//Handles the index.php(root)
class WebserviceWerkbonnenBeheerController implements ControllerProviderInterface {

    protected $data;

    function __construct() {
        
    }

    public function connect(Application $app) {
        $controllers = $app['controllers_factory'];
        /*         * ********************************** GET ********************************************* */
        // Bind sub-routes  for werkbonnenbeheer
        $controllers->get('/werkaanvraag/', array($this, 'wsWerkAanvraagOverzicht'));
        $controllers->get('/werkaanvraag/type/', array($this, 'wsWerkAanvraagGetTypes'));
        $controllers->get('/werkaanvraag/prioriteit/', array($this, 'wsWerkAanvraagGetPrioriteiten'));
        $controllers->post('/werkopdracht/save/', array($this, 'wsWerkOpdrachtSave'));
        $controllers->post('/werkbon/save/', array($this, 'wsWerkOpdrachtUpdateStatus'));
        $controllers->get('/werkbon/', array($this, 'wsWerkBonOverzicht'));
        $controllers->get('/werkopdracht/status/', array($this, 'wsWerkOpdrachtGetStatussen'));
        return $controllers;
    }

    public function wsWerkAanvraagGetTypes(Application $app) {
        $lijst = \clarafey\BLL\werkbonnenBeheerBLL\WerkbonnenBeheerBLL::getTypes();
        header('Content-type: application/json');
        return json_encode($lijst);
    }

    public function wsWerkAanvraagGetPrioriteiten(Application $app) {
        $lijst = \clarafey\BLL\werkbonnenBeheerBLL\WerkbonnenBeheerBLL::getPrioriteiten();
        header('Content-type: application/json');
        return json_encode($lijst);
    }

    public function wsWerkOpdrachtGetStatussen(Application $app) {
        $request = $app['request'];
        $type = $request->query->get('type');
        //indien mode gelijk is aan 'user', alle statussen ophalen, behalve
        //'nieuw' en 'gelezen'. Deze worden automatisch gezet
        $mode = $request->query->get('mode');
        $lijst = \clarafey\BLL\werkbonnenBeheerBLL\WerkbonnenBeheerBLL::getWerkOpdrachtStatussen($type,$mode);
        header('Content-type: application/json');
        return json_encode($lijst);
    }

    public function wsWerkOpdrachtSave(Application $app) {
        
       
        $request = $app['request'];
        $id = null;
        $idPost = $request->request->get('id');
        if (!empty($idPost))
            $id = $request->request->get('id');
        $parentid = null;
        $parentIdPost = $request->request->get('parentid');

        if (!empty($parentIdPost))
            $parentid = $request->request->get('parentid');
        if ($parentid == null)
            $wo = new \clarafey\BLL\werkbonnenBeheerBLL\Werkaanvraag();
        else
            $wo = new \clarafey\BLL\werkbonnenBeheerBLL\WerkBon();
        $titel = $request->request->get('titel');
        $locatieId = $request->request->get('hiddenLocationId');
        $inventarisPost = $request->request->get('comboBoxInventaris');
        $inventarisId = null;
        
        
    
        if (is_numeric($inventarisPost))
            $inventarisId = $inventarisPost;
        $typeId = $request->request->get('ddType');
        $supervisorStatusId = $request->request->get('ddSupstatus');
        $prioriteitId = $request->request->get('ddPrioriteit');
        $omschrijving = $request->request->get('omschrijving');
        $beginDatum = $request->request->get('dtBegin');
        $eindDatum = $request->request->get('dtEinde');
        
        $wo->setId($id);
        $wo->setTitel($titel);
        $wo->setBeschrijving($omschrijving);
        $locatie = new \clarafey\BLL\gebouwenbeheerBLL\Locatie();
        $locatie->setId($locatieId);
        $wo->setLocatie($locatie);
        $inventaris = new \clarafey\BLL\inventarisBLL\Inventaris();
        $inventaris->setId($inventarisId);
        $wo->setInventaris($inventaris);
        
         
        
        $type = new \clarafey\BLL\werkbonnenBeheerBLL\WerkOprachtType();
        $type->setId($typeId);
        $wo->setType($type);
        $prioriteit = new \clarafey\BLL\werkbonnenBeheerBLL\WerkOprachtPrioriteit();
        $prioriteit->setId($prioriteitId);
        $wo->setPrioriteit($prioriteit);
        $gebruikerId = \clarafey\BLL\gebruikerBLL\registrationClass::getInstance($app)->getId();

 
        if ($wo->getClassType() == "WA") {

            $wo->setBegindatum(null);
            $wo->setEinddatum(null);
             \clarafey\BLL\functions\functions::writeTrace(json_encode($wo));
            $retval = \clarafey\BLL\werkbonnenBeheerBLL\WerkbonnenBeheerBLL::saveWerkAanvraag($gebruikerId, $wo);
               
        }
        if ($wo->getClassType() == "WB") {

            //datums converteren naar PHP DateTime Object.
            //komt binnen als bijvoorbeeld 15/12/2015 of 1/02/2016
            $phpDateStart = \DateTime::createFromFormat("j/m/Y", $beginDatum);
            $phpDateEnd = \DateTime::createFromFormat("j/m/Y", $eindDatum);
            $wo->setBegindatum($phpDateStart);
            $wo->setEinddatum($phpDateEnd);
            $wa = new \clarafey\BLL\werkbonnenBeheerBLL\Werkaanvraag();
            $wa->setId($parentid);
            $wo->setParent($wa);
            $subgroepen = $request->request->get('msTechnDienstGroepenHidden');
            $toegewezenMedewerkers = $request->request->get('msTechnDienstMedewerkersHidden');
 
            $wo->setGebruikersListKommaSep($toegewezenMedewerkers);
            $wo->setGroepenListKommaSep($subgroepen);
            $retval = \clarafey\BLL\werkbonnenBeheerBLL\WerkbonnenBeheerBLL::saveWerkBon($gebruikerId, $wo);
        }
        if (is_string($retval)) {
            $output = 'error : ' . $retval;
        } else {
            $id = $retval;
            $wo->setId($id);
            $output = '{"result":true,"id":' . $id . '}';
        }
        return $output;
    }
    
    //deze functie wijzigt de status van een werkbon
    //Get parameters : 
    //woType : WA of WB
    //woId : id van werkaanvraag of werkbon
    //ddWBStatus : status id van de werkbon
    //ddWAStatus : status id van de werkaanvraag
    //statusOmschrijving : opmerking ingegeven door de persoon die de status wijzigy
    public function wsWerkOpdrachtUpdateStatus(Application $app) {
        
        $request = $app['request'];
        $woType = $request->request->get('woTypeHidden');

        \clarafey\BLL\functions\functions::writeTrace("wotype  : " . $woType);

        $id = null;
        $idPost = $request->request->get('woId');
        if (!empty($idPost))
            $id = $request->request->get('woId');
        \clarafey\BLL\functions\functions::writeTrace("wo id  : " . $id);
        $statusId = null;
        if ($woType == "WB") {
            $wbStatusPost = $request->request->get('ddWBStatus');
            if (!empty($wbStatusPost)) {
                $statusId = $wbStatusPost;
            }
        }
        if ($woType == "WA") {
            $waStatusPost = $request->request->get('ddWAStatus');
            if (!empty($waStatusPost)) {
                $statusId = $waStatusPost;
            }
        }
        $omschrijving = $request->request->get('statusOmschrijving');

 

        $status = new \clarafey\BLL\werkbonnenBeheerBLL\WerkOprachtStatus();
        $gebruiker = new \clarafey\BLL\gebruikerBLL\Gebruiker();
        $gebruiker->setId(\clarafey\BLL\gebruikerBLL\registrationClass::getInstance($app)->getId());


        $status->setId($statusId);
        $statusHistoriekItem = new \clarafey\BLL\werkbonnenBeheerBLL\WerkOprachtStatusHistorie();
        $statusHistoriekItem->setOpmerking($omschrijving);
        $statusHistoriekItem->setWerkOprachtStatus($status);
        $statusHistoriekItem->setGebruiker($gebruiker);
        $statusHistoriekItem->setWerkopdrachtId($id);

        $retval = \clarafey\BLL\werkbonnenBeheerBLL\WerkbonnenBeheerBLL::saveWerkOpdrachtStatus($statusHistoriekItem);

        if (is_string($retval)) {
             $output = 'error : ' . $retval;
        } else {

            $output = '{"result":true,"id":' . $id . '}';
        }
        return $output;
    }

    public function wsWerkAanvraagOverzicht(Application $app) {

        $request = $app['request'];
        $registrationInst = \clarafey\BLL\gebruikerBLL\registrationClass::getInstance($app);
        $groepCode = \clarafey\BLL\gebruikerBLL\registrationClass::getInstance($app)->getGroepFuncties()->getGroep()->getCode();
    
        $locatieId = \clarafey\BLL\gebruikerBLL\registrationClass::getInstance($app)->getGroepLocaties()->getLocaties();
        $list = \clarafey\BLL\werkbonnenBeheerBLL\werkbonnenBeheerBLL::getWerkAanvragen(null, $locatieId, $groepCode, null);
        return json_encode($list);
    }

    public function wsWerkBonOverzicht(Application $app) {
        $request = $app['request'];
        $registrationInst = \clarafey\BLL\gebruikerBLL\registrationClass::getInstance($app);
        $parentid = $request->query->get('parentid');
        $userId = $request->query->get('userId');
        $subGroep = $request->query->get('subGroep');
        $list = \clarafey\BLL\werkbonnenBeheerBLL\werkbonnenBeheerBLL::getWerkBonnen(null, $parentid, null, $userId, $subGroep);
        return json_encode($list);
    }

}
