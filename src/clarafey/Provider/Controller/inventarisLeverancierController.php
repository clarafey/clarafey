<?php

namespace clarafey\Provider\Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Silex\ControllerCollection;
use Silex\Provider\FormServiceProvider;

require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'BLL' . DIRECTORY_SEPARATOR . 'inventarisBLL.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'BLL' . DIRECTORY_SEPARATOR . 'gebouwenbeheerBLL.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . 'mainController.php';


class inventarisLeverancierController implements ControllerProviderInterface {

    protected $data;

    function __construct() {
        
    }
    public function connect(Application $app) {
        $controllers = $app['controllers_factory'];
        $controllers->get('/', array($this, 'overview'));
        return $controllers;
    } 
  

    public function overview(Application $app) {

        $data = \MainData::Process($app);
        if (\clarafey\BLL\gebruikerBLL\registrationClass::getInstance($app)->UserIsLoggedOn() == false)
            return $app->redirect($app['request']->getBaseUrl() . "/inloggen/");
        $request = $app['request'];
        $errormessages = array();
        $succesmessages = array();
      
        $data["errormessages"] = $errormessages;
        $data["succesmessages"] = $succesmessages;
       // $data["breadcrump"] = \clarafey\BO\globalData\breadCrumpManagerClass::getInstance()->GetBreadCrumps("INVENTARIS");
        return $app['twig']->render('inventarisLeverancier.twig', $data);
    } 
}
