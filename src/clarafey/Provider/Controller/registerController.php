<?php

namespace clarafey\Provider\Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Silex\ControllerCollection;
use Silex\Provider\FormServiceProvider;

require_once __DIR__ . DIRECTORY_SEPARATOR . 'mainController.php';

class registerController implements ControllerProviderInterface {

    protected $data;

    /* function __construct($data) {
      $this->data = $data;
      } */

    function __construct() {
        
    }

    public function connect(Application $app) {

        $controllers = $app['controllers_factory'];
        // Bind sub-routes 
        $controllers->get('/', array($this, 'overview'));
        $controllers->post('/', array($this, 'overview'));

        return $controllers;
    }

    public function overview(Application $app) {

        $data = array();
        $request = $app['request'];
        
 


      
       $data["meta_title"]= "silex basis app - Index";
       $data["meta_descr"]="startpagina van de silex basis app";
       $data["meta_keywords"]="php,silex,twig,composer "; 
       $data["meta_author"]="Gert"; 
      
       
       //deze functie gaat de index.twig parsen met gegevens in de $data variabale en de output HTML terugsturen naar de client
       return $app['twig']->render('index.twig', $data);
    }

}
