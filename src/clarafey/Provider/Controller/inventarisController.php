<?php

namespace clarafey\Provider\Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Silex\ControllerCollection;
use Silex\Provider\FormServiceProvider;

require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'BLL' . DIRECTORY_SEPARATOR . 'inventarisBLL.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'BLL' . DIRECTORY_SEPARATOR . 'gebouwenbeheerBLL.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . 'mainController.php';

class inventarisController implements ControllerProviderInterface {

    protected $data;

    function __construct() {
        
    }
    public function connect(Application $app) {
        $controllers = $app['controllers_factory'];
        $controllers->get('/', array($this, 'overview'));
        $controllers->get('/2/', array($this, 'overviewOld'));
        $controllers->get('/editeren/', array($this, 'createInventarisForm'));
        return $controllers;
    } 
    //Popup Inventaris Object
    public function createInventarisForm(Application $app) {

      $data = \MainData::Process($app);
        if (\clarafey\BLL\gebruikerBLL\registrationClass::getInstance($app)->UserIsLoggedOn() == false)
            return $app->redirect($app['request']->getBaseUrl() . "/inloggen/");
        $request = $app['request'];
        $errormessages = array();
        $succesmessages = array();
        $inventarisObj = new \clarafey\BLL\inventarisBLL\Inventaris();
        $aankoopdatum = null;
        $nummer263 = "";
        if ($request->request->get('formBtnInventarisUpdate') == "1") {
            $formdata = $request->request->get('form');
            $nummer263 = $formdata["nummer263"];
        } else if ($request->query->get('nummer263') != null) {
            $nummer263 = $request->query->get('nummer263'); //GET
        }
        if (!empty($nummer263)) {
            $inventarisObj = \clarafey\BLL\inventarisBLL\inventarisBLL::GetInventaris($nummer263);
        }
        $data["errormessages"] = $errormessages;
        $data["succesmessages"] = $succesmessages;
        $data["inventarisObj"] = $inventarisObj;
        return $app['twig']->render('inventarisEditPopup.twig', $data);
    }
    public function overview(Application $app) {

        $data = \MainData::Process($app);
        if (\clarafey\BLL\gebruikerBLL\registrationClass::getInstance($app)->UserIsLoggedOn() == false)
            return $app->redirect($app['request']->getBaseUrl() . "/inloggen/");
        $request = $app['request'];
        $errormessages = array();
        $succesmessages = array();
        $data["jsonLocation"] = "/webservice/";
        $data["errormessages"] = $errormessages;
        $data["succesmessages"] = $succesmessages;
        $data["breadcrump"] = \clarafey\BLL\globalData\breadCrumpManagerClass::getInstance()->GetBreadCrumps("INVENTARIS");
        return $app['twig']->render('inventaris2.twig', $data);
    } 
    //Obsolete in next version
     public function overviewOld(Application $app) {

        $data = \MainData::Process($app);
        if (\clarafey\BLL\gebruikerBLL\registrationClass::getInstance($app)->UserIsLoggedOn() == false)
            return $app->redirect($app['request']->getBaseUrl() . "/inloggen/");
        $request = $app['request'];
        $errormessages = array();
        $succesmessages = array();
        $data["jsonLocation"] = "/webservice/";
        $data["errormessages"] = $errormessages;
        $data["succesmessages"] = $succesmessages;
        $data["breadcrump"] = \clarafey\BLL\globalData\breadCrumpManagerClass::getInstance()->GetBreadCrumps("INVENTARIS");
        return $app['twig']->render('inventaris.twig', $data);
    } 
}
