<?php

namespace clarafey\Provider\Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Silex\ControllerCollection;
use Silex\Provider\FormServiceProvider;

require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'BLL' . DIRECTORY_SEPARATOR . 'globalData.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . 'mainController.php';

class externalLogonController implements ControllerProviderInterface {

    protected $data;

    function __construct() {
        
    }

    public function connect(Application $app) {
        $controllers = $app['controllers_factory'];
        // Bind sub-routes
        $controllers->get('/', array($this, 'doLogon'));
 
        return $controllers;
    }
 public function doLogon(Application $app) {

        $registrationInst = \clarafey\BLL\gebruikerBLL\registrationClass::getInstance($app);

        $request = $app['request'];

        $personeelsnummer = $request->query->get('personeelsnummer');
        $campus = $request->query->get('campus');
        $kaltype = $request->query->get('kaltype');
        if ('GET' == $request->getMethod()) {

            $registrationInst->doLogon($personeelsnummer, "","kalmode");
            $errorMessage = $registrationInst->getErrorMessage();
            if (empty($errorMessage)) {

                if ($registrationInst->getIsRegistered() == false || $registrationInst->needNewPassword()) {
                    return $app->redirect($app['request']->getBaseUrl() . '/registratie/accountgegevens/');
                } else if ($registrationInst->UserIsLoggedOn()) {
                    return $app->redirect($app['request']->getBaseUrl() . '/kalender/reservaties/'.$campus.'/'.$kaltype.'/');
                }
            } else {
                return $errorMessage;
            }
        }
    }

}
