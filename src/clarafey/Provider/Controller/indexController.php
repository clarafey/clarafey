<?php

namespace clarafey\Provider\Controller;

 

use Silex\Application;
use Silex\ControllerProviderInterface;
//use Silex\ControllerCollection;
//suse Silex\Provider\FormServiceProvider;
require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'BLL' . DIRECTORY_SEPARATOR . 'gebruikerBLL.php';
 
 require_once __DIR__ . DIRECTORY_SEPARATOR . 'mainController.php';

//echo \clarafey\DL\registrationDL\TestRegDL();
//echo \clarafey\DL\webserviceDL\webServiceDL::GetCategories();
//Handles the index.php(root)
class indexController implements ControllerProviderInterface {

    protected $data;

    /* function __construct($data) {
      $this->data = $data;
      } */

    function __construct() {
        
    }

    public function connect(Application $app) {
        $controllers = $app['controllers_factory'];

        // Bind sub-routes 
        $controllers->get('/', array($this, 'overview'));
        $controllers->post('/', array($this, 'overview'));
    
        return $controllers;
    }

    public function overview(Application $app) {
        return $app->redirect($app['request']->getBaseUrl() . '/registratie/');
        return "";
      
    }

}
