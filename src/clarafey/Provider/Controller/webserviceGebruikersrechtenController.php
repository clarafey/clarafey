<?php

namespace clarafey\Provider\Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Silex\ControllerCollection;
use Silex\Provider\FormServiceProvider;

require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'BLL' . DIRECTORY_SEPARATOR . 'inventarisBLL.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'BLL' . DIRECTORY_SEPARATOR . 'gebouwenbeheerBLL.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'BLL' . DIRECTORY_SEPARATOR . 'werkbonnenBeheerBLL.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'BLL' . DIRECTORY_SEPARATOR . 'kalenderBLL.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . 'mainController.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'BLL' . DIRECTORY_SEPARATOR . 'functions.php';

//Handles the index.php(root)
class webserviceGebruikersrechtenController implements ControllerProviderInterface {

    protected $data;

    function __construct() {
        
    }

    public function connect(Application $app) {
        $controllers = $app['controllers_factory'];
        /*         * ********************************** GET ********************************************* */

// Bind sub-routes  for SMM
        $controllers->get('/gebruikergroep/', array($this, 'wsGebruikergroepOverzicht'));
        $controllers->post('/gebruikergroep/save/', array($this, 'wsGebruikerSave'));
        $controllers->get('/gebruikersubgroep/', array($this, 'wsGebruikerSubgroepOverzicht'));
        $controllers->get('/groep/', array($this, 'wsGroepen'));
        $controllers->get('/groepFunctie/', array($this, 'wsGroepFuncties'));
        $controllers->get('/groepLocaties/', array($this, 'wsGroepLocaties'));
        $controllers->get('/functie/', array($this, 'wsFunctie'));



        return $controllers;
    }

    public function wsGebruikerSave(Application $app) {


        $request = $app['request'];
        $json = $request->request->get("models");
        $resultArray = json_decode($json, true);


        $geb = new \clarafey\BLL\gebruikerBLL\Gebruiker();
 
        $geb->setId($resultArray[0]["id"]);
        $geb->setVoornaam($resultArray[0]["voornaam"]);
        $geb->setAchternaam($resultArray[0]["achternaam"]);
        $geb->setPersoneelscode($resultArray[0]["personeelscode"]);
        $geb->setFunctieBeschrijving($resultArray[0]["functieBeschrijving"]);
        $geb->setWerkplaats($resultArray[0]["werkplaats"]);
        
   
        
        $geb->setEmail($resultArray[0]["email"]);
        $geb->setTelefoon($resultArray[0]["telefoon"]);
        $geb->setGsm($resultArray[0]["gsm"]);

        
        $groepFunctie = new \clarafey\BLL\gebruikerBLL\GroepFuncties();
        $groep = new \clarafey\BLL\gebruikerBLL\Groep();
        $groep->setCode($resultArray[0]["groepcode"]);
        $groep->setNaam($resultArray[0]["groepnaam"]);
        $groepFunctie->setGroep($groep);
        $geb->setGroepFuncties($groepFunctie);
 
 
        $retval = \clarafey\BLL\gebruikerBLL\gebruikerBLL::SaveGebruikersInfo($geb);

        if (is_string($retval)) {
            return $retval;
        } else {
            return json_encode($geb);
        }
    }

    public function wsGebruikergroepOverzicht(Application $app) {

        $request = $app['request'];
        $groepCode = $request->query->get('groepCode');
        $overzicht = \clarafey\BLL\gebruikerBLL\gebruikerBLL::GetOverzichtGebruikersGroepen($groepCode);
        $json = json_encode($overzicht);
        return $json;
    }

    public function wsGebruikerSubgroepOverzicht(Application $app) {

        $request = $app['request'];
        $groepCode = $request->query->get('groepCode');
        $overzicht = \clarafey\BLL\gebruikerBLL\gebruikerBLL::GetOverzichtGebruikersSubGroepen($groepCode);
        $json = json_encode($overzicht);
        return $json;
    }

    public function wsGroepen(Application $app) {

        $request = $app['request'];
        $overzicht = \clarafey\BLL\gebruikerBLL\gebruikerBLL::GetGroepen();
        $json = json_encode($overzicht);
        return $json;
    }

    //*
    public function wsFunctie(Application $app) {

        $request = $app['request'];
        $overzicht = \clarafey\BLL\gebruikerBLL\gebruikerBLL::GetFuncties();
        $json = json_encode($overzicht);
        return $json;
    }

    //x
    public function wsGroepFuncties(Application $app) {

        $request = $app['request'];
        $groepFuncties = \clarafey\BLL\gebruikerBLL\gebruikerBLL::GetGroepFuncties();
//een custom functie die een 1 dimensionale Json array teruggeeft (1 rij = 1 groep + 1 functie) 
        $s = '[';
        $first = true;
        foreach ($groepFuncties as $gf) {
            if ($first == false)
                $s.= ',';
            $first = false;
            $s.= '{';
            $s.= '"groepcode": "' . $gf->getGroep()->getCode() . '",';
            $s.= '"groepnaam": "' . $gf->getGroep()->getNaam() . '",';
            $functies = $gf->getFuncties();
            foreach ($functies as $f) {
                $s.= '"functiecode": "' . $f->getCode() . '",';
                $s.= '"functienaam": "' . $f->getNaam() . '",';
                $s.= '"functiecat": "' . $f->getCategorie() . '",';
                $s.= '"functierecht": "' . $f->getRecht() . '"';
            }
            $s .= '}';
        }
        $s.= ']';
        return $s;
    }

    //*
    public function wsGroepLocaties(Application $app) {

        $request = $app['request'];
        $groepFuncties = \clarafey\BLL\gebruikerBLL\gebruikerBLL::GetGroepLocaties();
//een custom functie die een 1 dimensionale Json array teruggeeft (1 rij = 1 groep + 1 functie) 
        $s = '[';
        $first = true;
        foreach ($groepFuncties as $gf) {
            if ($first == false)
                $s.= ',';
            $first = false;
            $s.= '{';
            $s.= '"groepcode": "' . $gf->getGroep()->getCode() . '",';
            $s.= '"groepnaam": "' . $gf->getGroep()->getNaam() . '",';
            $locaties = $gf->getLocaties();
            foreach ($locaties as $l) {
                $s.= '"locatienaam": "' . $l->getNaam() . '"';
            }
            $s .= '}';
        }
        $s.= ']';
        return $s;
    }

}
