<?php


namespace clarafey\Provider\Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Silex\ControllerCollection;
use Silex\Provider\FormServiceProvider;

require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'BLL' . DIRECTORY_SEPARATOR . 'globalData.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . 'mainController.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'BLL' . DIRECTORY_SEPARATOR . 'dashboardBLL.php';
//Handles the index.php(root)
class telefoonBoekController implements ControllerProviderInterface {

    protected $data;

    function __construct() {
        
    }

    public function connect(Application $app) {
       $controllers = $app['controllers_factory'];
        // Bind sub-routes 
       $controllers->get('/', array($this, 'overview'));
       $controllers->post('/', array($this, 'overview'));

        return $controllers;
    }

    public function overview(Application $app) {

        $data = \MainData::Process($app);
        //$dashBoardItemList = \clarafey\BO\dashboardBO\dashboardBO::getDashboardItems();
        //$data["dashBoardItemList"] = $dashBoardItemList;
        $errormessages = array();
        $succesmessages = array();
        $request = $app['request'];
        
        return $app['twig']->render('telefoonBoek.twig', $data);
    }

}
