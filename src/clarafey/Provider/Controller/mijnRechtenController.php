<?php

namespace clarafey\Provider\Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Silex\ControllerCollection;
use Silex\Provider\FormServiceProvider;

require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'BLL' . DIRECTORY_SEPARATOR . 'globalData.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . 'mainController.php';

class mijnRechtenController implements ControllerProviderInterface {

    protected $data;

    function __construct() {
        
    }

    public function connect(Application $app) {
        $controllers = $app['controllers_factory'];
        // Bind sub-routes
        $controllers->get('/', array($this, 'overview'));
        $controllers->post('/', array($this, 'overview'));
        return $controllers;
    }

    public function overview(Application $app) {



        $data = \MainData::Process($app);
      // \clarafey\BO\functions\functions::sendMail($app, 'onderwerp', 'body1', 'gert.donath@fracarita.org');
   
        $errormessages = array();
        $succesmessages = array();
        $request = $app['request'];
        $data["breadcrump"] = null;
        return $app['twig']->render('mijnRechten.twig', $data);
    }

}
