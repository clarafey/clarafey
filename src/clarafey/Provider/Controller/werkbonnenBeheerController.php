<?php

namespace clarafey\Provider\Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Silex\ControllerCollection;
use Silex\Provider\FormServiceProvider;

require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'BLL' . DIRECTORY_SEPARATOR . 'inventarisBLL.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'BLL' . DIRECTORY_SEPARATOR . 'gebruikerBLL.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . 'mainController.php';

class WerkbonnenBeheerController implements ControllerProviderInterface {

    protected $data;
    function __construct() {
    }
    public function connect(Application $app) {
        $controllers = $app['controllers_factory'];
        $controllers->get('/', array($this, 'overview'));
        $controllers->get('/mijnwerkbonnen/', array($this, 'mijnWerkBonnen'));
        $controllers->get('/editeren/', array($this, 'createWerkOpdrachtForm'));
        $controllers->get('/werkbon/editeren/', array($this, 'createWerkbonForm'));
        $controllers->get('/werkaanvraag/statushistoriek/', array($this, 'createPopupStatusHistoriek'));
        $controllers->get('/werkbon/statushistoriek/', array($this, 'createPopupStatusHistoriek'));
        return $controllers;
    } 
    //Popup venster Werkbonnenbeheer, zowel Werkaanvraag (WA) als Werkbon (WB)
    public function createWerkbonForm(Application $app) {
        
        $data = \MainData::Process($app);
        if (\clarafey\BLL\gebruikerBLL\registrationClass::getInstance($app)->UserIsLoggedOn() == false)
            return $app->redirect($app['request']->getBaseUrl() . "/inloggen/");
        $request = $app['request'];
        $errormessages = array();
        $succesmessages = array();
 
        return $app['twig']->render('werkOpdrachtPopupSaveStatus.twig', $data);
        
    }
     public function createWerkOpdrachtForm(Application $app) {
        
        $data = \MainData::Process($app);
        if (\clarafey\BLL\gebruikerBLL\registrationClass::getInstance($app)->UserIsLoggedOn() == false)
            return $app->redirect($app['request']->getBaseUrl() . "/inloggen/");
        $request = $app['request'];
        $errormessages = array();
        $succesmessages = array();
        $data["woObj"] =new \clarafey\BLL\werkbonnenBeheerBLL\Werkaanvraag();
        $data["treeJson"] =  \clarafey\BLL\gebouwenbeheerBLL\gebouwenbeheerBLL::GetLocatieTreeJsonNested();
        return $app['twig']->render('werkOpdrachtPopupSave.twig', $data);
        
    }
    public function overview(Application $app) {

        $data = \MainData::Process($app);
        if (\clarafey\BLL\gebruikerBLL\registrationClass::getInstance($app)->UserIsLoggedOn() == false)
            return $app->redirect($app['request']->getBaseUrl() . "/inloggen/");
        $request = $app['request'];
        $errormessages = array();
        $succesmessages = array();
        $data["errormessages"] = $errormessages;
        $data["succesmessages"] = $succesmessages;
        return $app['twig']->render('mijnWerkAanvragen.twig', $data); 
    } 
      public function mijnWerkBonnen(Application $app) {

        $data = \MainData::Process($app);
        if (\clarafey\BLL\gebruikerBLL\registrationClass::getInstance($app)->UserIsLoggedOn() == false)
            return $app->redirect($app['request']->getBaseUrl() . "/inloggen/");
        $request = $app['request'];
        $errormessages = array();
        $succesmessages = array();
        $data["errormessages"] = $errormessages;
        $data["succesmessages"] = $succesmessages;
        return $app['twig']->render('mijnWerkbonnen.twig', $data); 
    } 
    
     public function createPopupStatusHistoriek (Application $app) {
        
        $request = $app['request'];
        //de Id van de werkopdracht ophalen uit de GET bag
        $woId = $request->query->get('woId');
        $lijst = \clarafey\BLL\werkbonnenBeheerBLL\WerkbonnenBeheerBLL::getWerkopdrachtStatusHistoriek($woId);
        $data["statusHistoriekLijst"]=$lijst;
        
        return $app['twig']->render('werkopdrachtStatusHistoriek.twig', $data); 
    }
    
}
