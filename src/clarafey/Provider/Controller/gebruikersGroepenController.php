<?php

namespace clarafey\Provider\Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Silex\ControllerCollection;
use Silex\Provider\FormServiceProvider;

require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'BLL' . DIRECTORY_SEPARATOR . 'gebouwenbeheerBLL.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . 'mainController.php';

class gebruikersGroepenController implements ControllerProviderInterface {

    protected $data;

    /* function __construct($data) {
      $this->data = $data;
      } */

    function __construct() {
        
    }

    public function connect(Application $app) {
        $controllers = $app['controllers_factory'];

        // Bind sub-routes 
        $controllers->get('/', array($this, 'overview'));
        $controllers->post('/', array($this, 'overview'));

        return $controllers;
    }

    public function overview(Application $app) {
        
        $data = \MainData::Process($app);
        if (\clarafey\BLL\gebruikerBLL\registrationClass::getInstance($app)->UserIsLoggedOn() == false)
            return $app->redirect($app['request']->getBaseUrl() . "/inloggen/");
        $request = $app['request'];
        $errormessages = array();
        $succesmessages = array();
        return $app['twig']->render('gebruikersGroepen.twig', $data);
    }

}
