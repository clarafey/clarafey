<?php

namespace clarafey\Provider\Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Silex\ControllerCollection;
use Silex\Provider\FormServiceProvider;

require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'BLL' . DIRECTORY_SEPARATOR . 'inventarisBLL.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'BLL' . DIRECTORY_SEPARATOR . 'gebouwenbeheerBLL.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'BLL' . DIRECTORY_SEPARATOR . 'werkbonnenBeheerBLL.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'BLL' . DIRECTORY_SEPARATOR . 'kalenderBLL.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . 'mainController.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'BLL' . DIRECTORY_SEPARATOR . 'functions.php';

//Handles the index.php(root)
class webserviceKalenderController implements ControllerProviderInterface {

    protected $data;

    function __construct() {
        
    }

    public function connect(Application $app) {
        $controllers = $app['controllers_factory'];
        $controllers->get('/reservatie/{campus}/{categorie}/', array($this, 'wsGetKalenderInfo'));
        $controllers->get('/reservatietype/{campus}/{categorie}/', array($this, 'wsGetReservatieTypes'));
        $controllers->get('/reservatietypeextras/{categorie}/', array($this, 'wsGetReservatieTypesExtras'));
        $controllers->get('/reservatietypeextrasadmin/{categorie}/', array($this, 'wsGetReservatieTypesExtrasAdmin'));
        $controllers->post('/save/', array($this, 'wsSaveKalender'));
        $controllers->post('/delete/', array($this, 'wsDeleteKalender'));



        return $controllers;
    }

    public function wsGetKalenderInfo(Application $app, $campus, $categorie) {
        $gebruikerId = null;
        if (strtolower($categorie) == "mijnkalender")
            $gebruikerId = \clarafey\BLL\gebruikerBLL\registrationClass::getInstance($app)->getId();
        $list = \clarafey\BLL\kalenderBLL\kalenderBLL::GetKalenderReservaties($campus,$categorie, $gebruikerId);
        return json_encode($list);
    }

    public function wsDeleteKalender(Application $app) {
        $request = $app['request'];
        $json = $request->request->get("models");
        $resultArray = json_decode($json, true);
        $id = $resultArray[0]["TaskID"];
        $retval = \clarafey\BLL\kalenderBLL\kalenderBLL::DeleteKalenderReservatie($id);
        return true;
    }

    public function wsSaveKalender(Application $app) {

        $request = $app['request'];
        $json = $request->request->get("models");
        $resultArray = json_decode($json, true);
        $reservatie = new \clarafey\BLL\kalenderBLL\cKalenderReservatie();
        $reservatie->setId($resultArray[0]["TaskID"]);
        $reservatie->setRecurrenceException($resultArray[0]["RecurrenceException"]);
        if (!empty($resultArray[0]["RecurrenceId"]))
            $reservatie->setRecurrenceId($resultArray[0]["RecurrenceId"]);
        else
            $reservatie->setRecurrenceId(null);

        $reservatieType = new \clarafey\BLL\kalenderBLL\cKalenderReservationType();
        $reservatieType->setId($resultArray[0]["TypeID"]);
        $reservatie->setType($reservatieType);

        $reservatie->setTitel($resultArray[0]["Title"]);
        $reservatie->setDescription($resultArray[0]["Description"]);
        $phpDateStart = \clarafey\BLL\functions\functions::JavaDateTimeToPHP($resultArray[0]["Start"]);
        $phpDateEnd = \clarafey\BLL\functions\functions::JavaDateTimeToPHP($resultArray[0]["End"]);
        $reservatie->setStartDate($phpDateStart);
        $reservatie->setEndDate($phpDateEnd);
        $reservatie->setRecurrenceRule($resultArray[0]["RecurrenceRule"]);
        $reservatie->setIsAllDay($resultArray[0]["IsAllDay"]);
        //de optie 'hele dag' schakelen we uit. De gebruiker moet reserveren van 8:00 tot 24:00
        //$reservatie->setIsAllDay(0);
        //gaat het om een nieuw kalender-item gaan we de loggedOn-gebruiker invullen als creator
        $gebruikerId = \clarafey\BLL\gebruikerBLL\registrationClass::getInstance($app)->getId();
        $geb = new \clarafey\BLL\gebruikerBLL\Gebruiker();
        $geb->setId($gebruikerId);
        $geb->setAchternaam(\clarafey\BLL\gebruikerBLL\registrationClass::getInstance($app)->getAchternaam());
        $geb->setVoornaam(\clarafey\BLL\gebruikerBLL\registrationClass::getInstance($app)->getVoornaam());
        $reservatie->setCreationDate(new \DateTime());


        if (sizeof($resultArray[0]["Extras"]) > 0)
            $extraString = implode(",", array_filter(array_unique($resultArray[0]["Extras"]), 'strlen'));
        else
            $extraString = "";
        $reservatie->setExtras($extraString);

        if (sizeof($resultArray[0]["ExtrasDirectie"]) > 0)
            $extraAdminString = implode(",", array_filter(array_unique($resultArray[0]["ExtrasDirectie"]), 'strlen'));
        else
            $extraAdminString = "";
        $reservatie->setExtrasDirectie($extraAdminString);
        $reservatie->setAttendees($resultArray[0]["Attendees"]);
        $retval = \clarafey\BLL\kalenderBLL\kalenderBLL::uiKalenderReservatie($geb, $reservatie);
        if (is_string($retval)) {

            return $retval;
        } else {

            return json_encode($retval);
        }
    }

    public function wsGetReservatieTypes(Application $app,$campus, $categorie) {

        $lijst = \clarafey\BLL\kalenderBLL\kalenderBLL::GetReservatieTypes($campus,$categorie);
        return json_encode($lijst);
    }

    public function wsGetReservatieTypesExtras(Application $app, $categorie) {

        $lijst = \clarafey\BLL\kalenderBLL\kalenderBLL::GetReservatieTypesExtras($categorie, false);
        return json_encode($lijst);
    }

    public function wsGetReservatieTypesExtrasAdmin(Application $app, $categorie) {

        $lijst = \clarafey\BLL\kalenderBLL\kalenderBLL::GetReservatieTypesExtras($categorie, true);
        return json_encode($lijst);
    }

   

    

}
