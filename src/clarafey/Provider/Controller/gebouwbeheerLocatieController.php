<?php

namespace clarafey\Provider\Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Silex\ControllerCollection;
use Silex\Provider\FormServiceProvider;

require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'BLL' . DIRECTORY_SEPARATOR . 'gebouwenbeheerBLL.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . 'mainController.php';

class gebouwbeheerLocatieController implements ControllerProviderInterface {

    protected $data;

    /* function __construct($data) {
      $this->data = $data;
      } */

    function __construct() {
        
    }

    public function connect(Application $app) {
        $controllers = $app['controllers_factory'];

        // Bind sub-routes 
        $controllers->get('/', array($this, 'overview'));
        $controllers->post('/', array($this, 'overview'));

        return $controllers;
    }

    public function overview(Application $app) {

        $data = \MainData::Process($app);
        if (\clarafey\BLL\gebruikerBLL\registrationClass::getInstance($app)->UserIsLoggedOn() == false)
            return $app->redirect($app['request']->getBaseUrl() . "/inloggen/");
        $request = $app['request'];
        $errormessages = array();
        $succesmessages = array();
        $id = null;
        $naam = null;
        $opmerking = null;
        $beschrijving = null;
        $data['showForm'] = false;
        $postId = $request->request->get('id');
        if (!empty($postId)) {
            $id = $postId;
        } else if ($request->query->get('id') != null) {
            //via GET
            $id = $request->query->get('id');
        }
        $parentid = "";
        $postParentId = $request->request->get('parentid');
        if (!empty($postParentId)) {
            $parentid = $postParentId;
            $data['showForm'] = true;
        } else if ($request->query->get('parentid') != null) {
            $parentid = $request->query->get('parentid');
            $data['showForm'] = true;
        }
         $mode = "";
        $postMode = $request->request->get('mode');
        if (!empty($postMode)) {
            $mode = $postMode;
            
        } else if ($request->query->get('mode') != null) {
            $mode = $request->query->get('mode');
           
        }
        
        
           
        
        
        $data['locationParentDescr'] = "";
        if (!empty($parentid) && $parentid !=-1) {
            $locatieParent = \clarafey\BLL\gebouwenbeheerBLL\gebouwenbeheerBLL::GetLocatie($parentid);
           
        }
        if (!empty($id)) {
            $data['showForm'] = true;
             if ($mode=="delete") {
                
                 $data['showForm'] = false;
                $result = \clarafey\BLL\gebouwenbeheerBLL\gebouwenbeheerBLL::deleteLocatie($id);
                if ($result === true) {
                    $id = null;
                    $succesmessages[] = "locatie is verwijderd";
                    $data['showForm'] = false;
                } else {

                    $errormessages[] = "locatie kon niet verwijderd worden. Waarschijnlijk is deze locatie in gebruik.";
                }
            } else {}
          if ($mode=="update")
          {
               
               $locatie = \clarafey\BLL\gebouwenbeheerBLL\gebouwenbeheerBLL::GetLocatie($id);
                if (empty($locatie))
                    exit('fout,geen locatie gevonden met Id ' . $id);
                $id = $locatie[0]["loc_id"];
                $naam = $locatie[0]["loc_naam"];
                $parentid = $locatie[0]["loc_parent_id"];
                $beschrijving = $locatie[0]["loc_beschrijving"];
                $opmerking = $locatie[0]["loc_opmerking"];
                $data['locationParentDescr'] = $locatie[0]["parentnaam"];
                
                
            }
        }
        $fdata = array(
            'naam' => $naam,
            'beschrijving' => $beschrijving,
            'opmerking' => $opmerking,
        );
        $form = $app['form.factory']->createBuilder('form', $fdata, array('csrf_protection' => false))
                ->add('naam', 'text', array('label' => 'naam/nummer',
                    'attr' => array('size' => '80', 'placeholder' => 'naam/nummer van de locatie')))
                ->add('beschrijving', 'text', array('required' => false, 'label' => 'beschrijving',
                    'attr' => array('size' => '80', 'placeholder' => 'beschrijving van de locatie')))
                ->add('opmerking', 'textarea', array('required' => false, 'label' => 'opmerking',
                    'attr' => array('cols' => '80', 'rows' => '3', 'maxlength' => '1012', 'placeholder' => 'opmerking')))
                ->getForm();

        $form->handleRequest($request);
        if ($form->isValid() && $request->request->get('formBtnLocationAdd') == "1") {
            $fdata = $form->getData();
            
            if ($mode=="insert")
            {
                $id = null;
                //campus toevoegen
                 if ($parentid == "-1")
                     $parentid = null;
            }
            
          
            $id = \clarafey\BLL\gebouwenbeheerBLL\gebouwenbeheerBLL::iuLocatie($id, $parentid, $fdata["naam"], $fdata["beschrijving"], $fdata["opmerking"]);
            if ($mode == "insert") {
                $succesmessages[] = "de nieuwe locatie " . $fdata["naam"] . " is toegevoegd";
            } else {
                $succesmessages[] = "locatiegegevens zijn gewijzigd";
            }
            $data['showForm'] = false;
        }
       $data["formTitle"]="";
       if ($mode=="update") 
       {
           $data["formTitle"] = "locatie wijzigen";
           if ($mode=="update") 
           {
               if (empty($parentid) || $parentid==-1)
                    $data["formTitle"] = "campus wijzigen";
           }
                   
       }
       if ($mode=="insert") 
       {   
 
           if (empty($parentid) || $parentid==-1)
               $data["formTitle"] = "campus toevoegen";     
           else
                     $data["formTitle"] = "locatie toevoegen onder ". $locatieParent[0]["loc_naam"];
                   
       }
        
       
       
        $data["errormessages"] = $errormessages;
        $data["succesmessages"] = $succesmessages;
        $data['form'] = $form->createView();
        $data['id'] = $id;
        $data['parentid'] = $parentid;
        $data['mode'] = $mode;
        $data["meta_title"] = "Admin";
        $data["meta_descr"] = "";
        $data["meta_keywords"] = "";
        $data["meta_indexFollow"] = false;
        $data["breadcrump"] = \clarafey\BLL\globalData\breadCrumpManagerClass::getInstance()->GetBreadCrumps("SMM-LOCATIES");
        return $app['twig']->render('gebouwbeheerLocatie.twig', $data);
    }

}
