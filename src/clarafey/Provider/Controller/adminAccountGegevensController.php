<?php

namespace clarafey\Provider\Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Silex\ControllerCollection;
use Silex\Provider\FormServiceProvider;

require_once __DIR__ . DIRECTORY_SEPARATOR . 'mainController.php';

//Handles the index.php(root)
class adminAccountGegevensController implements ControllerProviderInterface {

    protected $data;

    /* function __construct($data) {
      $this->data = $data;
      } */

    function __construct() {
        
    }

    public function connect(Application $app) {
        $controllers = $app['controllers_factory'];

        // Bind sub-routes 
        $controllers->get('/', array($this, 'overview'));
        $controllers->post('/', array($this, 'overview'));

        return $controllers;
    }

    public function overview(Application $app) {
        
        $data = \MainData::Process($app); 
        $request = $app['request'];
        $errormessages = array();
        $succesmessages = array();
        $personeelsnummer = null;
        $infoMessage=null;
        $email=null;
        $registrationInst =  \clarafey\BLL\gebruikerBLL\registrationClass::getInstance($app);
      
        $data["showForm"] = true; 
        if ($registrationInst->getPersoneelscode()==null)
        {
           return $app->redirect($app['request']->getBaseUrl() . '/fout/');
        }
        $fdata = array(
            'email' => $registrationInst->getEmail(),
            'password' => '',
            'password2' => ''
        );
        $form = $app['form.factory']->createBuilder('form', $fdata)
                ->add('email', 'email', array('required' => false,'label' => 'Email'))
                ->add('password', 'password', array('required' => true,'label' => 'Password'))
                ->add('password2', 'password', array('required' => true, 'label' => 'Password herhaling'))
                ->getForm();
        $form->handleRequest($request);
        
        if ($form->isValid() && 'POST' == $request->getMethod() && $request->request->get('formChangeAccountButton') == 1) {
            $fdata = $form->getData();
            if (strlen(trim($fdata["password"])) < 4)
                $errormessages[] = "Gelieve een passwoord van min. 4 tekens in te geven.";
            if ($fdata["password"] != $fdata["password2"])
                $errormessages[] = "Paswoord en herhaling zijn niet gelijk.";
            if (count($errormessages) == 0) {
                $result = $registrationInst->updateAccount( $fdata["password"],$fdata["email"]);
                if ($result === true)  
                {
                    $succesmessages[] = "Uw accountgegevens zijn gewijzigd.<br><a href='".  $data["global_UrlRoot"] ."'> klik hier om naar naar de startpagina te gaan.</a>";
                    $data["showForm"] = false; 
                }
                else
                     $errormessages[] = $result;
            } 
        }
        if (!$registrationInst->getIsRegistered() || $registrationInst->needNewPassword())
            $infoMessage =  "Hallo " . $registrationInst->getVoornaam() . " " . $registrationInst->getAchternaam().""
                . ",<br>het is de eerste keer dat u zich aanmeldt.<br>Gelieve een wachtwoord in te geven en indien u een eigen emailadres heeft, gelieve ook deze in te geven.";
        
        
        
        $data["breadcrump"] =   \clarafey\BLL\globalData\breadCrumpManagerClass::getInstance()->GetBreadCrumps("ACCOUNTGEGEVENS");
        $data["infomessage"] = $infoMessage;
        $data["errormessages"] = $errormessages;
        $data["succesmessages"] = $succesmessages;
        $data['form'] = $form->createView();
        return $app['twig']->render('adminAccountGegevens.twig', $data);
    }

}
