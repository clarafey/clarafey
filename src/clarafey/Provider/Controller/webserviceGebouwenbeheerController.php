<?php

namespace clarafey\Provider\Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Silex\ControllerCollection;
use Silex\Provider\FormServiceProvider;

require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'BLL' . DIRECTORY_SEPARATOR . 'inventarisBLL.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'BLL' . DIRECTORY_SEPARATOR . 'gebouwenbeheerBLL.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'BLL' . DIRECTORY_SEPARATOR . 'werkbonnenBeheerBLL.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'BLL' . DIRECTORY_SEPARATOR . 'kalenderBLL.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . 'mainController.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'BLL' . DIRECTORY_SEPARATOR . 'functions.php';

//Handles the index.php(root)
class webserviceGebouwenbeheerController implements ControllerProviderInterface {

    protected $data;

    function __construct() {
        
    }

    public function connect(Application $app) {
        $controllers = $app['controllers_factory'];
        /*         * ********************************** GET ********************************************* */

 

// Bind sub-routes  for Gebouwenbeheer
        $controllers->get('/overzicht/', array($this, 'wsGebouwenbeheerOverzicht'));
        $controllers->get('/overzicht2/', array($this, 'wsGebouwenbeheerOverzichtNested'));
 


        return $controllers;
    }

    public function wsGebouwenbeheerOverzicht(Application $app) {
        \clarafey\BLL\gebouwenbeheerBLL\gebouwenbeheerBLL::GetLocatieTreeJson();
        return "";
    }

//json geformatteerd voor Kendo Treeview in elke JSON wheren een Children Object
    public function wsGebouwenbeheerOverzichtNested(Application $app) {
        return \clarafey\BLL\gebouwenbeheerBLL\gebouwenbeheerBLL::GetLocatieTreeJsonNested();
    }


}
