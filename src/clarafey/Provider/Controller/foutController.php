<?php

namespace clarafey\Provider\Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Silex\ControllerCollection;
use Silex\Provider\FormServiceProvider;

class foutController implements ControllerProviderInterface {

    protected $data;

    /* function __construct($data) {
      $this->data = $data;
      } */

    function __construct() {
        
    }

    public function connect(Application $app) {

        $controllers = $app['controllers_factory'];

        // Bind sub-routes
        $controllers->get('/', array($this, 'overview'));
        $controllers->post('/', array($this, 'overview'));

        return $controllers;
    }
  
    public function overview(Application $app) {


      //  \myBO::SetActivePage($app,DEF_PAGE_AZ);
       
   

        $request = $app['request'];
        $data = \MainData::Process($app); 
        
        $infoMessageTitle = "";
        $infoMessageBody = "";
        $fout = $request->query->get('$fout');
      
            $infoMessageTitle = "Er is een fout opgetreden";
            $infoMessageBody = "";
            
       /* if ($fout=="registered")
        {
            $email = $request->query->get('email');
            $infoMessageTitle = "Uw e-mailadres bevestigen";
            $infoMessageBody = "Een e-mail is verzonden naar " . $email . ".Klik binnen 24 uur op de link in deze e-mail om uw account te activeren.";
        
            
        }
        if ($infoType=="activationSuccess")
        {
            $email = $request->query->get('email');
            $infoMessageTitle = "Uw e-mailadres ".$email." is geactiveerd !";
            $infoMessageBody = "U kan inloggen en van start gaan met het ingeven van evenementen en attracties.";
        }
        if ($infoType=="passwordReset")
        {
            $email = $request->query->get('email');
            $infoMessageTitle = "Wachtwoord wijzigen - email verstuurd";
            $infoMessageBody = "Er is een email verstuurd naar " . $email. ". Gelieve de instructies te volgen om uw paswoord te wijzgen.";
           //U ontvangt binnen enkele minuten een e-mail waarmee u een nieuw wachtwoord kunt aanmaken.
            
        }
         if ($infoType=="passwordChanged")
        {
            $email = $request->query->get('email');
            $infoMessageTitle = "Uw wachtwoord is gewijzigd";
            $infoMessageBody = "";
           //U ontvangt binnen enkele minuten een e-mail waarmee u een nieuw wachtwoord kunt aanmaken.
            
        }
         if ($infoType=="accountDeleted")
        {
            $email = $request->query->get('email');
            $infoMessageTitle = "Profiel verwijderd";
            $infoMessageBody = "Al uw gevegens zijn uit onze website en databank verwijderd. Hartelijk dank voor uw bezoek aan onze website.";
           //U ontvangt binnen enkele minuten een e-mail waarmee u een nieuw wachtwoord kunt aanmaken.
            
        }*/
        
        
        $data["infoMessageTitle"] = $infoMessageTitle;
        $data["infoMessageBody"] = $infoMessageBody;
        return $app['twig']->render('fout.twig', $data);
    }

}


