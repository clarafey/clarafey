<?php

namespace clarafey\Provider\Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Silex\ControllerCollection;
use Silex\Provider\FormServiceProvider;

require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'BLL' . DIRECTORY_SEPARATOR . 'inventarisBLL.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'BLL' . DIRECTORY_SEPARATOR . 'gebouwenbeheerBLL.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'BLL' . DIRECTORY_SEPARATOR . 'werkbonnenBeheerBLL.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'BLL' . DIRECTORY_SEPARATOR . 'kalenderBLL.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . 'mainController.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'BLL' . DIRECTORY_SEPARATOR . 'functions.php';

//Handles the index.php(root)
class webserviceInventarisController implements ControllerProviderInterface {

    protected $data;

    function __construct() {
        
    }

    public function connect(Application $app) {
        $controllers = $app['controllers_factory'];
        /*         * ********************************** GET ********************************************* */



        $controllers->get('/overzicht/', array($this, 'wsInventarisOverzicht'));
        $controllers->get('/items/', array($this, 'wsInventarisGetItems'));
        $controllers->get('/merken/', array($this, 'wsInventarisGetMerken'));
        $controllers->get('/leverancier/', array($this, 'wsInventarisGetLeverancier'));
        $controllers->get('/toestel/', array($this, 'wsInventarisGetToestel'));
        $controllers->post('/merken/save/', array($this, 'wsInventarisMerkSave'));
        $controllers->post('/leverancier/save/', array($this, 'wsInventarisLeverancierSave'));
        $controllers->post('/toestel/save/', array($this, 'wsInventarisToestelSave'));
        $controllers->post('/save/', array($this, 'wsInventarisSave'));
       
        /*         * ********************************* POST ********************************************* */


        return $controllers;
    }

 
    public function wsInventarisMerkSave(Application $app) {
        $request = $app['request'];
        $json = $request->request->get("models");
        $resultArray = json_decode($json, true);
        $merk = new \clarafey\BLL\inventarisBLL\Merk();
        $merk->setId($resultArray[0]["id"]);
        $merk->setNaam($resultArray[0]["naam"]);
        $retval = \clarafey\BLL\inventarisBLL\inventarisBLL::saveMerk($merk);
        if (is_string($retval)) {
            return $retval;
        } else {
            return json_encode($retval);
        }
    }
   

    public function wsInventarisLeverancierSave(Application $app) {

        $request = $app['request'];
        $json = $request->request->get("models");
        $resultArray = json_decode($json, true);
        $lev = new \clarafey\BLL\inventarisBLL\Leverancier();
        $lev->setId($resultArray[0]["id"]);
        $lev->setNaam($resultArray[0]["naam"]);
        $retval = \clarafey\BLL\inventarisBLL\inventarisBLL::saveLeverancier($lev);
        if (is_string($retval)) {
            return $retval;
        } else {

            return json_encode($retval);
        }
    }

    public function wsInventarisToestelSave(Application $app) {



        $request = $app['request'];
        $json = $request->request->get("models");

        $resultArray = json_decode($json, true);
        $toestel = new \clarafey\BLL\inventarisBLL\Toestel();
        $toestel->setId($resultArray[0]["id"]);
        $toestel->setNaam($resultArray[0]["naam"]);
        $toestel->setCategorie($resultArray[0]["categorie"]);


        $retval = \clarafey\BLL\inventarisBLL\inventarisBLL::saveToestel($toestel);
        if (is_string($retval)) {

            return $retval;
        } else {

            return json_encode($retval);
        }
    }

    public function wsInventarisSave(Application $app) {

        $request = $app['request'];
        $dateAankoop = null;
        $dateUitDienst = null;
        $id = null;
        $idPost = $request->request->get('id');
        if (!empty($idPost))
            $id = $request->request->get('id');

        $datum = $request->request->get('jqxDateTimeAankoop');
        if (!empty($datum))
            $dateAankoop = \DateTime::createFromFormat('d/m/Y', $datum);

        $datum = $request->request->get('jqxDateTimeUitDienst');
        if (!empty($datum))
            $dateUitDienst = \DateTime::createFromFormat('d/m/Y', $datum);

        $inv = new \clarafey\BLL\inventarisBLL\Inventaris();
        $inv->setId($id);
        $inv->setNummer263($request->request->get('nummer263'));
        $inv->setProduktOmschrijving($request->request->get('omschrijving'));
        $inv->setSerienummer($request->request->get('serienummer'));
        $inv->setMacAdres($request->request->get('macadres'));
        $inv->setIpadres($request->request->get('ipdadres'));
        $inv->setToestelId($request->request->get('jqxComboToestel'));
        $inv->setMerkId($request->request->get('jqxComboMerk'));
        $inv->setLocatieId($request->request->get('locatie'));
        $inv->setLeverancierId($request->request->get('jqxComboLeverancier'));
        $inv->setProduktCode($request->request->get('produktcode'));
        $inv->setBestelbonNummer($request->request->get('bestelbonnr'));
        $inv->setDeviceLogin($request->request->get('devicelogin'));
        $inv->setDeviceWachtwoord($request->request->get('deviceww'));
        $inv->setUitDienstDatum($dateUitDienst);
        $inv->setAankoopDatum($dateAankoop);
        $inv->setLabel($request->request->get('label'));
        $inv->setOpmerking($request->request->get('editor'));
        $gebruikerId = \clarafey\BLL\gebruikerBLL\registrationClass::getInstance($app)->getId();

// \clarafey\BO\functions\functions::writeTrace("do = ".time());
        $retval = \clarafey\BLL\inventarisBLL\inventarisBLL::iuInventaris($gebruikerId, $inv);


        /*       $debug = "gebID:" . $gebruikerId .  " - " . $wa->getId(). " - titel:"  .  $wa->getTitel() . " - beschr:" . $wa->getBeschrijving() . " - loc:"
          . $wa->getLocatie()->getId() . " - inv:" .  $wa->getInventaris()->getId()
          . " - type:" .  $wa->getType()->getId()   . " - prio :" .  $wa->getPrioriteit()->getId(); */


// \clarafey\BO\functions\functions::writeTrace("do = ".time());


        if (is_string($retval)) {

            $output = 'fout : ' . $retval;
        } else {
            $id = $retval;
            $inv->setId($id);
            $output = 'ok';
        }
        return $output;
    }

    /**/

    public function wsInventarisGetItems(Application $app) {
        $request = $app['request'];
        $locatieId = $request->query->get('locatieid');
        $itemList = \clarafey\BLL\inventarisBLL\inventarisBLL::GetInventarisItems($locatieId);
        return json_encode($itemList);
    }

    public function wsInventarisOverzicht(Application $app) {
        $request = $app['request'];
        $zoektext = $request->query->get('zoektekst');
        $status = $request->query->get('status');
        $assetGroup = $request->query->get('assetGroup');
        \clarafey\BLL\inventarisBLL\inventarisBLL::GetInventarisLijstJson($zoektext, $status);
        return "";
    }

  

    public function wsInventarisGetMerken(Application $app) {
        $merkenLijst = \clarafey\BLL\inventarisBLL\inventarisBLL::GetMerken();
        return json_encode($merkenLijst);
    }

    public function wsInventarisGetLeverancier(Application $app) {
        $merkenLijst = \clarafey\BLL\inventarisBLL\inventarisBLL::GetLeverancier();
        return json_encode($merkenLijst);
    }

    public function wsInventarisGetToestel(Application $app) {
        $merkenLijst = \clarafey\BLL\inventarisBLL\inventarisBLL::GetToestellen();
        return json_encode($merkenLijst);
    }

   

}
