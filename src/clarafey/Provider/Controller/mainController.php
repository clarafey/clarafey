<?php

class MainData
{

    public static function Process($app)
    {
        
        $data = array();
        self::SetUrls($data);
        $data["global_releaseMode"] = (RELEASE_MODE == 'Y');
        $data["breadcrump"] = array();
        $data["meta_indexFollow"] = true;
        
        
        $detect = new Mobile_Detect;
        $data["global_isMobile"] = $detect->isMobile();
        $data["global_isTablet"] = $detect->isTablet();
        $data["global_isDesktop"] = !$detect->isMobile();
        

        $data["global_Registration"]  = \clarafey\BLL\gebruikerBLL\registrationClass::getInstance($app);
  
        $data["meta_title"] = "";
        $data["meta_descr"] = "";
        $data["meta_keywords"] = "";
        return $data;
    }

    static function SetUrls(&$data)
    {

        $data["global_UrlSelf"] = $_SERVER['PHP_SELF'];
        $data["global_UrlRoot"] = "http://" . $_SERVER['HTTP_HOST'];
        $data["global_UrlLogon"] = $data["global_UrlRoot"] . "/registratie/";
        $data["global_UrlAccountWijzigen"] = $data["global_UrlRoot"] . "/registratie/accountgegevens/";
        $data["global_UrlInventarisForm"] = $data["global_UrlRoot"];
    }
}
