<?php
namespace clarafey\Provider\UnitTests;
use Silex\Application;
use Silex\ControllerProviderInterface;
use Silex\ControllerCollection;
use Silex\Provider\FormServiceProvider;
require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'BLL' . DIRECTORY_SEPARATOR . 'globalData.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'BLL' . DIRECTORY_SEPARATOR . 'gebouwenbeheerBLL.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'BLL' . DIRECTORY_SEPARATOR . 'werkbonnenBeheerBLL.php';
require_once "testFunctions.php";

class unitTestWerkbonnenBeheerController implements ControllerProviderInterface {

    protected $data;
    protected $app;
    function __construct() {
       
    }
    public function connect(Application $app) {
       $this->app = $app;
       $controllers = $app['controllers_factory'];
        // Bind sub-routes 
       $controllers->get('/', array($this, 'PerformTests'));
       $controllers->post('/', array($this, 'PerformTests'));
       return $controllers;
    }
        //Deze functie wordt opgeropen bij aanvang van alle tests
    function InitTests()
    {
    }
   //Deze functie wordt opgeroepen bij einde van alle tests
    function CleanUpTests()
    {
    }
    
    //Deze functie voert alle tests uit.
    function PerformTests(Application $app) {

        $this->InitTests();
        $this->toevoegenWerkaanvraag();
        $this->updateWerkaanvraagTitel();
        $this->deleteWerkaanvraag();
        $this->CleanUpTests();
        $this->toevoegenWerkbon();
        $this->statusWijzigenWerkaanvraag();
        $this->statusWijzigenWerkbon();
        
        return "<br>all tests completed ";
    }

    //Deze unit-test voegt een werkaanvraag van het toe aan de databank en laadt deze weer in
    //Daarna worden verschillende tests uitgevoerd
    function toevoegenWerkaanvraag()
    {
        startTest("toevoegenWerkaanvraag");
        //een locatie ophalen. de eerste kampus gebruiken als testlocatie
        $campusList = \clarafey\BLL\gebouwenbeheerBLL\gebouwenbeheerBLL::GetCampussen();
        $campusKristusKoningId = $campusList[0]->getId();
        assertEquals("campus/locatie moet Kristus Koning zijn", $campusKristusKoningId,98,false);
        $locatie = new \clarafey\BLL\gebouwenbeheerBLL\Locatie();
        $locatie->setId($campusKristusKoningId);
        //Werkaanvraag aanmaken
        $wa = new \clarafey\BLL\werkbonnenBeheerBLL\Werkaanvraag();
        $wa->setLocatie($locatie); 
        $wa->setTitel("Werkaanvraag Unit Test");
        $wa->setBeschrijving("Beschrijving Unit Test");
        //de werkaanvraag toevoegen aan de databank(opslaan)
        $gebruikerId = 27; // Gert Donath
        $retval = \clarafey\BLL\werkbonnenBeheerBLL\WerkbonnenBeheerBLL::saveWerkAanvraag($gebruikerId, $wa);
        assertEquals("toevoegen van werkaanvraag geeft nieuwe Id terug",   gettype($retval),"integer",false);
        //werkaanvraag uit Databank halen
        $waNew = \clarafey\BLL\werkbonnenBeheerBLL\WerkbonnenBeheerBLL::getWerkAanvraag($retval);
        assertEquals("nieuwe werkaanvraag moet locatie Kristus Koning bevatten",   $waNew->getLocatie()->getId(),$campusKristusKoningId,false);
        assertEquals("nieuwe werkaanvraag juiste titel bevatten",   $waNew->getTitel(),"Werkaanvraag Unit Test",false);
        assertEquals("nieuwe werkaanvraag moet juiste beschrijving bevatten",   $waNew->getBeschrijving(),"Beschrijving Unit Test",false);
        assertEquals("de creator moet Gert Donath zijn",     $waNew->getCreator()->getId(),$gebruikerId,false);
        assertEquals("de status moet 'nieuwe werkaanvraag' bevatten",     $waNew->getStatus()->getWerkOprachtStatus()->getId(),5);
        //werkaanvraag weer verwijderen
        $deleteResult = \clarafey\BLL\werkbonnenBeheerBLL\WerkbonnenBeheerBLL::deleteWerkAanvraag($waNew->getId());
        assertEquals("het resultaat van een werkaanvraag verwijderen moet true zijn",  $deleteResult,true,true);
        endTest("toevoegenWerkaanvraag");
    }
    //deze test gaat de titel(korte beschrijving) wijzigen en opslaan in de databank
    function updateWerkaanvraagTitel()
    {
        startTest("updateWerkaanvraagTitel");
        $wa = $this->CreateTestData_Werkaanvraag();
        $wa->setTitel("Werkaanvraag Unit Test updated");
        $gebruikerId = 97; //  Jens Goetstouwers
        $id = \clarafey\BLL\werkbonnenBeheerBLL\WerkbonnenBeheerBLL::saveWerkAanvraag($gebruikerId, $wa);
        $waNew = \clarafey\BLL\werkbonnenBeheerBLL\WerkbonnenBeheerBLL::getWerkAanvraag($id);      
        assertEquals("de modifier moet 'Jens Goetstouwers' zijn",     $waNew->getModificator()->getId(),$gebruikerId,false);
        assertEquals("de nieuwe werkaanvraag moet de nieuwe titel bevatten",   $waNew->getTitel(),"Werkaanvraag Unit Test updated",false);
        $deleteResult = \clarafey\BLL\werkbonnenBeheerBLL\WerkbonnenBeheerBLL::deleteWerkAanvraag($waNew->getId());
        assertEquals("het resultaat van een werkaanvraag verwijderen moet 'true' zijn",  $deleteResult,true,true);
        endTest("updateWerkaanvraagTitel");
    }
    //deze test gaat een werkaanvrag wissen uit de databank
    function deleteWerkaanvraag()
    {
        startTest("deleteWerkaanvraag");
        $wa = $this->CreateTestData_Werkaanvraag();
        $wa->setTitel("Werkaanvraag Unit Test updated");
        $gebruikerId = 97; //  Jens Goetstouwers
        $id = \clarafey\BLL\werkbonnenBeheerBLL\WerkbonnenBeheerBLL::saveWerkAanvraag($gebruikerId, $wa);
        $deleteResult = \clarafey\BLL\werkbonnenBeheerBLL\WerkbonnenBeheerBLL::deleteWerkAanvraag($wa->getId());
        assertEquals("het resultaat van een werkaanvraag verwijderen moet true zijn",  $deleteResult,true,true);
        $waNew = \clarafey\BLL\werkbonnenBeheerBLL\WerkbonnenBeheerBLL::getWerkAanvraag($id);
        assertEquals("de verwijderde werkaanvraag inlezen moet 'null' teruggeven",  $waNew,null,true);
        endTest("deleteWerkaanvraag");
    }
    //deze test gaat een nieuwe werkbon aanmaken voor een werkaanvraag met status 'nieuwe Werkaanvraag'
    //dit mag niet lukken. een WB kan enkel aan een goedgekeurde werkaanvraag hangen.
    function toevoegenWerkbon()
    {
        startTest("toevoegenWerkbon");
        $wa = $this->CreateTestData_Werkaanvraag();
        $campusList = \clarafey\BLL\gebouwenbeheerBLL\gebouwenbeheerBLL::GetCampussen();
        $campusKristusKoningId = $campusList[0]->getId();
        $locatie = new \clarafey\BLL\gebouwenbeheerBLL\Locatie();
        $locatie->setId($campusKristusKoningId);
        assertEquals("campus/locatie moet Kristus Koning zijn", $campusKristusKoningId,98,false);
        //Werkaanvraag aanmaken
        $wb = new \clarafey\BLL\werkbonnenBeheerBLL\WerkBon();
        $wb->setParent($wa);
        $wb->setLocatie($locatie); 
        $wb->setTitel("Werkbon Unit Test");
        $wb->setBeschrijving("Beschrijving Unit Test");
        $type =  new \clarafey\BLL\werkbonnenBeheerBLL\WerkOprachtType();
        $type->setId(1);
        $wb->setType($type);
        //de werkbon toevoegen aan de databank(opslaan)
        $gebruikerId = 97; // Jens Goetstouwers
        $retval = \clarafey\BLL\werkbonnenBeheerBLL\WerkbonnenBeheerBLL::saveWerkBon($gebruikerId, $wb);
        assertEquals("toevoegen van werkbon geeft nieuwe Id terug",   gettype($retval),"integer",false);
        $wbNew = \clarafey\BLL\werkbonnenBeheerBLL\WerkbonnenBeheerBLL::getWerkBon($retval);
        assertEquals("nieuwe werkbon moet locatie Kristus Koning bevatten.",   $wbNew->getLocatie()->getId(),$campusKristusKoningId,false);
        assertEquals("nieuwe werkbon juiste titel bevatten",   $wbNew->getTitel(),"Werkbon Unit Test",false);
        assertEquals("nieuwe werkbon moet juiste beschrijving bevatten",   $wbNew->getBeschrijving(),"Beschrijving Unit Test",false);
        assertEquals("de creator moet Jens Goetstouwers zijn",     $wbNew->getCreator()->getId(),$gebruikerId,false);
        assertEquals("de status moet 'nieuwe werkbon' bevatten",   $wbNew->getStatus()->getWerkOprachtStatus()->getId(),1);
        //werkbon weer verwijderen
        $deleteResult = \clarafey\BLL\werkbonnenBeheerBLL\WerkbonnenBeheerBLL::deleteWerkBon($wbNew->getId());
        assertEquals("het resultaat van een werkbon verwijderen moet true zijn",  $deleteResult,true,true);
        //werkaanvraag terug verwijderen
        $deleteResult = \clarafey\BLL\werkbonnenBeheerBLL\WerkbonnenBeheerBLL::deleteWerkAanvraag($wa->getId());
        assertEquals("het resultaat van een werkaanvraag verwijderen moet true zijn",  $deleteResult,true,true);
        endTest("toevoegenWerkbon");
        
    }
    
    function statusWijzigenWerkaanvraag()
    {   
        startTest("statusWijzigenWerkaanvraag");
        $wa = $this->CreateTestData_Werkaanvraag();
        assertEquals("status nieuwe werkaanvraag moet 'nieuwe werkaanvraag' zijn",$wa->getStatus()->getWerkOprachtStatus()->getId(),5,false);
        $statush = new \clarafey\BLL\werkbonnenBeheerBLL\WerkOprachtStatusHistorie();
        $status = new \clarafey\BLL\werkbonnenBeheerBLL\WerkOprachtStatus();
        $gebruiker = new \clarafey\BLL\gebruikerBLL\Gebruiker();
        $gebruiker->setId(27);
        $status->setId(7);
        $statush->setWerkOprachtStatus($status);
        $statush->setWerkopdrachtId($wa->getId());
        $statush->setOpmerking("status wijzigen unit test");     
        $statush->setGebruiker($gebruiker);
        $wa->setStatus($statush);
        \clarafey\BLL\werkbonnenBeheerBLL\WerkbonnenBeheerBLL::saveWerkOpdrachtStatus($statush);
        $waNew = \clarafey\BLL\werkbonnenBeheerBLL\WerkbonnenBeheerBLL::getWerkAanvraag($wa->getId());
        assertEquals("de nieuwe status moet 'goedgekeurd' zijn",  $waNew->getStatus()->getWerkOprachtStatus()->getId(),7,false);
        assertEquals("de opmerking van de statuswijziging moet correct zijn",  $waNew->getStatus()->getOpmerking(),"status wijzigen unit test",false);
        assertEquals("de statuswijziging moet doorgevoerd zijn door Gert Donath",   $waNew->getStatus()->getGebruiker()->getId(),27,false);
        $deleteResult = \clarafey\BLL\werkbonnenBeheerBLL\WerkbonnenBeheerBLL::deleteWerkAanvraag($waNew->getId());
        assertEquals("het resultaat van een werkaanvraag verwijderen moet true zijn",  $deleteResult,true,true);
        endTest("statusWijzigenWerkaanvraag");
        
    }
    function statusWijzigenWerkbon()
    {
        startTest("statusWijzigenWerkbon");
        
        $wa = $this->CreateTestData_Werkaanvraag();
        $wb = $this->CreateTestData_Werkbon($wa);
        
        assertEquals("status nieuwe werkaanvraag moet 'nieuwe werkbon' zijn",$wb->getStatus()->getWerkOprachtStatus()->getId(),1,false);
        
        $statush = new \clarafey\BLL\werkbonnenBeheerBLL\WerkOprachtStatusHistorie();
        $status = new \clarafey\BLL\werkbonnenBeheerBLL\WerkOprachtStatus();
        $gebruiker = new \clarafey\BLL\gebruikerBLL\Gebruiker();
        $gebruiker->setId(97);
        $status->setId(3);
        $statush->setWerkOprachtStatus($status);
        $statush->setWerkopdrachtId($wb->getId());
        $statush->setOpmerking("status wijzigen unit test");     
        $statush->setGebruiker($gebruiker);
        $wb->setStatus($statush);
        \clarafey\BLL\werkbonnenBeheerBLL\WerkbonnenBeheerBLL::saveWerkOpdrachtStatus($statush);
        
        $wbNew = \clarafey\BLL\werkbonnenBeheerBLL\WerkbonnenBeheerBLL::getWerkBon($wb->getId());
        
        assertEquals("de nieuwe status moet 'goedgekeurd' zijn",  $wbNew->getStatus()->getWerkOprachtStatus()->getId(),3,false);
        assertEquals("de opmerking van de statuswijziging moet correct zijn",  $wbNew->getStatus()->getOpmerking(),"status wijzigen unit test",false);
        assertEquals("de statuswijziging moet doorgevoerd zijn door Jens Goedstouwers",   $wbNew->getStatus()->getGebruiker()->getId(),97,false);

        //status terug op 'nieuwe werbbon' zetten, zodat we deze kunnen verwijderen
        $status->setId(1);
        $statush->setWerkOprachtStatus($status);
        \clarafey\BLL\werkbonnenBeheerBLL\WerkbonnenBeheerBLL::saveWerkOpdrachtStatus($statush);
          
        $deleteResult = \clarafey\BLL\werkbonnenBeheerBLL\WerkbonnenBeheerBLL::deleteWerkBon($wbNew->getId());
        assertEquals("het resultaat van een werkaanvraag verwijderen moet true zijn",  $deleteResult,true,true);
        $deleteResult = \clarafey\BLL\werkbonnenBeheerBLL\WerkbonnenBeheerBLL::deleteWerkAanvraag($wa->getId());
        assertEquals("het resultaat van een werkbon verwijderen moet true zijn",  $deleteResult,true,true);
        
        
        
        
      
        endTest("statusWijzigenWerkbon");
    }
    /************************************/
    //Helper functies die testdata creeeren 
    /*************************************/
    //deze functie geeft een werkaanvraag terug
    function CreateTestData_Werkaanvraag()
    {
        $campusList = \clarafey\BLL\gebouwenbeheerBLL\gebouwenbeheerBLL::GetCampussen();
        $campusKristusKoningId = $campusList[0]->getId();
        $locatie = new \clarafey\BLL\gebouwenbeheerBLL\Locatie();
        $locatie->setId($campusKristusKoningId);
        //Werkaanvraag aanmaken
        $wa = new \clarafey\BLL\werkbonnenBeheerBLL\Werkaanvraag();
        $wa->setLocatie($locatie); 
        $wa->setTitel("Werkaanvraag Unit Test");
        $wa->setBeschrijving("Beschrijving Unit Test");
        //de werkaanvraag toevoegen aan de databank(opslaan)
        $gebruikerId = 27; // Gert Donath
        $id = \clarafey\BLL\werkbonnenBeheerBLL\WerkbonnenBeheerBLL::saveWerkAanvraag($gebruikerId, $wa);
        $waNew = \clarafey\BLL\werkbonnenBeheerBLL\WerkbonnenBeheerBLL::getWerkAanvraag($id);     
        return $waNew;
    }
    //deze functie geeft een werkbon terug
    //$wa: de parent werkaanvraag
    function CreateTestData_Werkbon(\clarafey\BLL\werkbonnenBeheerBLL\Werkaanvraag $wa)
    {
        $campusList = \clarafey\BLL\gebouwenbeheerBLL\gebouwenbeheerBLL::GetCampussen();
        $campusKristusKoningId = $campusList[0]->getId();
        $locatie = new \clarafey\BLL\gebouwenbeheerBLL\Locatie();
        $locatie->setId($campusKristusKoningId);
        //Werkaanvraag aanmaken
        $wb = new \clarafey\BLL\werkbonnenBeheerBLL\WerkBon();
        $wb->setParent($wa);
        $wb->setLocatie($locatie); 
        $wb->setTitel("Werkbon Unit Test");
        $wb->setBeschrijving("Beschrijving Unit Test");
        //type herstelling
        $type =  new \clarafey\BLL\werkbonnenBeheerBLL\WerkOprachtType();
        $type->setId(1);
        $wb->setType($type);
        //de werkbon toevoegen aan de databank(opslaan)
        $gebruikerId = 97; // Jens Goetstouwers
        $retval = \clarafey\BLL\werkbonnenBeheerBLL\WerkbonnenBeheerBLL::saveWerkBon($gebruikerId, $wb);
        //Werkbon terug inlezen uit databank
        $wbNew = \clarafey\BLL\werkbonnenBeheerBLL\WerkbonnenBeheerBLL::getWerkBon($retval);    
        return $wbNew;
    } 
     
}
 
 