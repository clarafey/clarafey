<?php

/* zelf geschreven functies voor onze unit-tests */

function startTest($functionName) {
    echo "<br>************************************************************************";
    echo "<br>start testfunctie '" . $functionName . "', " . $today = date("H:i:s");
}

function endTest($functionName) {
    echo "<br>einde testfunctie '" . $functionName . "', " . $today = date("H:i:s");
}

//De functie vergelijkt 2 waarden op gelijkheid.
//infoText : vrij textveld 
//functionName : de naam van de testfunctie
//objectValue :  de waarde die het te testen object bevat
//expectedValue : de waarde die verwacht wordt
//checkType : wanneer niet alleen de waarde, maar het type moet worden vergeleken (100 vs "100")
function assertEquals($infoText, $objectValue, $expectedValue, $checkType = false) {
    if ($checkType) {
        if ($objectValue === $expectedValue) {
            printSucces("assertEquals===", $infoText, $objectValue, $expectedValue);
            return true;
        } else {
            printFailure("assertEquals===", $infoText, $objectValue, $expectedValue);
            return false;
        }
    } else {
        if ($objectValue == $expectedValue) {
            printSucces("assertEquals==", $infoText, $objectValue, $expectedValue);
            return true;
        } else {
            printFailure("assertEquals==", $infoText, $objectValue, $expectedValue);
            return false;
        }
    }
}
//deze helper functie toont het resultaat van 1 test (bijv.assertEquals) indien de test postitief is
function printSucces($testType, $infoText, $objectValue, $expectedValue) {
    
    echo "<br><p font style='color:green'>OK : " . $testType . ", " . $infoText . " </p>";
   // echo "<p font style='color:green'>objectwaarde : " . $objectValue . ", verwachte waarde : " . $expectedValue . " </p>";

}
//deze helper functie toont het resultaat van 1 test (bijv.assertEquals) indien de test negatief is
function printFailure($testType, $infoText, $objectValue, $expectedValue) {
   
    echo "<br><p font style='color:red'>NOK : " . $testType . ", " . $infoText . " </p>";
    echo "<p font style='color:red'>objectwaarde : " . $objectValue . ", verwachte waarde : " . $expectedValue . " </p>";

    
}

