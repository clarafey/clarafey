<?php

namespace clarafey\BLL\kalenderBLL;

require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'DAL' . DIRECTORY_SEPARATOR . 'kalenderDAL.php';
require_once 'functions.php';
require_once 'gebruikerBLL.php';




class cKalenderReservationTypeExtras implements \JsonSerializable {

    protected $id;
    protected $naam;

    public function setNaam($naam) {
        $this->naam = $naam;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getId() {
        return $this->id;
    }

    public function getNaam() {
        return $this->naam;
    }

    public function __toString() {
        return $this->naam;
    }

    public function jsonSerialize() {
        return [


            "id" => $this->getId(),
            "naam" => $this->getNaam()
        ];
    }

}


class cKalenderReservationType implements \JsonSerializable {

    protected $id;
    protected $naam;
    protected $color;

    public function setNaam($naam) {
        $this->naam = $naam;
    }

    public function getNaam() {
        return $this->naam;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getId() {
        return $this->id;
    }

    public function setColor($color) {
        $this->color = $color;
    }

    public function getColor() {
        return $this->color;
    }

    public function __toString() {
        return $this->naam;
    }

    public function jsonSerialize() {
        return [
            "id" => $this->getId(),
            "naam" => $this->getNaam(), "color" => $this->getColor()
        ];
    }

}

class cKalenderReservatie implements \JsonSerializable {

    //uit db 263
    protected $id;
    protected $reservationType;
    protected $description;
    protected $title;
    protected $start;
    protected $end;
    protected $creationDate;
    protected $recurrenceId;
    protected $recurrenceRule;
    protected $recurrenceException;
    protected $isAllDay;
    //de gebruiker die het event/resrevatie heeft aangemaakt
    protected $gebruiker;
    protected $extras; //extras (laptop,beamer,kabel) (Komma seperated string)
    protected $extrasDirectie;//koffie;broodjes,
    protected $attendees; //aantal personen
    
    function __construct() {

        $reservationType = new cKalenderReservationType();
        $gebruiker = new \clarafey\BLL\gebruikerBLL\Gebruiker();
        $creationDate = new \DateTime();
    }

    public function setType(cKalenderReservationType $type) {
        $this->reservationType = $type;
    }

    public function getType() {
        return $this->reservationType;
    }

    public function setGebruiker(\clarafey\BLL\gebruikerBLL\Gebruiker $gebruiker) {
        $this->gebruiker = $gebruiker;
    }

    public function getGebruiker() {
        return $this->gebruiker;
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }
     public function getCreationDate() {
        return $this->creationDate;
    }

    public function setCreationDate($creationDate) {
        $this->creationDate = $creationDate;
    }
    
     public function getAttendees() {
        return $this->attendees;
    }

    public function setAttendees($attendees) {
        $this->attendees = $attendees;
    }
    
     public function getExtras() {
        return $this->extras;
    }

    public function setExtras($kommaSepString) {
        $this->extras = $kommaSepString;
    } 
    //Extra's zoals koffie, broodjes die enkel directie/directiemedewerkers kunnen boeken
  public function getExtrasDirectie() {
        return $this->extrasDirectie;
    }

    public function setExtrasDirectie($kommaSepString) {
        $this->extrasDirectie = $kommaSepString;
    } 
    public function getTitel() {
        return $this->title;
    }

    public function setTitel($titel) {
        $this->title = $titel;
    }

    public function getDescription() {
        return $this->description;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function getStartDate() {
        return $this->start;
    }

    public function setStartDate($startDate) {
        $this->start = $startDate;
    }

    public function getEndDate() {
        return $this->end;
    }

    public function setEndDate($endDate) {
        $this->end = $endDate;
    }

    public function getRecurrenceRule() {
        return $this->recurrenceRule;
    }

    public function setRecurrenceRule($rule) {
        $this->recurrenceRule = $rule;
    }
  public function getRecurrenceException() {
        return $this->recurrenceException;
    }

    public function setRecurrenceException($exception) {
        $this->recurrenceException = $exception;
    }
      public function getRecurrenceId() {
        return $this->recurrenceId;
    }

    public function setRecurrenceId($id) {
        $this->recurrenceId = $id;
    }
    public function getIsAllDay() {
        return $this->isAllDay;
    }

    public function setIsAllDay($isAllDay) {
        $this->isAllDay = $isAllDay;
    }

    // Datatables.net
    /* public function jsonSerialize() {
      return [

      "<a href='/inventaris/editeren/?nummer263=". $this->nummer263."'><span class='glyphicon glyphicon-edit' aria-hidden='true'></span></a>",
      $this->IsInventarised() ?  '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>' : '',
      $this->nummer263,
      $this->getProduktOmschrijving263(),
      round($this->investering263,2),
      $this->label,
      $this->toestel->getNaam(),
      $this->merk->getNaam(),
      $this->locatiePath



      ];
      } */
    /* voor phpGrid activeren */

    public function jsonSerialize() {
        return [
            "TaskID" => $this->getId(),
            "TypeID" => $this->getType()->getId(),
            "TypeName" => ($this->getType()->getNaam()!=null) ? $this->getType()->getNaam() : "",
            "Title" => $this->getTitel(),
            "Description" => $this->getDescription(),
            "Start" => \clarafey\BLL\functions\functions::PHPDateTimeToJavaDate($this->getStartDate()),
            "End" => \clarafey\BLL\functions\functions::PHPDateTimeToJavaDate($this->getEndDate()),
            "RecurrenceRule" => $this->getRecurrenceRule(),
            "RecurrenceId" => $this->getRecurrenceId(),
            "RecurrenceException" => $this->getRecurrenceException(),
            "IsAllDay" => $this->getIsAllDay(),
            "GebruikerID" => $this->getGebruiker()->getId(),
            "GebruikerNaam" => $this->getGebruiker()->getVoornaam() . " " . $this->getGebruiker()->getAchternaam(),
            
            "CreationDate" => ($this->creationDate==null) ? "" : date_format($this->creationDate, 'Y-m-d H:i'),
            
            "Attendees" =>$this->getAttendees(),
            "Extras" =>  explode(",",$this->getExtras()),
            "ExtrasDirectie" =>  explode(",",$this->getExtrasDirectie())
           
        ];
    }

}

class kalenderBLL {

    //om tegen tegaan dat deze klasse wordt geinstantieerd.
    private function __construct() {
        
    }
static function DeleteKalenderReservatie($id) {
        \clarafey\BLL\functions\functions::writeTrace("start DeleteKalenderReservatie ". $id );
return \clarafey\DAL\kalenderDAL\kalenderDAL::DeleteKalenderReservatie($id);
}

 
    static function uiKalenderReservatie(\clarafey\BLL\gebruikerBLL\Gebruiker $gebruiker, cKalenderReservatie $kalenderReservatieObj) {

        $result = \clarafey\DAL\kalenderDAL\kalenderDAL::uiKalenderReservatie(
                        $gebruiker->getId(), $kalenderReservatieObj->getId(), $kalenderReservatieObj->getType()->getId()
                        , $kalenderReservatieObj->getTitel(), $kalenderReservatieObj->getDescription(), $kalenderReservatieObj->getStartDate(), $kalenderReservatieObj->getEndDate(),
                $kalenderReservatieObj->getRecurrenceRule(),    $kalenderReservatieObj->getRecurrenceException(),   $kalenderReservatieObj->getRecurrenceId(),
                $kalenderReservatieObj->getIsAllDay(),
                 $kalenderReservatieObj->getExtras(),  $kalenderReservatieObj->getExtrasDirectie(),  $kalenderReservatieObj->getAttendees()
        );
  
        if (is_string($result))
            return $result;
        else {
            $kalenderReservatieObj->setGebruiker($gebruiker);
            $kalenderReservatieObj->setId($result);
            return $kalenderReservatieObj;
        }
    }

    static function GetKalenderReservaties($campus,$categorie,$gebruikerId) {
        $dbList = \clarafey\DAL\kalenderDAL\kalenderDAL::GetKalenderReservaties($campus,$categorie,$gebruikerId);
        $kalList = array();
        foreach ($dbList as $row) {
            $kalList[] = self::ParseKalenderRow($row);
        }
        return $kalList;
    }
    static function GetReservatieTypes($campus,$categorie) {
        $dbList = \clarafey\DAL\kalenderDAL\kalenderDAL::GetReservatieType($campus,$categorie);
        $rtList = array();
        foreach ($dbList as $row) {
            $rt = new cKalenderReservationType();
            $rt->setId($row["kalrt_id"]);
            $rt->setNaam($row["kalrt_naam"]);
            $rt->setColor($row["kalrt_color"]);
            $rtList[] = $rt;
        }
        return $rtList;
    }
    
     static function GetReservatieTypesExtras($categorie,$adminOnly) {
         
        $dbList = \clarafey\DAL\kalenderDAL\kalenderDAL::GetReservatieTypeExtras($categorie,$adminOnly);
        $rtList = array();
        foreach ($dbList as $row) {
            $rt = new cKalenderReservationType();
            $rt->setId($row["kalrte_id"]);
            $rt->setNaam($row["kalrte_naam"]);
 
            $rtList[] = $rt;
        }
        return $rtList;
    }
    
    static function ParseKalenderRow($row) {

        $k = new cKalenderReservatie();
        $k->setId($row["kalr_id"]);
        $k->setTitel($row["kalr_titel"]);
        $k->setDescription($row["kalr_beschrijving"]);
        $k->setRecurrenceRule($row["kalr_recurrenceRule"]);
        $k->setRecurrenceException($row["kalr_recurrenceException"]);
        $k->setRecurrenceId($row["kalr_recurrenceId"]);
        $type = new cKalenderReservationType();
        $type->setId($row["kalr_kalrt_id"]);
        $type->setNaam($row["kalrt_naam"]);
        $k->setType($type);
        $k->setIsAllDay($row["kalr_isAllDay"]);
        $startdatum = \clarafey\BLL\functions\functions::MsSQlToDateTime($row["kalr_startdatum"]);
        $k->setStartDate($startdatum);
        $einddatum = \clarafey\BLL\functions\functions::MsSQlToDateTime($row["kalr_einddatum"]);
        $k->setEndDate($einddatum);
        
        $createDatum = \clarafey\BLL\functions\functions::MsSQlToDateTime($row["kalr_geb_id_creationdate"]);
        $k->setCreationDate($createDatum);
        
        $k->setExtras($row["kalr_extras"]);
        $k->setExtrasDirectie($row["kalr_extras_directie"]);
        $k->setAttendees($row["kalr_attendees"]);
        $gb = new \clarafey\BLL\gebruikerBLL\Gebruiker();
        $gb->setId($row["kalr_geb_id_creator"]);
        $gb->setVoornaam($row["geb_voornaam"]);
        $gb->setAchternaam($row["geb_achternaam"]);
       
        $k->setGebruiker($gb);
        
        
        return $k;




        /* $inv = new Inventaris();
          $inv->setId($row["inv_id"]);
          if (array_key_exists("Serienummer", $row))
          $inv->setNummer263($row["Serienummer"]); //uit boekhoudingstabel
          else
          $inv->setNummer263($row["inv_263_nummer"]);  //uit onze inventaris tabel

          if (array_key_exists("Omschrijving", $row))
          $inv->setProduktOmschrijving263($row["Omschrijving"]);

          if (array_key_exists("inv_productomschrijving", $row))
          $inv->setProduktOmschrijving($row["inv_productomschrijving"]);

          if (array_key_exists("inv_serienummer", $row))
          $inv->setSerienummer($row["inv_serienummer"]);
          if (array_key_exists("inv_macadres", $row))
          $inv->setMacAdres($row["inv_macadres"]);
          if (array_key_exists("inv_ipadres", $row))
          $inv->setIpadres($row["inv_ipadres"]);
          if (array_key_exists("inv_tst_id", $row))
          $inv->setToestelId($row["inv_tst_id"]);
          if (array_key_exists("inv_mer_id", $row))
          $inv->setMerkId($row["inv_mer_id"]);
          if (array_key_exists("inv_loc_id", $row))
          $inv->setLocatieId($row["inv_loc_id"]);
          if (array_key_exists("inv_lev_id", $row))
          $inv->setLeverancierId($row["inv_lev_id"]);
          if (array_key_exists("inv_productcode", $row))
          $inv->setProduktCode($row["inv_productcode"]);
          if (array_key_exists("inv_bestelbon", $row))
          $inv->setBestelbonNummer($row["inv_bestelbon"]);
          if (array_key_exists("inv_devicelogin", $row))
          $inv->setDeviceLogin($row["inv_devicelogin"]);
          if (array_key_exists("inv_devicewachtwoord", $row))
          $inv->setDeviceWachtwoord($row["inv_devicewachtwoord"]);
          if (array_key_exists("inv_label", $row))
          $inv->setLabel($row["inv_label"]);
          if (array_key_exists("inv_aankoopdatum", $row))
          $inv->setAankoopDatum(\clarafey\BO\functions\functions::MsSQlToDate($row["inv_aankoopdatum"]));
          if (array_key_exists("inv_uitdienst", $row))
          $inv->setUitDienstDatum(\clarafey\BO\functions\functions::MsSQlToDate($row["inv_uitdienst"]));
          if (array_key_exists("inv_opmerking", $row))
          $inv->setOpmerking($row["inv_opmerking"]);
          $merk = new Merk();
          if (array_key_exists("mer_id", $row)) {

          $merk->setId($row["inv_mer_id"]);
          $merk->setNaam($row["mer_naam"]);
          }
          $inv->setMerk($merk);
          $toestel = new Toestel();
          if (array_key_exists("tst_id", $row)) {

          $toestel->setId($row["inv_tst_id"]);
          $toestel->setNaam($row["tst_naam"]);
          }
          $inv->setToestel($toestel);

          $locatie = new \clarafey\BO\gebouwenbeheerBO\Locatie();
          if (array_key_exists("loc_id", $row)) {

          $locatie->setId($row["inv_loc_id"]);
          $locatie->setNaam($row["loc_naam"]);
          $locatie->InsertLevelStrings($row["locatiepath"]);
          }
          $inv->setLocatie($locatie);
          $leverancier = new cLeverancier();
          if (array_key_exists("lev_id", $row)) {


          $leverancier->setId($row["inv_lev_id"]);
          $leverancier->setNaam($row["lev_naam"]);
          }
          $inv->setLeverancier($leverancier);
          if (array_key_exists("Factuurdatum", $row)) {
          $factuurDatum = \clarafey\BO\functions\functions::MsSQlToDate($row["Factuurdatum"]);
          $inv->setFactuurDatum263($factuurDatum);
          }
          if (array_key_exists("Groep", $row))
          $inv->setGroep263($row["Groep"]);
          if (array_key_exists("Investering", $row))
          $inv->setInvestering263($row["Investering"]);

          return $inv; */
    }

    /* static function GetComputerOwner() {
      $dbList = \clarafey\DAL\inventarisDL\inventarisDL::GetComputerOwnerType();
      $computerOwnerTypeList = array();
      foreach ($dbList as $k => $v) {
      $m = new C();
      $m->setId($k);
      $m->setNaam($v);
      $computerOwnerTypeList[] = $m;
      }
      return $computerOwnerTypeList;
      } */
}
