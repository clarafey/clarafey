<?php

namespace clarafey\BLL\werkbonnenBeheerBLL;

require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'DAL' . DIRECTORY_SEPARATOR . 'werkbonnenBeheerDAL.php';
require_once 'gebouwenbeheerBLL.php';
require_once 'gebruikerBLL.php';
require_once 'inventarisBLL.php';
require_once 'functions.php';

//classe WerkOprachtPrioriteit
//doel : bevat de prioriteit van een werkopdracht (laag,hoog,normaal,gevaar)
//implementeerd interface JsonSerializable voor webservice
class WerkOprachtPrioriteit implements \JsonSerializable {

    protected $id;
    protected $naam;

    public function setNaam($naam) {
        $this->naam = $naam;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getId() {
        return $this->id;
    }

    public function getNaam() {
        return $this->naam;
    }

    public function __toString() {
        return $this->naam;
    }

    public function jsonSerialize() {
        return [
            "id" => $this->getId(),
            "naam" => $this->getNaam()
        ];
    }

}

//Klasse : WerkOprachtStatus 
//Databank : tabel werkopdracht_status 
//bevat de statussen die de Technische dienst kan zetten op niveau werkbon (nieuwe werkbon,materiaal besteld, in uitvoering, afgewerkt)
class WerkOprachtStatus implements \JsonSerializable {

    protected $id;
    protected $naam;

    public function setNaam($naam) {
        $this->naam = $naam;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getId() {
        return $this->id;
    }

    public function getNaam() {
        return $this->naam;
    }

    public function __toString() {
        return $this->naam;
    }

    public function jsonSerialize() {
        return [
            "id" => $this->getId(),
            "naam" => $this->getNaam()
        ];
    }

}

//Deze klasse bevat een item uit de status historiek van een werkbon
//de technische dienst wijzigt doorheen het process de statussen.
//Database : tabel werkopdracht_status_historiek
class WerkOprachtStatusHistorie implements \JsonSerializable{

  
    protected $werkOprachtId;
    protected $werkOprachtStatus;
    protected $gebruiker;
    protected $datum;
    protected $opmerking;

    function __construct() {

        $werkOprachtStatus = new WerkOprachtStatus();
        $gebruiker = new \clarafey\BLL\gebruikerBLL\Gebruiker();
        $datum = new \DateTime();
    }

    public function setWerkOprachtStatus(WerkOprachtStatus $werkOprachtStatus) {
        $this->werkOprachtStatus = $werkOprachtStatus;
    }

    public function getWerkOprachtStatus() {
        return $this->werkOprachtStatus;
    }

    public function setGebruiker(\clarafey\BLL\gebruikerBLL\Gebruiker $gebruiker) {
        $this->gebruiker = $gebruiker;
    }

    public function getGebruiker() {
        return $this->gebruiker;
    }

    public function setOpmerking($opmerking) {
        $this->opmerking = $opmerking;
    }

    public function getOpmerking() {
        return $this->opmerking;
    }
    public function setDatum($datum) {
        $this->datum = $datum;
    }

    public function getDatum() {
        return $this->datum;
    }
  

    public function setWerkopdrachtId($id) {
        $this->werkOprachtId = $id;
    }

    public function getWerkopdrachtId() {
        return $this->werkOprachtId;
    }
     public function jsonSerialize() {
        return [
            
            "datum" => $this->getDatum(),
            "statusnaam" => $this->getWerkOprachtStatus()->getNaam(),
             "statusopmerking" => $this->getOpmerking(),
            "voornaam" => $this->getGebruiker()->getVoornaam(),
             "achternaam" => $this->getGebruiker()->getAchternaam()
        ];
    }

}

//Klasse : WerkOprachtType
//Databank : tabel werkopdracht_type
//bevat een  mogelijke type of soort werkaanvraag  (herstelling, investering, periodiek onderhoud, renovatie )
class WerkOprachtType implements \JsonSerializable {

    protected $id;
    protected $naam;

    public function setNaam($naam) {
        $this->naam = $naam;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getId() {
        return $this->id;
    }

    public function getNaam() {
        return $this->naam;
    }

    public function __toString() {
        return $this->naam;
    }

    public function jsonSerialize() {
        return [
            "id" => $this->getId(),
            "naam" => $this->getNaam()
        ];
    }

}

//De klasse WerkOpdracht
//type abstract : kan niet geintantieerd worden.
//afgeleide klassen : Werkaanvraag en Werkbon
abstract class WerkOpdracht {

    protected $id = null;
    protected $classType = null;
    protected $type;
    protected $status;
    protected $prioriteit;
    protected $creatieDatum;
    protected $creator;
    protected $modificatieDatum;
    protected $modificator;
    protected $titel;
    protected $beschrijving;
    protected $locatie;
    protected $inventaris;
    protected $parentWerkOpdracht;
    protected $begindatum;
    protected $einddatum;
    function __construct() {

        $this->type = new WerkOprachtType();
        $this->status = new WerkOprachtStatusHistorie();
        $this->prioriteit = new WerkOprachtPrioriteit();
        $this->locatie = new \clarafey\BLL\gebouwenbeheerBLL\Locatie();
        $this->inventaris = new \clarafey\BLL\inventarisBLL\Inventaris();
        $this->creator = new \clarafey\BLL\gebruikerBLL\Gebruiker();
        $this->modificator = new \clarafey\BLL\gebruikerBLL\Gebruiker();
        $this->parentWerkOpdracht = null;
    }
   
    public function setId($id) {
        $this->id = $id;
    }

    public function getId() {
        return $this->id;
    }

    public function getClassType() {
        return $this->classType;
    }
    public function setStatus(WerkOprachtStatusHistorie $status) {
        $this->status = $status;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setBegindatum($datum) {
        $this->begindatum = $datum;
    }

    public function getBegindatum() {
        return $this->begindatum;
    }

    public function setEinddatum($datum) {
        $this->einddatum = $datum;
    }

    public function getEinddatum() {
        return $this->einddatum;
    }

    public function setTitel($titel) {
        $this->titel = $titel;
    }

    public function getTitel() {
        return $this->titel;
    }

    public function setBeschrijving($text) {
        $this->beschrijving = $text;
    }

    public function getBeschrijving() {
        return $this->beschrijving;
    }

    public function setCreator(\clarafey\BLL\gebruikerBLL\Gebruiker $gebruiker) {
        $this->creator = $gebruiker;
    }

    public function getCreator() {
        return $this->creator;
    }

    public function setModificator(\clarafey\BLL\gebruikerBLL\Gebruiker $gebruiker) {
        $this->modificator = $gebruiker;
    }

    public function getModificator() {
        return $this->modificator;
    }

    public function setLocatie(\clarafey\BLL\gebouwenbeheerBLL\Locatie $locatie) {
        $this->locatie = $locatie;
    }

    public function getLocatie() {
        return $this->locatie;
    }

    public function setInventaris(\clarafey\BLL\inventarisBLL\Inventaris $inventaris) {
        $this->inventaris = $inventaris;
    }

    public function getInventaris() {
        return $this->inventaris;
    }

    public function setType(WerkOprachtType $type) {
        $this->type = $type;
    }

    public function getType() {
        return $this->type;
    }

    public function getPrioriteit() {

        return $this->prioriteit;
    }

    public function setPrioriteit(WerkOprachtPrioriteit $prio) {
        $this->prioriteit = $prio;
    }

    public function getCreatiedatum() {
        return $this->creatieDatum;
    }

    public function setCreatiedatum($datum) {
        $this->creatieDatum = $datum;
    }

}

//class : cWerkaanvraag
//parent class : cWerkopdracht(abstract)
class Werkaanvraag extends WerkOpdracht implements \JsonSerializable {

    function __construct() {

        parent::__construct();
        $this->classType = "WA";
        $statusHistoriekItem = new WerkOprachtStatusHistorie();
        $werkOprachtStatus = new WerkOprachtStatus();
        $werkOprachtStatus->setId(5);
        $statusHistoriekItem->setWerkOprachtStatus($werkOprachtStatus);
        $this->status = $statusHistoriekItem;
        $this->type = new WerkOprachtType();
        $this->type->setId(1);
        $this->prioriteit = new WerkOprachtPrioriteit();
        $this->prioriteit->setId(1);
    }

    public function jsonSerialize() {
        return [

            "id" => $this->getId(),
            "classType" => $this->getClassType(),
            "titel" => $this->getTitel(),
            "beschrijving" => $this->getBeschrijving(),
            "prioriteit_id" => $this->getPrioriteit()->getId(),
            "prioriteit_naam" => $this->getPrioriteit()->getNaam(),
            "type_id" => $this->getType()->getId(),
            "type_naam" => $this->getType()->getNaam(),
            "status_opmerking" => $this->getStatus()->getOpmerking(),
            "status_id" => $this->getStatus()->getWerkOprachtStatus()->getId(),
            "status_naam" => $this->getStatus()->getWerkOprachtStatus()->getNaam(),
            "modifier_id" => $this->getModificator()->getId(),
            "modifier_email" => $this->getModificator()->getEmail(),
            "modifier_voornaam" => $this->getModificator()->getVoornaam(),
            "modifier_achternaam" => $this->getModificator()->getAchternaam(),
            "creator_id" => $this->getCreator()->getId(),
            "creator_email" => $this->getCreator()->getEmail(),
            "creator_naam" => $this->getCreator()->getVoornaam() . " " . $this->getCreator()->getAchternaam(),
            "creatiedatum" => $this->getCreatiedatum(),
            "begindatum" => $this->begindatum,
            "einddatum" => $this->einddatum,
            "campus" => $this->getLocatie()->getLevelString(0),
            "leefgroep" => $this->getLocatie()->getLevelString(1),
            "locatie" => $this->getLocatie()->getLocationFullPath(),
            "locatie_id" => $this->getLocatie()->getId(),
            "inventaris_id" => $this->getInventaris()->getId()
        ];
    }

}

class WerkBon extends WerkOpdracht implements \JsonSerializable {

    //Array van de klasse Gebruiker : deze array bevat toegewezen gebruikers (wie moet de werkbon uitvoeren ?)
    protected $gebruikersList;
    //Stringarray  : deze array bevat toegewezen groepen (welke groep moet de werkbon uitvoeren ? (electiciteit,bouw,loodgieters,...))
    protected $groepenList;

    function __construct() {
        parent::__construct();
        $this->classType = "WB";
        $this->gebruikersList = array();
        $this->groepenList = array();
        $statusHistoriekItem = new WerkOprachtStatusHistorie();
        $werkOprachtStatus = new WerkOprachtStatus();
        $werkOprachtStatus->setId(1);
        $statusHistoriekItem->setWerkOprachtStatus($werkOprachtStatus);
        $this->status = $statusHistoriekItem;
    }

    public function setParent(WerkOpdracht $wo) {
        $this->parentWerkOpdracht = $wo;
    }

    public function getParent() {
        return $this->parentWerkOpdracht;
    }
    public function setGebruikersList($gebruikersArray) {

        $this->gebruikersList = $gebruikersArray;
    }
    //converteerd een lijst van komma seperated id's in een object lijst van gebruikers
    public function setGebruikersListKommaSep($kommaSepIdString) {

        $this->gebruikersList = null;
        if ($kommaSepIdString) {
            $idArray = explode(',', $kommaSepIdString);

            $gebruikersArray = array();
            foreach ($idArray as $value) {
                if ($value) {
                    $geb = new \clarafey\BLL\gebruikerBLL\Gebruiker();
                    $geb->setId($value);
                    $gebruikersArray[] = $geb;
                    \clarafey\BLL\functions\functions::writeTrace("value : " . $value);
                }
            }
            $this->gebruikersList = $gebruikersArray;
        }
    }
    public function getGebruikersList() {
        return $this->gebruikersList;
    }
    //geeft een komma separeted array terug van de Id's van de toegewezen gebruikers (bijvoorbeeld '2,6,7')
    public function getGebruikersListKommaSeparated() {
        $kommaSepString = "";
        if ($this->gebruikersList) {
            foreach ($this->gebruikersList as $value) {
                if (strlen($kommaSepString) > 0)
                    $kommaSepString.=",";
                $kommaSepString.=$value->getId();
            }
        }
        return $kommaSepString;
    }

    public function getGebruikersNamenListKommaSeparated() {
        $kommaSepString = "";
        foreach ($this->gebruikersList as $value) {
            if (strlen($kommaSepString) > 0)
                $kommaSepString.=",";
            $kommaSepString.=$value->getVoornaam() . " " . $value->getAchternaam();
        }
        return $kommaSepString;
    }
    public function setGroepenList($groepenList) {
        $this->groepenList = $gebruikersArray;
    }
    //converteerd een lijst van komma seperated id's in een array lijst van gebruikers
    public function setGroepenListKommaSep($kommaSepIdString) {
        $this->groepenList = array();
        $idArray = explode(',', $kommaSepIdString);
        $groepenArray = array();
        foreach ($idArray as $value) {
            if ($value) {
                $this->groepenList[] = trim($value);
            }
        }
    }
    public function getGroepenList() {

        return $this->groepenList;
    }
   //geeft een komma separeted array terug van de groepen (bijvoorbeeld 'electriciteit','loodgieter','bouw')
    public function getGroepenListKommaSeparated() {
        $kommaSepString = "";
        foreach ($this->groepenList as $value) {
            if (strlen($kommaSepString) > 0)
                $kommaSepString.=",";
            $kommaSepString.=trim($value);
        }
        return $kommaSepString;
    }

    public function jsonSerialize() {
        return [
            "id" => $this->getId(),
            "classType" => $this->getClassType(),
            "titel" => $this->getTitel(),
            "beschrijving" => $this->getBeschrijving(),
            "prioriteit_id" => $this->getPrioriteit()->getId(),
            "prioriteit_naam" => $this->getPrioriteit()->getNaam(),
            "type_id" => $this->getType()->getId(),
            "type_naam" => $this->getType()->getNaam(),
            "status_id" => $this->getStatus()->getWerkOprachtStatus()->getId(),
            "status_naam" => $this->getStatus()->getWerkOprachtStatus()->getNaam(),
            "status_opmerking" => $this->getStatus()->getOpmerking(),
            "modifier_id" => $this->getModificator()->getId(),
            "modifier_email" => $this->getModificator()->getEmail(),
            "modifier_voornaam" => $this->getModificator()->getVoornaam(),
            "modifier_achternaam" => $this->getModificator()->getAchternaam(),
            "creator_id" => $this->getCreator()->getId(),
            "creator_email" => $this->getCreator()->getEmail(),
            "creator_naam" => $this->getCreator()->getVoornaam() . " " . $this->getCreator()->getAchternaam(),
            "creatiedatum" => $this->getCreatiedatum(),
            "parentid" => $this->getParent()->getId(),
            "begindatum" => $this->getBegindatum(),
            "einddatum" => $this->getEinddatum(),
            "campus" => $this->getLocatie()->getLevelString(0),
            "leefgroep" => $this->getLocatie()->getLevelString(1),
            "locatie" => $this->getLocatie()->getLocationFullPath(),
            "locatie_id" => $this->getLocatie()->getId(),
            "gebruikersids" => array_map(create_function('$o', 'return $o->getId();'), $this->getGebruikersList()), //als nested array van classe gebruikers
            "gebruikersgroepen_kommasep" => $this->getGroepenListKommaSeparated(), //voor het gemak ook de komma-separated string meegeven(handig voor grid)
            "gebruikersnamen_kommasep" => $this->getGebruikersNamenListKommaSeparated(), //voor het gemak ook de komma-separated string meegeven(handig voor grid)
        ];
    }
}
class WerkbonnenBeheerBLL {
   
    //om tegen tegaan dat deze klasse wordt geinstantieerd.
    private function __construct() {
    }
    //deze functie update of insert een Werkaanvraag
    static function saveWerkAanvraag($gebruikerId, Werkaanvraag $werkaanvraag) {

        $retval =  \clarafey\DAL\werkbonnenBeheerDAL\WerkbonnenBeheerDAL::saveWerkopdracht(
                        $gebruikerId, $werkaanvraag->getId(), null/* parentid */
                        , $werkaanvraag->getPrioriteit()->getId(), $werkaanvraag->getType()->getId()
                        , $werkaanvraag->getTitel(), $werkaanvraag->getBeschrijving(), $werkaanvraag->getLocatie()->getId()
                        , $werkaanvraag->getInventaris()->getId(), null/* begindatum */, null/* eindatum */, null, null
        );
        //indien het resultaat geen string is(foutmelding) bevat het resultaat de nieuwe of bestaande id
        if (is_string($retval)==false) {
            $id = $retval;
            $werkaanvraag->setId($id);
        }
        return $retval;
    }
    //deze functie update of insert een nieuwe Werkbon
    static function saveWerkBon($gebruikerId, WerkBon $werkbon) {


        $retval =  \clarafey\DAL\werkbonnenBeheerDAL\WerkbonnenBeheerDAL::saveWerkopdracht(
                        $gebruikerId, $werkbon->getId(), $werkbon->getParent()->getId()
                        , $werkbon->getPrioriteit()->getId(), $werkbon->getType()->getId()
                        , $werkbon->getTitel(), $werkbon->getBeschrijving(), $werkbon->getLocatie()->getId()
                        , $werkbon->getInventaris()->getId(),  $werkbon->getBegindatum(), $werkbon->getEinddatum(), $werkbon->getGebruikersListKommaSeparated(), $werkbon->getGroepenListKommaSeparated()
        );
        //indien het resultaat geen string is(foutmelding) bevat het resultaat de nieuwe of bestaande id
        if (is_string($retval)==false) {
            $id = $retval;
            $werkbon->setId($id);
        }
        return $retval;
    }
    //Deze functie update de status van een Werkbon.
    static function saveWerkOpdrachtStatus(WerkOprachtStatusHistorie $statusHistoriekItem) {
        
  
        return \clarafey\DAL\werkbonnenBeheerDAL\WerkbonnenBeheerDAL::saveWerkOpdrachtStatus($statusHistoriekItem->getWerkopdrachtId(),$statusHistoriekItem->getGebruiker()->getId(),$statusHistoriekItem->getWerkOprachtStatus()->getId()
               , $statusHistoriekItem->getOpmerking()
                );
    }

    static function getWerkAanvraag($id) {
       
        $waList =  self::GetWerkAanvragen($id, null, null, null);
        if (count($waList)>0)
            return $waList[0];
        else return null;
    }
    //deze functie delete een werkopdracht op de databank
    //is enkel mogelijk wanneer de aanvraag de status 'Nieuwe werkaanvraag' heeft
    static function deleteWerkaanvraag($id) {
        
      return  \clarafey\DAL\werkbonnenBeheerDAL\WerkbonnenBeheerDAL::deleteWerkaanvraag ($id);
    
      
    }
    //deze functie delete een werkopdracht op de databank
    //is enkel mogelijk wanneer de aanvraag de status 'Nieuwe werkopdracht' heeft
    static function deleteWerkBon($id) {
     
        return  \clarafey\DAL\werkbonnenBeheerDAL\WerkbonnenBeheerDAL::deleteWerkBon ($id);
    
        
    }
    //deze functie geeft een lijst vab Werkaanvragen
    //id : indien 1 werkaanvraag moeten worden opgehaald
    //locationClusterIds : werkaanvragen van een bepaalde locatie en onderliggende sublocaties
    //groepCode : werkaanvragen van een bepaalde groep medewerkers (directie, boekhouding,...)
    //$userId : werkaanvragen gemaakt door een bepaalde gebruiker
    static function getWerkAanvragen($id, $locatieId, $groepCode, $userId) {
        $dbList = \clarafey\DAL\werkbonnenBeheerDAL\WerkbonnenBeheerDAL::getWerkOprachten($id, "WA", null, $locatieId, $groepCode, $userId, null);
     

        $woList = array();
        foreach ($dbList as $row) {
            $woList[] = self::parseWerkOpdrachtRow("WA", $row);
        }
        return $woList;
    }
    //geeft 1 werkbon terug
    static function getWerkBon($id) {
        $wbList =  self::GetWerkBonnen($id, null, null, null,null);
        
        if (count($wbList)>0)
            return $wbList[0];
        else return null;
        
    }
    //deze functie geeft een lijst vab Werkaanvragen
    //id : indien 1 werkbon moeten worden opgehaald
    //parentid : alle werkbonnen van 1 welbepaalde werkaanvraag
    //locationClusterIds : werkaanvragen van een bepaalde locatie en onderliggende sublocaties
    //groepCode : werkaanvragen van een bepaalde groep medewerkers (directie, boekhouding,...)
    //userId : werkaanvragen gemaakt door een bepaalde gebruiker
    
    static function getWerkBonnen($id, $parentid, $locatieId, $userId, $subGroep) {
        
        $dbList = \clarafey\DAL\werkbonnenBeheerDAL\WerkbonnenBeheerDAL::getWerkOprachten($id, "WB", $parentid, $locatieId, null, $userId, $subGroep);
        $woList = array();
        foreach ($dbList as $row) {
            $woList[] = self::parseWerkOpdrachtRow("WB", $row);
        }
        return $woList;
    }

    static function parseWerkOpdrachtRow($woSoort, $row) {

        if ($woSoort == "WA")
            $wo = new Werkaanvraag();
        else
            $wo = new WerkBon();

        $wo->setId($row["wo_id"]);
        $wo->setTitel($row["wo_titel"]);
        $wo->setBeschrijving($row["wo_beschrijving"]);
        //type
        $type = new WerkOprachtType();
        $type->setId($row["wot_id"]);
        $type->setNaam($row["wot_naam"]);
        $wo->setType($type);
        //status
        $statusHistoriekItem = new WerkOprachtStatusHistorie();
        $st = new WerkOprachtStatus();
        $st->setId($row["wos_id"]);
        $st->setNaam($row["wos_naam"]);
        $gebr = new \clarafey\BLL\gebruikerBLL\Gebruiker();
        $gebr->setid($row["wosh_geb_id"]);
        $statusHistoriekItem->setWerkOprachtStatus($st);
        $statusHistoriekItem->setOpmerking($row["wosh_opmerking"]);
        $statusHistoriekItem->setGebruiker($gebr);
        $wo->setStatus($statusHistoriekItem);
        //prioriteit
        $st = new WerkOprachtPrioriteit();
        $st->setId($row["wop_id"]);
        $st->setNaam($row["wop_naam"]);
        $wo->setPrioriteit($st);

        //creator
        $gb = new \clarafey\BLL\gebruikerBLL\Gebruiker();
        $gb->setId($row["creator_id"]);
        $gb->setVoornaam($row["creator_voornaam"]);
        $gb->setAchternaam($row["creator_achternaam"]);
        $gb->setEmail($row["creator_email"]);
        $gb->setPersoneelscode($row["creator_personeelscode"]);
        $wo->setCreator($gb);

        //modifier
        $gb = new \clarafey\BLL\gebruikerBLL\Gebruiker();
        $gb->setId($row["modifier_id"]);
        $gb->setVoornaam($row["modifier_voornaam"]);
        $gb->setAchternaam($row["modifier_achternaam"]);
        $gb->setEmail($row["modifier_email"]);
        $gb->setPersoneelscode($row["modifier_personeelscode"]);
        $wo->setModificator($gb);


        $locatie = new \clarafey\BLL\gebouwenbeheerBLL\Locatie();
        $locatie->setId($row["wo_loc_id"]);
        $locatie->setNaam($row["loc_naam"]);
        $locatie->InsertLevelStrings($row["locatiepath"]);
        $wo->setLocatie($locatie);
        $inv = new \clarafey\BLL\inventarisBLL\Inventaris();
        $inv = \clarafey\BLL\inventarisBLL\inventarisBLL::ParseInventarisRow($row);
        $wo->setInventaris($inv);
        $bd = \clarafey\BLL\functions\functions::MsSQlToDateTime($row["wo_begindatum"]);
        $wo->setBegindatum($bd);
        $ed = \clarafey\BLL\functions\functions::MsSQlToDateTime($row["wo_einddatum"]);
        $wo->setEinddatum($ed);
        $cd = \clarafey\BLL\functions\functions::MsSQlToDateTime($row["wo_creatiedatum"]);
        $wo->setCreatiedatum($cd);

        if ($wo instanceof WerkBon) {
            $p = new Werkaanvraag();
            //een werkbon heeft altijd een parent werkaanvraag
            $p->setId($row["wo_parent_id"]);
            $wo->setParent($p);
            //een werkbon eventueel heeft toegewezen gebruikers of groepen
            //ze komen komma-seperated binnen in 1 query. Wordt omgevormd tot array
            $wo->setGroepenListKommaSep($row["gebruikersgroepen"]);
            //de gebruikers zitten komma-separated in 1 string. de informatie van elke gebruiker
            //wordt gescheiden door een @-teken. id@voornaam@achternaam
            $gebruikers = $row["gebruikers"];
            //spaties bij komma's verwijderen
            $gebruikers = str_replace(" ,", ",", $gebruikers);
            //converteren naar array
            $gebArray = explode(",", $gebruikers);
            $gebruikersList = array();
            foreach ($gebArray as $gebString) {
                if ($gebString) {
                    $gebParts = explode("@", $gebString);
                    $gebruiker = new \clarafey\BLL\gebruikerBLL\Gebruiker();
                    $gebruiker->setId($gebParts[0]);
                    $gebruiker->setVoornaam($gebParts[1]);
                    $gebruiker->setAchternaam($gebParts[2]);
                    $gebruikersList[] = $gebruiker;
                }
            }
            $wo->setGebruikersList($gebruikersList);
        }

        return $wo;
    }

    static function getTypes() {
        $dbList = \clarafey\DAL\werkbonnenBeheerDAL\WerkbonnenBeheerDAL::getTypes();
        $resultList = array();
        foreach ($dbList as $k => $v) {
            $m = new WerkOprachtType();
            $m->setId($k);
            $m->setNaam($v);
            $resultList[] = $m;
        }
        return $resultList;
    }

    static function getPrioriteiten() {
        $dbList = \clarafey\DAL\werkbonnenBeheerDAL\WerkbonnenBeheerDAL::getPrioriteiten();
        $resultList = array();
        foreach ($dbList as $k => $v) {
            $m = new WerkOprachtPrioriteit();
            $m->setId($k);
            $m->setNaam($v);
            $resultList[] = $m;
        }
        return $resultList;
    }

    ///Geeft de mogelijk statussen terug voor een Werkopdracht
    ///de mogelijke statussen hangen af van soort Werkopdracht
    ///$type : 'WA' of 'WB'
    //$mode : user of null : indien mode=='user' worden statussen die automatisch worden gezet
    static function getWerkOpdrachtStatussen($type,$mode) {
        $dbList = \clarafey\DAL\werkbonnenBeheerDAL\WerkbonnenBeheerDAL::getWerkopdrachtStatussen($type);
        $resultList = array();
        foreach ($dbList as $k => $v) {
            if ($mode == 'user' && ($k==1/*nieuwe werkbon*/ || $k==5 /*nieuwe werkaanvraag*/ || $k==6 /*gelezen*/ ))
                continue;
            $m = new WerkOprachtStatus();
            $m->setId($k);
            $m->setNaam($v);
            $resultList[] = $m;
        }
        return $resultList;
    }
    
    
    static function getWerkopdrachtStatusHistoriek ($woId)
    {
     
        $dbList = \clarafey\DAL\werkbonnenBeheerDAL\WerkbonnenBeheerDAL::getWerkopdrachtStatusHistoriek($woId);
        $resultList = array();
        foreach ($dbList as $row) {
            $historiekItem= new WerkOprachtStatusHistorie();
            $gebruiker = new \clarafey\BLL\gebruikerBLL\Gebruiker();
            $status = new WerkOprachtStatus();
            $status->setId($row["wos_id"]);
            $status->setNaam($row["wos_naam"]);
            $historiekItem->setWerkOprachtStatus($status);     
            $gebruiker->setId($row["geb_id"]);
            $gebruiker->setAchternaam($row["geb_achternaam"]);
            $gebruiker->setVoornaam($row["geb_voornaam"]);
            $historiekItem->setGebruiker($gebruiker);
            $historiekItem->setOpmerking($row["wosh_opmerking"]);
            $wijzigingsdatum =  \clarafey\BLL\functions\functions::MsSQlToDateTime($row["wosh_datum"]);
            $historiekItem->setDatum($wijzigingsdatum);
            $resultList[] = $historiekItem;
        }
        return $resultList;
        
    }

}
