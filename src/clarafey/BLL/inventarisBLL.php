<?php

namespace clarafey\BLL\inventarisBLL;

require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'DAL' . DIRECTORY_SEPARATOR . 'inventarisDAL.php';
require_once 'gebouwenbeheerBLL.php';
require_once 'functions.php';

class Merk implements \JsonSerializable {

    protected $id;
    protected $naam;

    public function setNaam($naam) {
        $this->naam = $naam;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getId() {
        return $this->id;
    }

    public function getNaam() {
        return $this->naam;
    }

    public function __toString() {
        return $this->naam;
    }

    public function jsonSerialize() {
        return [


            "id" => $this->getId(),
            "naam" => $this->getNaam()
        ];
    }

}

class Toestel implements \JsonSerializable {

    protected $id;
    protected $naam;
    protected $categorie;

    public function setNaam($naam) {
        $this->naam = $naam;
    }

    public function getNaam() {
        return $this->naam;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getId() {
        return $this->id;
    }

    public function getCategorie() {
        return $this->categorie;
    }

    public function setCategorie($categorie) {
        $this->categorie = $categorie;
    }

    public function __toString() {
        return $this->naam;
    }

    public function jsonSerialize() {
        return [
            "id" => $this->getId(),
            "naam" => $this->getNaam(),
            "categorie" => $this->getCategorie()
        ];
    }

}

class Inventaris implements \JsonSerializable {

    //uit db 263
    protected $nummer263;
    protected $produktOmschrijving263;
    protected $factuurDatum263;
    protected $groep263;
    protected $investering263;
    //uit DB Clara Fey
    protected $id = null;
    protected $produktOmschrijving;
    protected $serienummer;
    protected $macAdres;
    protected $ipAdres;
    protected $toestelId;
    protected $merkId;
    protected $locatieId;
    protected $leverancierId;
    protected $produktCode;
    protected $bestelbonNummer;
    protected $deviceLogin;
    protected $deviceWachtwoord;
    protected $uitDienstDatum;
    protected $aankoopDatum;
    protected $label;
    protected $opmerking;
    protected $merk;
    protected $toestel;
    protected $leverancier;
    protected $locatie;

    function __construct() {

        $merk = new Merk();
        $toestel = new Toestel();
        $leverancier = new Leverancier();
        $locatie = new \clarafey\BLL\gebouwenbeheerBLL\Locatie();
    }

    public function setLocatie(\clarafey\BLL\gebouwenbeheerBLL\Locatie $locatie) {
        $this->locatie = $locatie;
    }

    public function getLocatie() {
        return $this->locatie;
    }

    public function setToestel(Toestel $toestel) {
        $this->toestel = $toestel;
    }

    public function setMerk(Merk $merk) {
        $this->merk = $merk;
    }

    public function getMerk() {
        return $this->merk;
    }

    public function getToestel() {
        return $this->toestel;
    }

    public function setLeverancier(Leverancier $leverancier) {
        $this->leverancier = $leverancier;
    }

    public function getLeverancier() {
        return $this->leverancier;
    }

    public function setNummer263($nummer) {
        $this->nummer263 = $nummer;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setProduktCode($code) {
        $this->produktCode = $code;
    }

    public function getProduktCode() {
        return $this->produktCode;
    }

    public function setOpmerking($text) {
        $this->opmerking = $text;
    }

    public function getOpmerking() {
        return $this->opmerking;
    }

    public function setUitdienstDatum($datum) {
        $this->uitDienstDatum = $datum;
    }

    public function getUitDienstDatum() {
        return $this->uitDienstDatum;
    }

    public function setAankoopDatum($datum) {
        $this->aankoopDatum = $datum;
    }

    public function getAankoopDatum() {

        return $this->aankoopDatum;
    }

    public function setDeviceLogin($login) {
        $this->deviceLogin = $login;
    }

    public function getDeviceLogin() {
        return $this->deviceLogin;
    }

    public function setDeviceWachtwoord($login) {
        $this->deviceWachtwoord = $login;
    }

    public function getDeviceWachtwoord() {
        return $this->deviceWachtwoord;
    }

    public function setBestelbonNummer($nr) {
        $this->bestelbonNummer = $nr;
    }

    public function getBestelbonNummer() {
        return $this->bestelbonNummer;
    }

    public function setLabel($label) {
        $this->label = $label;
    }

    public function getLabel() {
        return $this->label;
    }

    public function setIpAdres($ip) {
        $this->ipAdres = $ip;
    }

    public function getIpadres() {
        return $this->ipAdres;
    }

    public function setMacAdres($adres) {
        $this->macAdres = $adres;
    }

    public function getMacAdres() {
        return $this->macAdres;
    }

    public function setToestelId($id) {
        $this->toestelId = $id;
    }

    public function setLocatieId($id) {
        $this->locatieId = $id;
    }

    public function setMerkId($id) {
        $this->merkId = $id;
    }

    public function setLeverancierId($id) {
        $this->leverancierId = $id;
    }

    public function getToestelId() {
        return $this->toestelId;
    }

    public function getLocatieId() {
        return $this->locatieId;
    }

    public function getMerkId() {
        return $this->merkId;
    }

    public function getNummer263() {
        return $this->nummer263;
    }

    public function getLevernacierId() {
        return $this->leverancierId;
    }

    public function setSerienummer($serienummer) {
        $this->serienummer = $serienummer;
    }

    public function getSerienummer() {
        return $this->serienummer;
    }

    public function setProduktOmschrijving263($omschrijving) {
        $this->produktOmschrijving263 = $omschrijving;
    }

    public function getProduktOmschrijving263() {
        return $this->produktOmschrijving263;
    }

    public function setProduktOmschrijving($omschrijving) {
        $this->produktOmschrijving = $omschrijving;
    }

    public function getProduktOmschrijving() {
        return $this->produktOmschrijving;
    }

    public function setFactuurDatum263($datum) {
        $this->factuurDatum263 = $datum;
    }

    public function setGroep263($groep) {
        $this->groep263 = $groep;
    }

    public function getGroep263() {
        return $this->groep263;
    }

    public function setInvestering263($investering) {
        $this->investering263 = $investering;
    }

    public function getFactuurDatum263() {
        return $this->factuurDatum263;
    }

    public function getInvestering263() {
        return $this->investering263;
    }

    public function getId() {
        return $this->id;
    }

    public function IsInventarised() {
        return !empty($this->id);
    }

    /* voor phpGrid activeren */

    public function jsonSerialize() {
        return [

            //"editeren" => "<a href='/inventaris/editeren/?nummer263=" . $this->nummer263 . "'><span class='glyphicon glyphicon-edit' aria-hidden='true'></span></a>",
            "id" => $this->getId(),
            "inv" => $this->IsInventarised(),
            "inv_text" => $this->IsInventarised() ? "ja" : "nee",
            "groep" => $this->groep263,
            "nummer" => $this->nummer263,
            "omschrijving" => $this->IsInventarised() ? $this->produktOmschrijving : $this->produktOmschrijving263,
            "aankoopdatum" => $this->IsInventarised() ? $this->aankoopDatum : $this->factuurDatum263,
            "isuitdienst" => ($this->getUitDienstDatum() != null),
            "isuitdienst_text" => ($this->getUitDienstDatum() != null) ? "ja" : "nee",
            "investering" => round($this->investering263, 2),
            "leverancier" => $this->leverancier->getNaam(),
            "label" => $this->label,
            "toestel" => $this->toestel->getNaam(),
            "merk" => $this->merk->getNaam(),
            "serienummer" => $this->getSerienummer(),
            "campus" => $this->getLocatie()->getLevelString(0),
            "leefgroep" => $this->getLocatie()->getLevelString(1),
            "locatie" => $this->getLocatie()->getLocationFullPath()
        ];
    }

}

class Leverancier implements \JsonSerializable {

    protected $id;
    protected $naam;

    public function setNaam($naam) {
        $this->naam = $naam;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getId() {
        return $this->id;
    }

    public function getNaam() {
        return $this->naam;
    }

    public function __toString() {
        return $this->naam;
    }

    public function jsonSerialize() {
        return [


            "id" => $this->getId(),
            "naam" => $this->getNaam()
        ];
    }

}

class inventarisBLL {

    //om tegen tegaan dat deze klasse wordt geinstantieerd.
    private function __construct() {
        
    }

    static function iuInventaris($gebruikerId, Inventaris $inventarisObj) {
        return \clarafey\DAL\inventarisDAL\inventarisDAL::uiInventaris(
                        $gebruikerId, $inventarisObj->getId(), $inventarisObj->getNummer263(), $inventarisObj->getProduktOmschrijving(), $inventarisObj->getSerienummer(), $inventarisObj->getMacAdres(), $inventarisObj->getIpadres(), $inventarisObj->getToestelId(), $inventarisObj->getMerkId(), $inventarisObj->getLocatieId(), $inventarisObj->getLevernacierId(), $inventarisObj->getProduktCode(), $inventarisObj->getBestelbonNummer(), $inventarisObj->getDeviceLogin(), $inventarisObj->getDeviceWachtwoord(), $inventarisObj->getUitDienstDatum(), $inventarisObj->getAankoopDatum(), $inventarisObj->getLabel(), $inventarisObj->getOpmerking()
        );
    }

    static function saveMerk(Merk $merk) {

        $result = \clarafey\DAL\inventarisDAL\inventarisDAL::saveMerk($merk->getId(), $merk->getNaam());
        if (is_string($result))
            return $result;
        else {
            $merk->setId($result);
            return $merk;
        }
    }

    static function saveLeverancier(Leverancier $lev) {

        $result = \clarafey\DAL\inventarisDAL\inventarisDAL::saveLeverancier($lev->getId(), $lev->getNaam());
        if (is_string($result))
            return $result;
        else {
            $lev->setId($result);
            return $lev;
        }
    }

    static function saveToestel(Toestel $toestel) {


        $result = \clarafey\DAL\inventarisDAL\inventarisDAL::saveToestel($toestel->getId(), $toestel->getNaam(), $toestel->getCategorie());
        if (is_string($result))
            return $result;
        else {
            $toestel->setId($result);
            return $toestel;
        }
    }

    static function GetInventarisLijstJson($zoektekst, $status) {

        $invList = self::GetInventarisLijst($zoektekst, $status);
        header('Content-type: application/json');
        // header('Access-Control-Allow-Origin: *');
        echo json_encode($invList);
    }

    static function GetInventarisItems($locatieId) {


        $dbList = \clarafey\DAL\inventarisDAL\inventarisDAL::GetInventarisItems($locatieId);

        $invList = array();
        foreach ($dbList as $row) {
            $inv = new Inventaris();
            $invList[] = self::ParseInventarisRow($row);
        }


        return $invList;
    }

    static function GetInventarisLijst($zoektekst, $status) {


        $dbList = \clarafey\DAL\inventarisDAL\inventarisDAL::GetInventaris(null, $zoektekst, $status);

        $invList = array();
        foreach ($dbList as $row) {
            $inv = new Inventaris();
            $invList[] = self::ParseInventarisRow($row);
        }
        return $invList;
    }

    static function GetInventaris($inv263) {

        $dbList = \clarafey\DAL\inventarisDAL\inventarisDAL::GetInventaris($inv263, null, null);
        return self::ParseInventarisRow($dbList[0]);
    }

    static function ParseInventarisRow($row) {


        $inv = new Inventaris();
        $inv->setId($row["inv_id"]);
        if (array_key_exists("Serienummer", $row))
            $inv->setNummer263($row["Serienummer"]); //uit boekhoudingstabel
        else
            $inv->setNummer263($row["inv_263_nummer"]);  //uit onze inventaris tabel

        if (array_key_exists("Omschrijving", $row))
            $inv->setProduktOmschrijving263($row["Omschrijving"]);

        if (array_key_exists("inv_productomschrijving", $row))
            $inv->setProduktOmschrijving($row["inv_productomschrijving"]);

        if (array_key_exists("inv_serienummer", $row))
            $inv->setSerienummer($row["inv_serienummer"]);
        if (array_key_exists("inv_macadres", $row))
            $inv->setMacAdres($row["inv_macadres"]);
        if (array_key_exists("inv_ipadres", $row))
            $inv->setIpadres($row["inv_ipadres"]);
        if (array_key_exists("inv_tst_id", $row))
            $inv->setToestelId($row["inv_tst_id"]);
        if (array_key_exists("inv_mer_id", $row))
            $inv->setMerkId($row["inv_mer_id"]);
        if (array_key_exists("inv_loc_id", $row))
            $inv->setLocatieId($row["inv_loc_id"]);
        if (array_key_exists("inv_lev_id", $row))
            $inv->setLeverancierId($row["inv_lev_id"]);
        if (array_key_exists("inv_productcode", $row))
            $inv->setProduktCode($row["inv_productcode"]);
        if (array_key_exists("inv_bestelbon", $row))
            $inv->setBestelbonNummer($row["inv_bestelbon"]);
        if (array_key_exists("inv_devicelogin", $row))
            $inv->setDeviceLogin($row["inv_devicelogin"]);
        if (array_key_exists("inv_devicewachtwoord", $row))
            $inv->setDeviceWachtwoord($row["inv_devicewachtwoord"]);
        if (array_key_exists("inv_label", $row))
            $inv->setLabel($row["inv_label"]);
        if (array_key_exists("inv_aankoopdatum", $row))
            $inv->setAankoopDatum(\clarafey\BLL\functions\functions::MsSQlToDate($row["inv_aankoopdatum"]));
        if (array_key_exists("inv_uitdienst", $row))
            $inv->setUitDienstDatum(\clarafey\BLL\functions\functions::MsSQlToDate($row["inv_uitdienst"]));
        if (array_key_exists("inv_opmerking", $row))
            $inv->setOpmerking($row["inv_opmerking"]);
           $merk = new Merk();
        if (array_key_exists("mer_id", $row)) {
         
            $merk->setId($row["inv_mer_id"]);
            $merk->setNaam($row["mer_naam"]);
            
        }
        $inv->setMerk($merk);
             $toestel = new Toestel();
        if (array_key_exists("tst_id", $row)) {
       
            $toestel->setId($row["inv_tst_id"]);
            $toestel->setNaam($row["tst_naam"]);
         
        }
   $inv->setToestel($toestel);

 $locatie = new \clarafey\BLL\gebouwenbeheerBLL\Locatie();
        if (array_key_exists("loc_id", $row))  {
               
                $locatie->setId($row["inv_loc_id"]);
                $locatie->setNaam($row["loc_naam"]);
                $locatie->InsertLevelStrings($row["locatiepath"]);
           
            }
                 $inv->setLocatie($locatie);
   $leverancier = new Leverancier();
            if (array_key_exists("lev_id", $row)) {

             
                $leverancier->setId($row["inv_lev_id"]);
                $leverancier->setNaam($row["lev_naam"]);
              
            }
  $inv->setLeverancier($leverancier);
            if (array_key_exists("Factuurdatum", $row)) {
                $factuurDatum = \clarafey\BLL\functions\functions::MsSQlToDate($row["Factuurdatum"]);
                $inv->setFactuurDatum263($factuurDatum);
            }
            if (array_key_exists("Groep", $row))
                $inv->setGroep263($row["Groep"]);
            if (array_key_exists("Investering", $row))
                $inv->setInvestering263($row["Investering"]);

            return $inv;
        }

        static function GetToestellen() {
            $dbList = \clarafey\DAL\inventarisDAL\inventarisDAL::GetToestellen();

            $toestelList = array();
            foreach ($dbList as $row) {
                $m = new Toestel();
                $m->setId($row["tst_id"]);
                $m->setNaam($row["tst_naam"]);
                $m->setCategorie($row["tst_cat"]);
                $toestelList[] = $m;
            }

            return $toestelList;
        }

        /* static function GetToestellenKeyValue() {
          return \clarafey\DAL\inventarisDL\inventarisDL::GetToestellen();
          } */

        static function GetMerkenKeyValue() {
            return \clarafey\DAL\inventarisDAL\inventarisDAL::GetMerken();
        }

        static function GetLeverancierKeyValue() {
            return \clarafey\DAL\inventarisDAL\inventarisDAL::GetLeverancier();
        }

        static function GetMerken() {
            $dbList = \clarafey\DAL\inventarisDAL\inventarisDAL::GetMerken();
            $merkenList = array();
            foreach ($dbList as $k => $v) {
                $m = new Merk();
                $m->setId($k);
                $m->setNaam($v);
                $merkenList[] = $m;
            }
            return $merkenList;
        }

        static function GetLeverancier() {
            $dbList = \clarafey\DAL\inventarisDAL\inventarisDAL::GetLeverancier();
            $levList = array();
            foreach ($dbList as $k => $v) {
                $m = new Leverancier();
                $m->setId($k);
                $m->setNaam($v);
                $levList[] = $m;
            }
            return $levList;
        }
    }
    