<?php

namespace clarafey\BLL\functions;

class functions {

    public static function sendMail ($app, $subject,$body,$destEmail)
    {
        
     $message = \Swift_Message::newInstance()
        ->setSubject($subject)
        ->setFrom(array('clarafey@fracarita.com'))
        ->setTo(array($destEmail))
        ->setBody($body);

    $app['mailer']->send($message);
 
    }
    
    public static function MsSQlToDate($dbDateValue) {


        $datum = null;
        if ($dbDateValue != null) {
            $datum = \DateTime::createFromFormat('Y-m-d', $dbDateValue);
            $datum->setTime(0, 0, 0);
        }
        return $datum;
    }

    public static function MsSQlToDateTime($dbDateValue) {


        $datum = null;
        if ($dbDateValue != null) {
            $datum = \DateTime::createFromFormat('Y-m-d H:i:s.u', $dbDateValue);
            //$datum->setTime(0, 0, 0);
        }
        return $datum;
    }

    //formatteerd een String in javaDate (bijvoorbeeld : 2015-03-18T10:30:00)
    //naar een PHP DateTime Object
    public static function JavaDateTimeToPHP($javaDate) {
        if (!empty($javaDate)) {
            $split = explode("T", $javaDate);
            $date = $split[0];
            $time = explode(".", $split[1])[0]; //het tijdsgedeelte na de T
            $phpDate = \DateTime::createFromFormat("Y-m-d H:i:s", $date . " " . $time);
            return $phpDate;
        } else
            return null;
    }

    //converteerd een PHP DateTime Object naar Javascript DateTime String (2015-03-18T10:30:00)
    public static function PHPDateTimeToJavaDate($phpDate) {

        if (!empty($phpDate)) {
            $javaDate = $phpDate->format('Y-m-d') . "T" . $phpDate->format('H:i:s').".000Z";
            return $javaDate;
        } else
            return null;
    }

    public static function writeTrace($message) {
        \file_put_contents('traceLog.txt', PHP_EOL . $message, FILE_APPEND);
    }
    
    
    

}
