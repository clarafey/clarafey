<?php

namespace clarafey\BLL\dashboardBLL;

require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'DAL' . DIRECTORY_SEPARATOR . 'dashboardDAL.php';
 
require_once 'functions.php';

class DashboardItem implements \JsonSerializable {

   
    protected $aantal;
    protected $type;
    protected $module;
 

    function __construct() {
         }

    public function setAantal($aantal) {
        $this->aantal = $aantal;
    }

    public function getAantal() {
        return $this->aantal;
    }
    public function setType($type) {
        $this->type = $type;
    }

    public function getType() {
        return $this->type;
    }
      public function setModule($module) {
        $this->module = $module;
    }

    public function getModule() {
        return $this->module;
    }
     public function jsonSerialize() {
        return [
            "aantal" => $this->getAantal(),
            "type" => $this->getType(),
            "module" => $this->getModule()
        ];
    }
}
class dashboardBLL {

    //om tegen tegaan dat deze klasse wordt geinstantieerd.
    private function __construct() {
        
    }
    static function getDashboardItems() {
        $dbList = \clarafey\DAL\dashboardDAL\dashboardDAL::getDashboardItems();
  // var_dump($dbList);
        $dashBoardList = array();
        foreach ($dbList as $row) {
 
            $item = new DashboardItem();
            $item->setAantal($row["aantal"]);
            $item->setType($row["type"]);
            $item->setModule($row["module"]);
            $dashBoardList[] =  $item;
        }
        return $dashBoardList;
       
    }
    
}
