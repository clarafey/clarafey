<?php

namespace clarafey\BLL\gebouwenbeheerBLL;

require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'DAL' . DIRECTORY_SEPARATOR . 'gebouwenbeheerDAL.php';

class Locatie implements \JsonSerializable {

    //uit db 263
    protected $id;
    protected $parentId;
    protected $naam;
    protected $beschrijving;
    protected $opmerking;
    protected $level;
    protected $levelStrings;

    function __construct() {
        $this->levelStrings = array();
        $this->levelStrings [] = "";
        $this->levelStrings [] = "";
        $this->levelStrings [] = "";
        $this->levelStrings [] = "";
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getId() {
        return $this->id;
    }
    public function InsertLevelStrings($dbString) {

        $this->levelStrings = explode("@@", $dbString);

        $this->levelStrings [] = "";
        $this->levelStrings [] = "";
        $this->levelStrings [] = "";
        $this->levelStrings [] = "";
    }

    public function getLevelString($level) {
        return $this->levelStrings[$level];
    }

    public function getLocationFullPath() {
        $str = "";
        foreach ($this->levelStrings as $l) {

            if (!empty($l)) {
                if (!empty($str)) {
                    $str.= "->";
                }
                    $str.=$l;
            }
        }
        return $str;
    }
    public function setParentId($parentId) {
        $this->parentId = $parentId;
    }
    public function getParentId() {
        return $this->parentId;
    }
    public function setNaam($naam) {
        $this->naam = $naam;
    }
    public function getNaam() {
        return $this->naam;
    }
    public function setLevel($level) {
        $this->level = $level;
    }
    public function getLevel() {
        return $this->level;
    }
    public function setBeschrijving($beschrijving) {
        $this->beschrijving = $beschrijving;
    }

    public function getBeschrijving() {
        return $this->beschrijving;
    }

    public function setOpmerking($text) {
        $this->opmerking = $text;
    }

    public function getOpmerking() {
        return $this->opmerking;
    }

    public function jsonSerialize() {
        return [

            "id" => $this->id . "",
            "parentid" => empty($this->parentId) ? -1 . "" : $this->parentId . "",
            "naam" => $this->naam . " " . $this->beschrijving . " ",
            "level" => $this->level
        ];
    }

}

class gebouwenbeheerBLL {

    //om tegen tegaan dat deze klasse wordt geinstantieerd.
    private function __construct() {
        
    }

    static function iuLocatie($id, $parentid, $naam, $beschrijving, $opmerking) {

        return \clarafey\DAL\gebouwenbeheerDAL\gebouwenbeheerDAL::iuLocatie($id, $parentid, $naam, $beschrijving, $opmerking);
    }

    static function deleteLocatie($id) {

        return \clarafey\DAL\gebouwenbeheerDAL\gebouwenbeheerDAL::deleteLocatie($id);
    }
    static function GetClusterOfPaviljoen($locationId)
    {
        if (empty($locationId))
            return null;
        else
        {
         return  \clarafey\DAL\gebouwenbeheerDAL\gebouwenbeheerDAL::GetClusterOfPaviljoen($locationId);
          
          
        }     
            
    }
    //Deze functie geeft alle campussen terug.
    //Campussen zijn locaties zonder parent, maw hoogste niveau(Kristus Koning, Sint Raphael,.... )
    static function GetCampussen() {
        
        
     
        
        $dbList =  \clarafey\DAL\gebouwenbeheerDAL\gebouwenbeheerDAL::GetCampussen();
        $campusList = array();
        foreach ($dbList as $row) {
          $campusList[] = self::ParseLocatieRow($row);
        }
        return $campusList;
    }
    static function GetLocatie($id) {

        return \clarafey\DAL\gebouwenbeheerDAL\gebouwenbeheerDAL::GetLocatie($id);
    }

    static function GetLocatieTreeForForm() {
        $locTree = \clarafey\DAL\gebouwenbeheerDAL\gebouwenbeheerDAL::GetLocatieTree();
        $newTree = array();
        foreach ($locTree as $row) {

            $descr = str_repeat("-----------", $row["Level"]) . $row["loc_naam"] . " " . $row["loc_beschrijving"];
            $newTree[$row["loc_id"]] = $descr;
        }
        return $newTree;
    }

    static function GetLocaties() {

        return \clarafey\DAL\gebouwenbeheerDAL\gebouwenbeheerDAL::GetLocaties();
    }

    static function GetLocatieTreeStraightFromDB() {

        return \clarafey\DAL\gebouwenbeheerDAL\gebouwenbeheerDAL::GetLocatieTree();
    }

    static function GetLocatieTreeJson() {
        $locTree = self::GetLocatieTree();
        header('Content-type: application/json');
        header('Access-Control-Allow-Origin: *');
        echo json_encode($locTree);
    }
   
    
    //Helper functie voor GetLocatieTreeJsonNested.
     private static function GetLocatieTreeJsonNested_AddChilds($locTree, $node) {
 
     
        $childs = array();
        foreach ($locTree as $loc )
        {   
   
           $parentid  = $loc->getParentId();
           if (!empty($parentid))
           {
               if ($parentid==$node["id"])
               {
                   $d=array("id"=>$loc->getId(),"parentid"=>$loc->getParentId(),"naam"=>$loc->getNaam(),"items"=>array());
                   $d["items"]=self::GetLocatieTreeJsonNested_AddChilds($locTree, $d);
                   $childs[]=$d;
                }
            }
        }
        return $childs;
      
 }
 
 //Deze functie converteerd de locatieboom met parent-child-relation 
 //naar een JSON met nested arrays. Nodig voor Kendo UI TreeView
  static function GetLocatieTreeJsonNested() {
        $locTree = self::GetLocatieTree();
        $data=array();
        $i=0;
        foreach ($locTree as $loc)
        {    
           $parent = $loc->getParentId();
           if (empty($parent))
           { 
             $d = array("id"=>$loc->getId(),"parentid"=>$loc->getParentId(),"naam"=>$loc->getNaam(),"items"=>array());
            $d["items"] = self::GetLocatieTreeJsonNested_AddChilds($locTree,$d);
             $data[]=$d;
          
           }
           $i++; 
        }
        return json_encode($data);  
}
 


    static function GetLocatieTree() {

        $locTree = \clarafey\DAL\gebouwenbeheerDAL\gebouwenbeheerDAL::GetLocatieTree();

        $locList = array();
        foreach ($locTree as $row) {

            $locList[] = self::ParseLocatieRow($row);
        }
        return $locList;
    }

    static function ParseLocatieRow($row) {


        $loc = new Locatie();
        $loc->setId($row["loc_id"]);
        $loc->setParentId($row["loc_parent_id"]);
        $loc->setNaam($row["loc_naam"]);
        $loc->setBeschrijving($row["loc_beschrijving"]);
        if (array_key_exists("Level",$row))
            $loc->setLevel($row["Level"]);
        return $loc;
    }

}   
