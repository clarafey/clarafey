<?php

namespace clarafey\BLL\globalData;

 

class cBreadCrump {

    protected $url;
    protected $text;
    function __construct($url,$text) {
        $this->url = $url;
        $this->text = $text;
    }
    public function getUrl()
    {
        return $this->url;
    }
    public function getText()
    {
        return $this->text;
    }
}
class breadCrumpManagerClass {

     private static $_instance = null;
     private $breadCrumps = array();
     public static function getInstance() {

        if (self::$_instance == null) {
            self::$_instance = new breadCrumpManagerClass();
        }
        return self::$_instance;
    }
    function __construct() {
        $this->breadCrumps["INDEX"][]  = new cBreadCrump ("http://" . $_SERVER['HTTP_HOST'],"home");
        $this->breadCrumps["INLOGGEN"][]=new cBreadCrump ("http://" . $_SERVER['HTTP_HOST'],"home");
        $this->breadCrumps["INLOGGEN"][]=new cBreadCrump ("http://" . $_SERVER['HTTP_HOST'] . "/registratie/","registratie");
        $this->breadCrumps["INVENTARIS"][]=new cBreadCrump ("http://" . $_SERVER['HTTP_HOST'],"home");
        $this->breadCrumps["INVENTARIS"][]=new cBreadCrump ("http://" . $_SERVER['HTTP_HOST'] . "/inventaris/","inventaris");
        $this->breadCrumps["SMM-LOCATIES"][]=new cBreadCrump ("http://" . $_SERVER['HTTP_HOST'],"home");
        $this->breadCrumps["SMM-LOCATIES"][]=new cBreadCrump (null,"smm");
        $this->breadCrumps["SMM-LOCATIES"][]=new cBreadCrump ("http://" . $_SERVER['HTTP_HOST'] . "/smm/gebouwenbeheer/","gebouwenbeheer");
      
        
        $this->breadCrumps["ACCOUNTGEGEVENS"][]=new cBreadCrump ("http://" . $_SERVER['HTTP_HOST'],"home");
        $this->breadCrumps["ACCOUNTGEGEVENS"][]=new cBreadCrump ("http://" . $_SERVER['HTTP_HOST'] . "/registratie/accountgegevens/","accountgegevens");
        
    }
    function GetBreadCrumps($def)
    {
        return $this->breadCrumps[$def];
    }
}


class globalData {

    private $app;
    private static $_instance = null;
    private $breadCrumps;
    public static function getInstance($app) {

        if (self::$_instance == null) {
            self::$_instance = new registrationClass($app);
        }
        return self::$_instance;
    }
    
    function __construct($app) {

        $this->app = $app;
        

    }
}