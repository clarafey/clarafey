<?php

namespace clarafey\DAL\gebruikerDAL;

require_once 'dataBase.php';

use PDO;

class gebruikerDAL {

    //om tegen tegaan dat deze klasse wordt geinstantieerd.  
    private function __construct() {
        
    }

    static function updateAccount($gebruikersId, $password, $email) {

        $db = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();
        $stmt = $db->prepare('update gebruiker set geb_wachtwoord = ?,geb_email = ? where geb_id=?');
        try {




            $stmt->execute(array($password, $email, $gebruikersId));
            $affectedRows = $stmt->rowCount();
            if ($affectedRows == 1)
                return true;
            else
                return false;
        } catch (\PDOException $e) {
            \clarafey\DAL\database\DB::writeError('gebruikerDAL', 'updateAccount', $stmt, $e->getMessage());
            return $e->getMessage();
        }
    }

    static function insertAccount($persNr, $password, $email, $voornaam, $achternaam) {
        try {
            $db = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();
            $stmt = $db->prepare('insert into gebruiker (geb_personeelscode,geb_wachtwoord,geb_email,geb_voornaam,geb_achternaam) values (?,?,?,?,?)');
            $stmt->execute(array($persNr, $password, $email, $voornaam, $achternaam));
            return $db->lastInsertId();
        } catch (\PDOException $e) {

            return $e->getMessage();
        }
    }

    static function SetLogonDetails($gebruikersId) {

        try {

            $cDate = new \DateTime('NOW');
            $formattedDate = $cDate->format('Y-m-d H:i:s');
            $db = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();
            $stmt = $db->prepare('update gebruiker set geb_lastlogindatum = ? where geb_id = ?');
            $stmt->execute(array($formattedDate, $gebruikersId));

            return $db->lastInsertId();
        } catch (\PDOException $e) {

            return $e->getMessage();
        }
    }

    public static function GetGroepen() {
        $sqlstmt = 'select * from rechten_groep';


        try {
            $db = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();
            $stmt = $db->prepare($sqlstmt);
            $stmt->execute();
            return $stmt->fetchAll();
        } catch (\PDOException $e) {
            //self::WriteError($e->getMessage(), $sqlstmt);
            exit($e->getMessage());
        }
    }

    public static function GetFuncties() {
        $sqlstmt = 'select * from rechten_functie';


        try {
            $db = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();
            $stmt = $db->prepare($sqlstmt);
            $stmt->execute();
            return $stmt->fetchAll();
        } catch (\PDOException $e) {
            //self::WriteError($e->getMessage(), $sqlstmt);
            exit($e->getMessage());
        }
    }

    //Deze functie geeft alle groepen en bijhorende functies terug.
    //voor o.a overzicht in SMM->groepenfuncties
    public static function GetGroepFuncties() {
        $sqlstmt = '
select * from rechten_groep,rechten_functie,rechten_groepenfuncties
where groep_code = groepfunc_groep_code and func_code = groepfunc_func_code';


        try {
            $db = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();
            $stmt = $db->prepare($sqlstmt);
            $stmt->execute();
            return $stmt->fetchAll();
        } catch (\PDOException $e) {
            //self::WriteError($e->getMessage(), $sqlstmt);
            exit($e->getMessage());
        }
    }

    public static function GetGroepLocaties() {
        $sqlstmt = 'select * from rechten_groep,locatie,rechten_groepenlocaties
where groep_code = groeploc_groep_code and loc_id = groeploc_loc_id';


        try {
            $db = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();
            $stmt = $db->prepare($sqlstmt);
            $stmt->execute();
            return $stmt->fetchAll();
        } catch (\PDOException $e) {
            //self::WriteError($e->getMessage(), $sqlstmt);
            exit($e->getMessage());
        }
    }

    public static function GetOverzichtGebruikersSubGroepen($groepCode) {

        $paramsArray = array();
        $sqlstmt = 'select distinct geb_subgroep as code ,geb_subgroep as naam  from vw_gebruikergroep where 1=1 ';
        if (!empty($groepCode)) {
            $paramsArray[] = $groepCode;
            $sqlstmt .= ' and groep_code=?';
        }
        $sqlstmt .= ' order by geb_subgroep';
        try {
            $db = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();
            $stmt = $db->prepare($sqlstmt);
            $stmt->execute($paramsArray);
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

            return $result;
        } catch (\PDOException $e) {
            //self::WriteError($e->getMessage(), $sqlstmt);
            exit($e->getMessage());
        }
    }

    public static function SaveGebruikersInfo($id, $email, $telefoon, $gsm, $functieBeschrijving,$werkplaats) {
        $conn = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();
        $sql = "";
        try {
            $sql = "update gebruiker set geb_email=:email,geb_telefoon=:telefoon,geb_gsm=:gsm,geb_functiebeschrijving=:functieBeschrijving,geb_werkplaats=:werkplaats where geb_id = :id";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam('id', $id, PDO::PARAM_INT);
            $stmt->bindParam('email', $email, PDO::PARAM_STR);
            $stmt->bindParam('telefoon', $telefoon, PDO::PARAM_STR);
            $stmt->bindParam('gsm', $gsm, PDO::PARAM_STR);
            $stmt->bindParam('functieBeschrijving', $functieBeschrijving, PDO::PARAM_STR);
            $stmt->bindParam('werkplaats', $werkplaats, PDO::PARAM_STR);
            $stmt->execute();
            //if (empty($id))
            //    $id = intval($conn->lastInsertId()); //komt als string binnen
            return $id;
        } catch (\PDOException $e) {


            \clarafey\DAL\database\DB::writeError('gebruikerDAL', 'SaveGebruikersInfo', '', $sql, " id = " . $id, $e->getMessage());

            return $e->getMessage();
        }
    }

    public static function GetOverzichtGebruikersGroepen($groepCode) {



        $paramsArray = array();
        $sqlstmt = 'select * from vw_gebruikergroep where 1=1 ';
        if (!empty($groepCode)) {
            $paramsArray[] = $groepCode;
            $sqlstmt .= ' and groep_code=?';
        }
        $sqlstmt .= ' order by groep_code,geb_achternaam';
        try {
            $db = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();
            $stmt = $db->prepare($sqlstmt);
            $stmt->execute($paramsArray);
            $result = $stmt->fetchAll();

            return $result;
        } catch (\PDOException $e) {
            //self::WriteError($e->getMessage(), $sqlstmt);
            exit($e->getMessage());
        }
    }

    static function GetUserByPersnr($personeelsNummer) {

        // ,(select loc_id from dbo.GetLocationParents(inv_loc_id,2)) pav_id,(select loc_naam from dbo.GetLocationParents(inv_loc_id,2)) pav_naam
        $sqlstmt = 'select distinct p.VPERSCOD,p.VPERSNAAM,p.VPERSVRNM, gebruiker.*
           
            
from orbis.dbo.BPRPERSO p 
JOIN orbis.dbo.BPRHCONT c
ON p.VPERSCOD = c.VPERSCOD
LEFT OUTER JOIN  gebruiker
ON p.VPERSCOD = gebruiker.geb_personeelscode
where (
(GETDATE() between  c.VHCONTRDATB and c.VHCONTRDATE
or c.VHCONTRDATE is null)
) and p.VPERSCOD = ?';


        try {
            $db = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();
            $stmt = $db->prepare($sqlstmt);
            $stmt->execute(array($personeelsNummer));
            $userRow = $stmt->fetchAll();
            if (count($userRow) == 0) {
                return null;
            } else {
                return $userRow[0];
            }
        } catch (\PDOException $e) {
            \clarafey\DAL\database\DB::writeError('gebruikerDAL', 'GetUserByPersnr', $personeelsNummer, $sqlstmt, " persNr = " . $personeelsNummer, $e->getMessage());
            exit($e->getMessage());
        }
    }

    static function getGebruikersRechten($personeelsNummer) {


        $sqlstmt = 'select distinct func_code,func_naam,func_cat,groepfunc_recht from rechten_functie,rechten_groepenfuncties,rechten_groepengebruikers,gebruiker
where rechten_functie.func_code = rechten_groepenfuncties.groepfunc_func_code
and rechten_groepengebruikers.groepgeb_groep_code = rechten_groepenfuncties.groepfunc_groep_code
and geb_id = rechten_groepengebruikers.groepgeb_geb_id and
  geb_personeelscode = ?';


        try {
            $db = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();
            $stmt = $db->prepare($sqlstmt);
            $stmt->execute(array($personeelsNummer));

            return $stmt->fetchAll();
        } catch (\PDOException $e) {
            \clarafey\DAL\database\DB::writeError('gebruikerDAL', 'getGebruikersRechten', $personeelsNummer, $sqlstmt, " persNr = " . $personeelsNummer, $e->getMessage());
            exit($e->getMessage());
        }
    }

    static function getGebruikersLocaties($personeelsNummer) {

        // ,(select loc_id from dbo.GetLocationParents(inv_loc_id,2)) pav_id,(select loc_naam from dbo.GetLocationParents(inv_loc_id,2)) pav_naam
        $sqlstmt = 'select 
 groep_code,groep_naam,loc_id,loc_naam from vw_gebruikergroep,rechten_groepenlocaties,locatie where groep_code
= groeploc_groep_code and loc_id = groeploc_loc_id
and geb_personeelscode = ?';


        try {
            $db = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();
            $stmt = $db->prepare($sqlstmt);
            $stmt->execute(array($personeelsNummer));
            return $stmt->fetchAll();
        } catch (\PDOException $e) {
            \clarafey\DAL\database\DB::writeError('gebruikerDAL', 'getGebruikersLocaties', $personeelsNummer, $sqlstmt, " persNr = " . $personeelsNummer, $e->getMessage());
            exit($e->getMessage());
        }
    }

}
