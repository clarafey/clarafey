<?php

namespace clarafey\DAL\kalenderDAL;

require_once 'dataBase.php';

use PDO;

class kalenderDAL {

    //om tegen tegaan dat deze klasse wordt geinstantieerd.  
    private function __construct() {
        
    }
static function DeleteKalenderReservatie($id)
{
    $sql = "";
    try
    {
        $conn = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();  
        $sql = "delete from kalender_reservatie where kalr_id = ?";
        $stmt = $conn->prepare($sql);
        $stmt->execute(array($id));
        
             return true;
    } 
 catch (\PDOException $e) {
            return false;
    }
}
    
    static function uiKalenderReservatie($gebruikerId, $id, $type, $titel, $omschrijving, $startdatum, $einddatum, $recurrenceRule, $recurrenceException,$recurrenceId,$isAllDay,$extras,$extrasDirectie,$attendees) {
        $sql = "";
        
         //  \clarafey\BO\functions\functions::writeTrace("uiKalenderReservatie DALDAL Start");
        try {
            if (empty($id))
                $id = null;
            //http://trentrichardson.com/2011/08/10/making-sense-of-stored-procedures-with-php-pdo-and-sqlsrv/
            $conn = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();
            $sql = "{:retval = CALL saveKalenderReservatie (@id=:id,@type=:type,@titel=:titel"
                    . ",@omschrijving=:omschrijving,@startdatum=:startdatum,@einddatum=:einddatum,@recurrenceRule=:recurrenceRule,@recurrenceId=:recurrenceId,@recurrenceException=:recurrenceException,@isAllDay=:isAllDay,@gebruikerId=:gebruikerId,@extras=:extras,@extrasDirectie=:extrasDirectie,@attendees=:attendees"
                    . ")}";

            $formattedStartDatum = null;
            if ($startdatum != null)
                $formattedStartDatum = $startdatum->format('Y-m-d H:i:s');
            $formattedEindDatum = null;
            if ($einddatum != null)
                $formattedEindDatum = $einddatum->format('Y-m-d H:i:s');
          
             
                        
 
            
            if (empty($isAllDay))
                $isAllDay = "0";
            else
                $isAllDay = "1";
            
            $stmt = $conn->prepare($sql);
            $stmt->bindParam('retval', $retval, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4);
            $stmt->bindParam('id', $id, PDO::PARAM_INT);
            $stmt->bindParam('type', $type, PDO::PARAM_INT);
            $stmt->bindParam('titel', $titel, PDO::PARAM_STR);
            $stmt->bindParam('omschrijving', $omschrijving, PDO::PARAM_STR);
            $stmt->bindParam('startdatum', $formattedStartDatum, PDO::PARAM_STR);
            $stmt->bindParam('einddatum', $formattedEindDatum, PDO::PARAM_STR);
            $stmt->bindParam('recurrenceRule', $recurrenceRule, PDO::PARAM_STR);
            $stmt->bindParam('recurrenceId', $recurrenceId, PDO::PARAM_INT);
            $stmt->bindParam('recurrenceException', $recurrenceException, PDO::PARAM_STR);
            $stmt->bindParam('isAllDay', $isAllDay, PDO::PARAM_STR);
            $stmt->bindParam('gebruikerId', $gebruikerId, PDO::PARAM_INT);
            $stmt->bindParam('extras', $extras, PDO::PARAM_STR);
            $stmt->bindParam('extrasDirectie', $extrasDirectie, PDO::PARAM_STR);     
            $stmt->bindParam('attendees', $attendees, PDO::PARAM_INT);
            $stmt->execute();
            $results = array();
            $stmt->closeCursor();
            return $retval;
        } catch (\PDOException $e) {

            \clarafey\DAL\database\DB::writeError('kalenderDAL', 'uiKalenderReservatie', $gebruikerId, $sql, "", $e->getMessage());
            return $e->getMessage();
        }
    }
    
    /*
     * Deze functie haalt alle reservaties op van de laatste 3 maanden en toekomst
     * Behalve de wederkerige opdrachten. Deze worden allemaal ingeladen
     */
     static function GetKalenderReservaties($campus,$categorie,$gebruikerId) {
        
        $paramsArray = array();  
        $sqlstmt = "select * from kalender_reservatie,kalender_reservatietype,gebruiker where kalrt_id= kalr_kalrt_id and kalr_geb_id_creator = geb_id and ((IsNull(kalr_recurrenceRule, '') = '' and kalr_einddatum > dateadd(m, -3, getdate()) or  IsNull(kalr_recurrenceRule, '') != ''))";
        $sqlstmt .= " and lower (kalrt_kalrtc_code) = ? ";
          $sqlstmt .= " and lower (kalrt_campus) = ? ";
        $params[] = strtolower($categorie);
             $params[] = strtolower($campus);
        if ($gebruikerId != null)
        {
              $sqlstmt .= " and kalr_geb_id_creator = ? ";
              $params[] = $gebruikerId;
        }
        
        $sqlstmt .= "order by kalr_startdatum";
        
     
        try {
            $db = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();
            $stmt = $db->prepare($sqlstmt);
            $stmt->execute($params);
            return $stmt->fetchAll();
        } catch (\PDOException $e) {
            //self::WriteError($e->getMessage(), $sqlstmt);
            exit("error ! " . $e->getMessage());
        }
    }

    static function GetReservatieType($campus,$categorie) {
        $sqlstmt = "select * from kalender_reservatietype "
                . "where lower (kalrt_kalrtc_code) = ? and lower (kalrt_campus) = ? "
                . "order by kalrt_naam";
        
      
        try {
            $db = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();
            $stmt = $db->prepare($sqlstmt);

            $stmt->execute(array(strtolower($categorie),strtolower($campus)));
             return $stmt->fetchAll();
        } catch (\PDOException $e) {
            //self::WriteError($e->getMessage(), $sqlstmt);
            exit("error ! " . $e->getMessage());
        }
    }
      static function GetReservatieTypeExtras($categorie,$adminOnly) {
        $sqlstmt = "select * from kalender_reservatietype_extras where 1=1 ";
        if ($adminOnly)  
             $sqlstmt .= " and kalrte_kalrtc_directie='Y'";
        else 
             $sqlstmt .= " and kalrte_kalrtc_directie='N'";
       $sqlstmt .= "and lower (kalrte_kalrtc_code) = ? order by kalrte_id";

        try {
            $db = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();
            $stmt = $db->prepare($sqlstmt);

            $stmt->execute(array(strtolower($categorie)));
            return $stmt->fetchAll();
        } catch (\PDOException $e) {
            
            exit("error ! " . $e->getMessage());
        }
    }

}
