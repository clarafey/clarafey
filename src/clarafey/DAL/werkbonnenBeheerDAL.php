<?php
namespace clarafey\DAL\werkbonnenBeheerDAL;

require_once 'dataBase.php';

use PDO;


class WerkbonnenBeheerDAL 
{
    //om tegen tegaan dat deze klasse wordt geinstantieerd.  
    private function __construct() {
    }

    //functie : uiWerkopdracht
    //doel : update en insert van een Werkopdracht, maw een Werkaanvraag of Werkbon
    //hoe ? : de de databankprocedure iuWerkopdracht wordt aangesproken.
    //resultaat : id (integer) of errormessage (String)
    static function saveWerkopdracht($gebruikerId, $id, $parentid, $prio_id, $wot_id, $titel, $beschrijving, $loc_id, $inv_id, $beginDatum, $eindDatum, $gebruikersKommaSep, $groepenKommaSep) 
    {
        $sql = "";
        $conn = "";
        try {
            if (empty($parentid))
                $parentid = null;
            $conn = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();
            $sql = "{:retval = CALL saveWerkopdracht ("
                    . "@id=:id"
                    . ",@parentid=:parentid"
                    . ",@gebruikerid=:gebruikerid"
                    . ",@wot_id=:wot_id"
                    . ",@loc_id=:loc_id"
                    . ",@titel=:titel"
                    . ",@beschrijving=:beschrijving"
                    . ",@prio_id=:prio_id"
                    . ",@begindatum=:begindatum"
                    . ",@einddatum=:einddatum"
                    . ",@inv_id=:inv_id"
                    . ",@gebruikers=:gebruikers"
                    . ",@groepen=:groepen"
                    . ")}";
            //Datums formatteren. php->sql
            $formattedBeginDatum = null;
            if ($beginDatum != null)
                $formattedBeginDatum = $beginDatum->format('Y-m-d H:i:s');
            $formattedEindDatum = null;
            if ($eindDatum != null)
                $formattedEindDatum = $eindDatum->format('Y-m-d H:i:s');
            $stmt = $conn->prepare($sql);
            $stmt->bindParam('retval', $retval, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4);
            $stmt->bindParam('id', $id, PDO::PARAM_INT);
            $stmt->bindParam('parentid', $parentid, PDO::PARAM_INT);
            $stmt->bindParam('gebruikerid', $gebruikerId, PDO::PARAM_INT);
            $stmt->bindParam('wot_id', $wot_id, PDO::PARAM_INT);
            $stmt->bindParam('loc_id', $loc_id, PDO::PARAM_INT);
            $stmt->bindParam('titel', $titel, PDO::PARAM_STR);
            $stmt->bindParam('beschrijving', $beschrijving, PDO::PARAM_STR);
           // $stmt->bindParam('woss_id', $woss_id, PDO::PARAM_INT);
            $stmt->bindParam('prio_id', $prio_id, PDO::PARAM_INT);
            $stmt->bindParam('begindatum', $formattedBeginDatum, PDO::PARAM_STR);
            $stmt->bindParam('einddatum', $formattedEindDatum, PDO::PARAM_STR);
            $p_inv_id = $inv_id;
            if (empty($inv_id))
                $p_inv_id = null;
            $stmt->bindParam('inv_id', $p_inv_id, PDO::PARAM_INT);
           // $stmt->bindParam('wos_id', $wos_id, PDO::PARAM_INT);
            $stmt->bindParam('gebruikers', $gebruikersKommaSep, PDO::PARAM_STR);
            $stmt->bindParam('groepen', $groepenKommaSep, PDO::PARAM_STR);
            /* Begin a transaction, turning off autocommit */
            $conn->beginTransaction();
            $stmt->execute();
            $results = array();
            $stmt->closeCursor();
            $conn->commit();
            return $retval;
        } catch (\PDOException $e) {
            //bij fout alles rollbacken and fout loggen
            $conn->rollback();
            \clarafey\DAL\database\DB::writeError('werkbonnenBeheerDAL', 'uiWerkopdracht', $gebruikerId, $sql, "", $e->getMessage());
            return $e->getMessage();
        }
    }
    
    
    //Functie saveWerkOpdrachtStatus
    //deze functie roept op de datababank de procedure 'saveWerkOpdrachtStatus' op.
    //deze procedure voegt wanneer de status wijzigt een nieuw record toe in de status-geschiedenis-tabel(tabel werkopdracht_status_historiek)
    //wanneer de status niet wijzigt wordt de opmerking geupdate.
    //Laatst gewijzigd : 28 maart 2015 GD
    //$werkOpdrachtId : de id van de werkopdracht (uit tabel werkopdracht)
    //$gebruikerId : de id van de gebruiker die de status wijzigd (uit tabel gebruiker)
    //de id van de nieuwe status (uit tabel werkopdracht_status)
    static function saveWerkOpdrachtStatus ($werkOpdrachtId, $gebruikerId, $statusId, $omschrijving)
    {   $sql = "";
        $conn = "";
        try {
          
            //database connectie ophalen 
            $conn = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();
            //sql commando opbouwen
            $sql = "{CALL saveWerkOpdrachtStatus ("
                    . "@wo_id=:wo_id"
                    . ",@wos_id=:wos_id"
                    . ",@geb_id=:geb_id"
                    . ",@opmerking=:opmerking"
                    . ")}";
           
            $stmt = $conn->prepare($sql);
            //parameters binden
            $stmt->bindParam('wo_id', $werkOpdrachtId, PDO::PARAM_INT);
            $stmt->bindParam('wos_id', $statusId, PDO::PARAM_INT);
            $stmt->bindParam('geb_id', $gebruikerId, PDO::PARAM_INT);
            $stmt->bindParam('opmerking', $omschrijving, PDO::PARAM_STR);

            $conn->beginTransaction();
            $stmt->execute();
            $results = array();
            $stmt->closeCursor();
            $conn->commit();
            return true;
        } catch (\PDOException $e) {
            //bij fout alles rollbacken and fout loggen
            $conn->rollback();
            \clarafey\DAL\database\DB::writeError('werkbonnenBeheerDAL', 'saveWerkBonStatus', $gebruikerId, $sql, "wo:" . $werkOpdrachtId . "  - statusid: " . $statusId . "  - gebruikerId: " . $gebruikerId, $e->getMessage());
            return $e->getMessage();
        }
    }
    //Deze functie laadt 1 of meerdere Werkaanvragen en/of Werkbonnen
    //Parameters :
    //id : wanneer 1 WA of WB moet geladen worden , de unieke Id meegeven
    //type : WA of WB of leeg voor beide
    //parentid : enkel de werkbonnen van een bepaalde werkaanvraag. de id van de WA meegeven
    //locaties : array van locatie-id's : enkele WA/WB van een bepaalde locatie (zie gebouwenbeheer)
    //groepCode : enkel WA/WB van een bepaalde gebruikersgroep bijvoorbeeld KKDIR -> Directie
    //subGroep : onderverdeling binnen een Groep. Wordt momenteel enkel gebruikt om de technischhe dienst op te splitsen in 'Bouw', 'Looodgieters',...
    static function getWerkOprachten($id, $type, $parentid, $locaties, $groepCode,$userId,$subGroep) {
        $paramsArray = array();
        if (!empty($id)) {
            //enkel werkopdrachten onder een bepaalde locatie
            $paramsArray[] = $id;
        }
        if (!empty($locaties)) {
            //enkel werkopdrachten onder een bepaalde locatie
            //array_map => maakt een array van alle id's van de locatie-object-array $locatieIds
            $locationIdsKommaSep = implode(',', (array_map(create_function('$o', 'return $o->getId();'), $locaties)));
            $paramsArray[] = "," . $locationIdsKommaSep . ",";
        } else {
            if (!empty($groepCode)) {
                //geen locatie-> enkel werkaanvragen van zijn eigen groep (directie,leefgroep,receptie,....)
                $paramsArray[] = $groepCode;
            }
        }
        if (!empty($parentid)) {
            //Enkel werkopdrachten (hoogste niveau) (geen werkbonnen)
            $paramsArray[] = $parentid;
        }
        if (!empty($userId)) {
            //Enkel werkopdrachten (hoogste niveau) (geen werkbonnen)
            $paramsArray[] = $userId;
        }
         if (!empty($subGroep)) {
            //Enkel werkopdrachten (hoogste niveau) (geen werkbonnen)
            $paramsArray[] = $subGroep;
        }
        $sqlstmt = "select werkopdracht.*,locatie.*,werkopdracht_type.* ,werkopdracht_prioriteit.*,inventaris.*,
             werkopdracht_status.*,werkopdracht_status_historiek.*,
      
           
modifier.geb_id as modifier_id,
modifier.geb_email as modifier_email,
modifier.geb_personeelscode as modifier_personeelscode,
modifier.geb_voornaam as modifier_voornaam,
modifier.geb_achternaam as modifier_achternaam,
creator.geb_id as creator_id,
creator.geb_email as creator_email,
creator.geb_personeelscode as creator_personeelscode,
creator.geb_voornaam as creator_voornaam,
creator.geb_achternaam as creator_achternaam
,loc_naam,groepgeb_groep_code,
(select concat(Path1,'@@', Path2,'@@', Path3,'@@', Path4,'@@', Path5) from  vw_locationtreepath   where vw_locationtreepath.loc_id = werkopdracht.wo_loc_id) as  'locatiepath',
(SELECT  CAST (wogeb_geb_id as varchar) + '@' + geb_voornaam + '@' + geb_achternaam + ',' AS 'data()'
FROM werkopdracht_gebruikers,gebruiker where  wogeb_wo_id = wo_id and  wogeb_geb_id = geb_id
FOR XML PATH('')) as gebruikers,
(SELECT  werkopdracht_groepen.wogroep_geb_subgroep  + ',' AS 'data()'
FROM werkopdracht_groepen where  werkopdracht_groepen.wogroep_wo_id = wo_id
FOR XML PATH('')) as gebruikersgroepen
from werkopdracht
left outer join locatie on loc_id = wo_loc_id
left outer join werkopdracht_type on wot_id = wo_wot_id
 
left outer join werkopdracht_prioriteit on wop_id = wo_wop_id 
left outer join gebruiker as modifier on modifier.geb_id = wo_geb_id_modificator
left outer join gebruiker as creator on creator.geb_id = wo_geb_id_creator
left outer join inventaris on wo_inv_id = inv_id
left join rechten_groepengebruikers on  rechten_groepengebruikers.groepgeb_geb_id = creator.geb_id
left join werkopdracht_status_historiek on wo_wosh_id = wosh_id
left join werkopdracht_status on wosh_wos_id = wos_id
where 1=1 ";
        if (!empty($id)) {
            //indien de ID wordt meegegeven wordt 1 werkaanvraag of werkbon opgehaald.
            $sqlstmt .= " and wo_id = ?";
        }
        if (!empty($locaties)) {
            //Enkel werkopdrachten (hoogste niveau)
            $sqlstmt .= " and wo_loc_id in (select loc_id from dbo.GetLocationTree(?))";
        } else {
            if (!empty($groepCode)) {
                //Enkel werkopdrachten (hoogste niveau)
                $sqlstmt .= " and groepgeb_groep_code = ?";
            }
        }
        if ($type == "WA") {
            //Enkel werkopdrachten (hoogste niveau)
            $sqlstmt .= " and wo_parent_id is null";
        }
        if ($type == "WB") {

            //Enkel werkbonnen (sun-niveau)
            $sqlstmt .= " and wo_parent_id is not null";
        }
        if (!empty($parentid)) {
            $sqlstmt .= " and wo_parent_id = ?";
        }
        
        if (!empty($userId) && !empty($subGroep))
        {
            $sqlstmt .= " and (? in (select  wogeb_geb_id from werkopdracht_gebruikers where wogeb_wo_id = werkopdracht.wo_id)";
            $sqlstmt .= "  or ? in (select  wogroep_geb_subgroep from werkopdracht_groepen where wogroep_wo_id = werkopdracht.wo_id))";
        }
        else{
            
        if (!empty($userId)) {
        {
            
            $sqlstmt .= " and ? in (select  wogeb_geb_id from werkopdracht_gebruikers where wogeb_wo_id = werkopdracht.wo_id)";
        }
            
        }
        
         if (!empty($subGroep)) {
            $sqlstmt .= "  and ? in (select  wogroep_geb_subgroep from werkopdracht_groepen where wogroep_wo_id = werkopdracht.wo_id)";
        }
    }
        $sqlstmt .= " order by wo_creatiedatum desc";
        try {
            $db = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();

            $stmt = $db->prepare($sqlstmt);
            $stmt->execute($paramsArray);
            return $stmt->fetchAll();
        } catch (Exception $e) {
            \clarafey\DAL\database\DB::writeError('werkbonnenBeheerDAL', 'GetWerkOprachten', null, $sqlstmt, "", $e->getMessage());
            exit("error ! " . $e->getMessage());
        }
    }

     //deze functie geeft van een WB alle toegewezen gebruikers terug
      static function getToegewezenGebruikers($wo_id) {

        $sqlstmt = "select geb_id,geb_voornaam,geb_achternaam from werkopdracht_gebruikers,gebruiker where wogeb_geb_id = geb_id"
                . " and wogeb_wo_id = ? order by geb_voornaam desc";
        try {
            $db = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();
            $stmt = $db->prepare($sqlstmt);

            $stmt->execute(array($wo_id));
            return $stmt->fetchAll();
        } catch (\PDOException $e) {
            //self::WriteError($e->getMessage(), $sqlstmt);
            exit("error ! " . $e->getMessage());
        }
    }
    
    //geeft de type werkaanvragen terug (herstelling, renovatie; investering,.....)
    static function getTypes() {

        $sqlstmt = "select wot_id,wot_naam from werkopdracht_type order by wot_ordernum asc";
        try {
            $db = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();
            $stmt = $db->prepare($sqlstmt);

            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_KEY_PAIR);
        } catch (\PDOException $e) {
            \clarafey\DAL\database\DB::writeError('werkbonnenBeheerDAL', 'GetTypes', null, $sqlstmt, "", $e->getMessage());
            
        }
    }

    //Deze functie geeft een lijst terug met de mogelijke prioriteiten van een werkopdracht (laag,hoog,...) 
    static function getPrioriteiten() {
        $sqlstmt = "select wop_id,wop_naam from werkopdracht_prioriteit order by wop_ordernum asc";
        try {
            $db = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();
            $stmt = $db->prepare($sqlstmt);

            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_KEY_PAIR);
        } catch (\PDOException $e) {
          \clarafey\DAL\database\DB::writeError('werkbonnenBeheerDAL', 'GetPrioriteiten', null, $sqlstmt, "", $e->getMessage());
            exit("error ! " . $e->getMessage());
        }
    }

    ///Geeft de mogelijk statussen terug voor een Werkopdracht
    ///de mogelijke statussen hangen af van soort Werkopdracht
    ///$type : 'WA' of 'WB'
    static function getWerkopdrachtStatussen($type) {
        $sqlstmt = "select wos_id,wos_naam from werkopdracht_status where wos_type = ? order by wos_ordernum asc";
        try {
            $db = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();
            $stmt = $db->prepare($sqlstmt);

            $stmt->execute(array($type));
            
            return $stmt->fetchAll(PDO::FETCH_KEY_PAIR);
        } catch (\PDOException $e) {
             \clarafey\DAL\database\DB::writeError('werkbonnenBeheerDAL', 'GetSupervisorStatussen', null, $sqlstmt, "", $e->getMessage());
            exit("error ! " . $e->getMessage());
        }
    }
     //Deze functie geeft de geschiedenis van de statuswijzigingen van een werkaanvraag of werkbon terug.
     static function getWerkopdrachtStatusHistoriek($woId) {
        $sqlstmt = "select wosh_datum,wos_id, wos_naam, wosh_opmerking,geb_id,geb_voornaam,geb_achternaam from werkopdracht_status_historiek, werkopdracht_status,
gebruiker
where wosh_wos_id = wos_id and wosh_geb_id = geb_id and wosh_wo_id = ? order by wosh_datum desc";
        try {
            $db = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();
            $stmt = $db->prepare($sqlstmt);

            $stmt->execute(array($woId));
            return $stmt->fetchAll();
        } catch (\PDOException $e) {
             \clarafey\DAL\database\DB::writeError('werkbonnenBeheerDAL', 'getWerkopdrachtStatussen', null, $sqlstmt, "", $e->getMessage());
            exit("error ! " . $e->getMessage());
        }
    }

    //deze functie delete een werkaanvraag op de databank
    //is enkel mogelijk wanneer de aanvraag de status 'Nieuwe aanvraag' heeft
   static function deleteWerkaanvraag ($id)
    { 
      
        $sql = "";
        $conn = "";
        try {
            if (empty($parentid))
                $parentid = null;
            $conn = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();
            $sql = "{CALL deleteWerkaanvraag ("
                    . "@wo_id=:wo_id"
                    . ")}";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam('wo_id', $id, PDO::PARAM_INT);
            $conn->beginTransaction();
            $stmt->execute();
            $results = array();
            $stmt->closeCursor();
            $conn->commit();
        
            return true;
        } catch (\PDOException $e) {
            //bij fout alles rollbacken and fout loggen
            $conn->rollback();
            \clarafey\DAL\database\DB::writeError('werkbonnenBeheerDAL', 'deleteWerkaanvraag', $id, $sql, "", $e->getMessage());
            return $e->getMessage();
        }
        
    }
    //deze functie delete een werkopdracht op de databank
    //is enkel mogelijk wanneer de aanvraag de status 'Nieuwe werkopdracht' heeft
    static function deleteWerkBon ($id)
    { 
        $sql = "";
        $conn = "";
        try {
            if (empty($parentid))
                $parentid = null;
            $conn = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();
            $sql = "{CALL deleteWerkBon ("
                    . "@wo_id=:wo_id"
                    . ")}";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam('wo_id', $id, PDO::PARAM_INT);
            $conn->beginTransaction();
            $stmt->execute();
            $results = array();
            $stmt->closeCursor();
            $conn->commit();
        
            return true;
        } catch (\PDOException $e) {
            //bij fout alles rollbacken and fout loggen
            $conn->rollback();
            \clarafey\DAL\database\DB::writeError('werkbonnenBeheerDAL', 'deleteWerkBon', $id, $sql, "", $e->getMessage());
            return $e->getMessage();
        }
        
    }
    
}
