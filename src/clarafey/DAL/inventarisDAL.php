<?php

namespace clarafey\DAL\inventarisDAL;

require_once 'dataBase.php';

use PDO;

class inventarisDAL {

    //om tegen tegaan dat deze klasse wordt geinstantieerd.  
    private function __construct() {
        
    }

    static function saveMerk($id, $naam) {
        $conn = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();
        $sql = "";
        if (empty($id)) {
            $sql = "insert into merk (mer_naam) values (:naam)";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam('naam', $naam, PDO::PARAM_STR);
        } else {
            $sql = "update merk set mer_naam=:naam where mer_id = :id";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam('id', $id, PDO::PARAM_INT);
            $stmt->bindParam('naam', $naam, PDO::PARAM_STR);
        }
        try {
            $stmt->execute();
            if (empty($id))
                $id = intval($conn->lastInsertId()); //komt als string binnen
            return $id;
        } catch (\PDOException $e) {


            \clarafey\DAL\database\DB::writeError('inventarisDAL', 'saveMerk', '', $sql, " id = " . $id . " naam = " . $naam, $e->getMessage());

            return $e->getMessage();
        }
    }

    static function saveLeverancier($id, $naam) {
        $conn = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();
        $sql = "";
        if (empty($id)) {
            $sql = "insert into leverancier (lev_naam) values (:naam)";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam('naam', $naam, PDO::PARAM_STR);
        } else {
            $sql = "update leverancier set lev_naam=:naam where lev_id = :id";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam('id', $id, PDO::PARAM_INT);
            $stmt->bindParam('naam', $naam, PDO::PARAM_STR);
        }
        try {
            $stmt->execute();
            if (empty($id))
                $id = intval($conn->lastInsertId()); //komt als string binnen
            return $id;
        } catch (\PDOException $e) {


            \clarafey\DAL\database\DB::writeError('inventarisDAL', 'saveLeverancier', '', $sql, " id = " . $id . " naam = " . $naam, $e->getMessage());

            return $e->getMessage();
        }
    }

    static function saveToestel($id, $naam, $cat) {

        $conn = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();
        $sql = "";
        if (empty($id)) {
            $sql = "insert into toestel (tst_naam,tst_cat) values (:naam,:cat)";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam('naam', $naam, PDO::PARAM_STR);
            $stmt->bindParam('cat', $cat, PDO::PARAM_STR);
        } else {
            $sql = "update toestel set tst_naam=:naam, tst_cat=:cat where tst_id = :id";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam('id', $id, PDO::PARAM_INT);
            $stmt->bindParam('naam', $naam, PDO::PARAM_STR);
            $stmt->bindParam('cat', $cat, PDO::PARAM_STR);
        }
        try {
            $stmt->execute();
            if (empty($id))
                $id = intval($conn->lastInsertId()); //komt als string binnen
            return $id;
        } catch (\PDOException $e) {


            \clarafey\DAL\database\DB::writeError('inventarisDAL', 'saveToestel', '', $sql, " id = " . $id . " naam = " . $naam . " cat = " . $cat, $e->getMessage());

            return $e->getMessage();
        }
    }

    static function uiInventaris($gebruikerId, $id, $nummer263, $produktOmschrijving, $serienummer, $macAdres, $ipAdres, $toestelId, $merkId, $locatieId, $leverancierId, $produktCode, $bestelbonNummer, $deviceLogin, $deviceWachtwoord, $uitDienstDatum, $aankoopDatum, $label, $opmerking) {

        $sql = "";
        try {
            if (empty($parentid))
                $parentid = null;
            //http://trentrichardson.com/2011/08/10/making-sense-of-stored-procedures-with-php-pdo-and-sqlsrv/
            $conn = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();
            $sql = "{:retval = CALL saveInventaris (@id=:id,@nummer263=:nummer263,@label=:label"
                    . ",@tst_id=:tst_id,@mer_id=:mer_id,@productcode=:productcode,@macadres=:macadres,@produktomschrijving=:produktomschrijving,@loc_id=:loc_id,@serienummer=:serienummer,@bestelbon=:bestelbon"
                    . ",@ipadres=:ipadres,@devicelogin=:devicelogin,@devicewachtwoord=:devicewachtwoord,@uitdienst=:uitdienst,@lev_id=:lev_id,@aankoopdatum=:aankoopdatum,@gebruikerid=:gebruikerid,@opmerking=:opmerking"
                    . ")}";

            $formattedUitDatum = null;
            if ($uitDienstDatum != null)
                $formattedUitDatum = $uitDienstDatum->format('Y-m-d H:i:s');
            $formattedAankoopDatum = null;
            if ($aankoopDatum != null)
                $formattedAankoopDatum = $aankoopDatum->format('Y-m-d H:i:s');
            $stmt = $conn->prepare($sql);
            $stmt->bindParam('retval', $retval, PDO::PARAM_INT | PDO::PARAM_INPUT_OUTPUT, 4);
            $stmt->bindParam('id', $id, PDO::PARAM_INT);
            $stmt->bindParam('nummer263', $nummer263, PDO::PARAM_STR);
            $stmt->bindParam('label', $label, PDO::PARAM_INT);
            $stmt->bindParam('tst_id', $toestelId, PDO::PARAM_INT);
            $stmt->bindParam('mer_id', $merkId, PDO::PARAM_INT);
            $stmt->bindParam('productcode', $produktCode, PDO::PARAM_STR);
            $stmt->bindParam('macadres', $macAdres, PDO::PARAM_STR);
            $stmt->bindParam('produktomschrijving', $produktOmschrijving, PDO::PARAM_STR);
            $stmt->bindParam('loc_id', $locatieId, PDO::PARAM_INT);
            $stmt->bindParam('serienummer', $serienummer, PDO::PARAM_STR);
            $stmt->bindParam('bestelbon', $bestelbonNummer, PDO::PARAM_STR);
            $stmt->bindParam('ipadres', $ipAdres, PDO::PARAM_STR);
            $stmt->bindParam('devicelogin', $deviceLogin, PDO::PARAM_STR);
            $stmt->bindParam('devicewachtwoord', $deviceWachtwoord, PDO::PARAM_STR);
            $stmt->bindParam('uitdienst', $formattedUitDatum, PDO::PARAM_STR);
            $stmt->bindParam('lev_id', $leverancierId, PDO::PARAM_STR);
            $stmt->bindParam('aankoopdatum', $formattedAankoopDatum, PDO::PARAM_STR);
            $stmt->bindParam('gebruikerid', $gebruikerId, PDO::PARAM_STR);
            $stmt->bindParam('opmerking', $opmerking, PDO::PARAM_STR);

            $stmt->execute();
            $results = array();
            $stmt->closeCursor();
            return $retval;
        } catch (\PDOException $e) {

            \clarafey\DAL\database\DB::writeError('inventarisDAL', 'uiInventaris', $gebruikerId, $sql, "", $e->getMessage());
            return $e->getMessage();
        }
    }

    static function GetInventarisItems($locatieId) {

        $sqlstmt = "select inventaris.*,mer_id,mer_naam,tst_id,tst_naam,lev_id,lev_naam,loc_id,loc_naam

,
(select concat(Path1,'@@', Path2,'@@', Path3,'@@', Path4,'@@', Path5) from  vw_locationtreepath   where vw_locationtreepath.loc_id = inventaris.inv_loc_id) as  'locatiepath'

from inventaris
left outer join merk on merk.mer_id = inventaris.inv_mer_id 
left outer join toestel on toestel.tst_id = inventaris.inv_tst_id
left outer join leverancier on leverancier.lev_id = inventaris.inv_lev_id
left outer join locatie on locatie.loc_id = inventaris.inv_loc_id
where inv_uitdienst is null and inv_loc_id = ?";
        
        $params = array();
        $params[] = $locatieId;
        $sqlstmt .= " order by inv_productomschrijving ";
        try {
            $db = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();
            $stmt = $db->prepare($sqlstmt);
            $stmt->execute($params);
            return $stmt->fetchAll();
        } catch (Exception $e) {
            self::WriteError($e->getMessage(), $sqlstmt);
            exit("error ! " . $e->getMessage());
        }
    }
    static function GetInventaris($nummer263, $zoektekst, $status) {
        $sqlstmt = "select (cast(UserDate_03  as Date)) as Factuurdatum,AssetGroup as Groep, Number as Serienummer,Description 'Omschrijving',Active as 'Actief',
 
ROUND(PurchasePrice,2) as 'Investering' ,inventaris.*,mer_id,mer_naam,tst_id,tst_naam,lev_id,lev_naam,loc_id,loc_naam

,
(select concat(Path1,'@@', Path2,'@@', Path3,'@@', Path4,'@@', Path5) from  vw_locationtreepath   where vw_locationtreepath.loc_id = inventaris.inv_loc_id) as  'locatiepath'

from [263].dbo.ItemNumbers
left outer join inventaris on  inventaris.inv_263_nummer  = Number 
left outer join merk on merk.mer_id = inventaris.inv_mer_id 
left outer join toestel on toestel.tst_id = inventaris.inv_tst_id
left outer join leverancier on leverancier.lev_id = inventaris.inv_lev_id
left outer join locatie on locatie.loc_id = inventaris.inv_loc_id

where 1=1 "; //AssetGroup='23105000'
        $params = array();
        if (!empty($nummer263)) {
            $sqlstmt .= " and Number = ?";
            $params[] = $nummer263;
        }

        if (!empty($zoektekst)) {
            $zoektekst = "%" . $zoektekst . "%";
            $sqlstmt .= " and (lower(Number) like lower(?) or lower(Description) like lower(?) or lower(inv_label) like lower(?))";
            $params[] = $zoektekst;
            $params[] = $zoektekst;
            $params[] = $zoektekst;
        }

        if (!empty($status)) {

            if ($status == "INV")
                $sqlstmt .= " and inv_id is not null";
            else
                $sqlstmt .= "and inv_id is null";
        }
        $sqlstmt .= " order by DateEnd desc,Number,Omschrijving ";
        try {
            $db = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();
            $stmt = $db->prepare($sqlstmt);
            $stmt->execute($params);
            return $stmt->fetchAll();
        } catch (Exception $e) {
            self::WriteError($e->getMessage(), $sqlstmt);
            exit("error ! " . $e->getMessage());
        }
    }

    //de goederen uit de Accountant/BoekhoudingsTabel (DB 263) halen
    /* static function GetInventaris($serieNummer) {

      $sqlstmt = "select Number as Serienummer,Description 'Omschrijving',Active as 'Actief',
      (cast(DateStart  as Date))  'Begindatum', (cast(DateEnd  as Date)) 'Einddatum',
      ROUND(PurchasePrice,2) as 'Investering' , ClaraFey.dbo.inventaris.*
      from ItemNumbers
      left outer join  ClaraFey.dbo.inventaris on  ClaraFey.dbo.inventaris.inv_263_nummer  =Number where Number = ?";


      try {
      $db = \clarafey\DAL\database\DB::getDatabaseSQLServer263();
      $stmt = $db->prepare($sqlstmt);
      $stmt->execute(array($serieNummer));

      return $stmt->fetchAll();
      } catch (Exception $e) {
      self::WriteError($e->getMessage(), $sqlstmt);
      exit("error ! " . $e->getMessage());
      }
      } */

    /* static function GetInventaris($serieNummer)
      {

      $sqlstmt = "select * from inventaris where inv_263_nummer = ?";


      try {
      $db = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();
      $stmt = $db->prepare($sqlstmt);
      $stmt->execute(array($serieNummer));
      return $stmt->fetchAll();
      } catch (Exception $e) {
      self::WriteError($e->getMessage(), $sqlstmt);
      exit("error ! " . $e->getMessage());
      }
      }
     */

    static function GetMerken() {
        $sqlstmt = "Select mer_id,mer_naam
FROM 
  merk 
ORDER BY
  case when mer_naam = 'andere' then 1 else 0 End,mer_naam";
        try {
            $db = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();
            $stmt = $db->prepare($sqlstmt);

            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_KEY_PAIR);
        } catch (\PDOException $e) {
            //self::WriteError($e->getMessage(), $sqlstmt);
            exit("error ! " . $e->getMessage());
        }
    }

    static function GetLeverancier() {
        $sqlstmt = "Select lev_id,lev_naam
FROM 
  leverancier 
ORDER BY
  case when lev_naam = 'andere' then 1 else 0 End,lev_naam";

        try {
            $db = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();
            $stmt = $db->prepare($sqlstmt);

            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_KEY_PAIR);
        } catch (\PDOException $e) {
            self::WriteError($e->getMessage(), $sqlstmt);
            exit("error ! " . $e->getMessage());
        }
    }

    static function GetToestellen() {
//+ ' (' + ISNULL(tst_cat,'') +  
        $sqlstmt = "Select tst_id,tst_naam,tst_cat
FROM 
  toestel 
ORDER BY
 tst_cat, case when tst_naam = 'andere' then 1 else 0 End, tst_naam";
        try {
            $db = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();
            $stmt = $db->prepare($sqlstmt);

            $stmt->execute();
            return $stmt->fetchAll();
        } catch (\PDOException $e) {
            // self::WriteError($e->getMessage(), $sqlstmt);
            exit("error ! " . $e->getMessage());
        }
    }

}
