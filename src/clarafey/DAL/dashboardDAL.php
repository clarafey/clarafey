<?php
namespace clarafey\DAL\dashboardDAL;

require_once 'dataBase.php';

use PDO;


class dashboardDAL 
{
    //om tegen tegaan dat deze klasse wordt geinstantieerd.  
    private function __construct() {
    }
   

    //Deze functie geeft de belangrijktste zaken betreft de verschillende modules terug
    //voor het dashboard
   static function getDashboardItems() {
        $sqlstmt = "with tmp(DataItem,kalr_extras) as (
select   cast (LEFT(kalr_extras, CHARINDEX(',',kalr_extras +',')-1)  as varchar(100)),
    STUFF(kalr_extras, 1, CHARINDEX(',',kalr_extras+','), '')
from kalender_reservatie where isnull(kalr_extras,'') != ''
and
  kalr_startdatum BETWEEN 
   CAST(GETDATE() AS DATE) AND DATEADD(DAY, 1, CAST(GETDATE() AS DATE))
   and kalr_extras is not null
union all
select cast (LEFT(kalr_extras, CHARINDEX(',',kalr_extras +',')-1) as varchar(100)),
    STUFF(kalr_extras, 1, CHARINDEX(',',kalr_extras +','), '')
from tmp
where isnull(DataItem,'') != '' and  isnull(kalr_extras,'') != ''
),

 tmp2(DataItem,kalr_extras_directie) as (
select   cast (LEFT(kalr_extras_directie, CHARINDEX(',',kalr_extras_directie +',')-1)  as varchar(100)),
    STUFF(kalr_extras_directie, 1, CHARINDEX(',',kalr_extras_directie+','), '')
from kalender_reservatie where isnull(kalr_extras_directie,'') != ''
and
  kalr_startdatum BETWEEN 
   CAST(GETDATE() AS DATE) AND DATEADD(DAY, 1, CAST(GETDATE() AS DATE))
   and kalr_extras_directie is not null
union all
select cast (LEFT(kalr_extras_directie, CHARINDEX(',',kalr_extras_directie +',')-1) as varchar(100)),
    STUFF(kalr_extras_directie, 1, CHARINDEX(',',kalr_extras_directie +','), '')
from tmp2
where isnull(DataItem,'') != '' and  isnull(kalr_extras_directie,'') != ''
)
select count(*) aantal ,kalrte_naam type, 'kalender' module
from tmp,kalender_reservatietype_extras
where DataItem =   kalender_reservatietype_extras.kalrte_id
group by DataItem,kalrte_naam
union
select count(*) aantal ,kalrte_naam type, 'kalender' module
from tmp2,kalender_reservatietype_extras
where DataItem =   kalender_reservatietype_extras.kalrte_id
group by DataItem,kalrte_naam
union
-- boekhouditems die nog niet geinventariseerd zijn
select count(*),'inventaris_open', 'inventaris' module from [263].dbo.ItemNumbers
left outer join inventaris on  inventaris.inv_263_nummer  = Number 
where inv_id is null 
-- werkaanvragen die nog niet zijn gelezen
select count(*),'werkaanvraag_ongelezen', 'werkbonnenbeheer' module from werkopdracht,werkopdracht_status_historiek where
wo_parent_id is null and wosh_id = 5 and wosh_wo_id = wo_id
union
--niet (goed)gekeurde aanvragen
select count(*),'werkaanvraag_keuring', 'werkbonnenbeheer' module from werkopdracht,werkopdracht_status_historiek where
wo_parent_id is null and wosh_id in (5,6) and wosh_wo_id = wo_id
 order by module";
        try {
            $db = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();
            $stmt = $db->prepare($sqlstmt);

            $stmt->execute();
            return $stmt->fetchAll();
        } catch (\PDOException $e) {
             \clarafey\DAL\database\DB::writeError('werkbonnenBeheerDL', 'GetWerkbonStatussen', null, $sqlstmt, "", $e->getMessage());
            exit("error ! " . $e->getMessage());
        }
    }
}
