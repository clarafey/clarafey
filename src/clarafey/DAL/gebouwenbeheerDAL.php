<?php

namespace clarafey\DAL\gebouwenbeheerDAL;

require_once 'dataBase.php';

use PDO;

class gebouwenbeheerDAL
{

    //om tegen tegaan dat deze klasse wordt geinstantieerd.  
    private function __construct()
    {

    }
    static function  GetClusterOfPaviljoen($locid)
    {

        $sqlstmt = "select * from dbo.GetLocationParents(?,2)"; // 2 voor level 2 
        
        try {
            $db = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();
            $stmt = $db->prepare($sqlstmt);
            $stmt->execute(array($locid));
            return $stmt->fetchAll();
        } catch (\PDOException $e) {
            //self::WriteError($e->getMessage(), $sqlstmt);
            exit("error ! " . $e->getMessage());
        }      
    }
    
    static function  deleteLocatie($id) {
        $sqlstmt = "delete from locatie where loc_id=?";
        try {
            $db = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();
            $stmt = $db->prepare($sqlstmt);
            $stmt->execute(array($id));
            return true;
        } catch (\PDOException $e) {
           // self::WriteError($e->getMessage(), $sqlstmt);
            return  $e->getMessage();
        }
         return false;
     }
    static function iuLocatie($id,$parentid, $naam,$beschrijving,$opmerking) {
        try {
            
             if (empty($id))
                $id = null;
            if (empty($parentid))
                $parentid = null;
            
            
            //http://trentrichardson.com/2011/08/10/making-sense-of-stored-procedures-with-php-pdo-and-sqlsrv/
            $conn = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();
            $sql = "{:retval = CALL saveLocatie (@id=:id,@naam=:naam,@beschrijving=:beschrijving,@opmerking=:opmerking,@parent_id=:parentid)}";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam('retval', $retval, PDO::PARAM_INT|PDO::PARAM_INPUT_OUTPUT, 4);
            $stmt->bindParam('id', $id, PDO::PARAM_INT);
            $stmt->bindParam('naam', $naam, PDO::PARAM_STR);
            $stmt->bindParam('beschrijving', $beschrijving, PDO::PARAM_STR);
            $stmt->bindParam('opmerking', $opmerking, PDO::PARAM_STR);
            $stmt->bindParam('parentid', $parentid, PDO::PARAM_INT);
            $stmt->execute();
            $results = array();
            $stmt->closeCursor();
            return $retval;
        } catch (\PDOException $e) {
            exit("error ! " . $e->getMessage());
        }
    } 
    static function GetLocatieTree()
    {
        $sqlstmt = "WITH tree (loc_id, loc_parent_id, Level, loc_naam,loc_beschrijving, rn) as 
(
   SELECT loc_id, loc_parent_id, 0 as Level, loc_naam,loc_beschrijving,
       RIGHT('00000' + convert(varchar(max),right(row_number() over (order by loc_id),10)),6) rn
   FROM locatie
   WHERE loc_parent_id is null

   UNION ALL

   SELECT c2.loc_id, c2.loc_parent_id, tree.level + 1, c2.loc_naam,c2.loc_beschrijving,
       rn + '/' + RIGHT('00000' + convert(varchar(max),right(row_number() over (order by tree.loc_id),10)) ,6)
   FROM locatie c2 
     INNER JOIN tree ON tree.loc_id = c2.loc_parent_id  
)
 select *
FROM tree
order by RN";


 
        try {
            $db = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();
            $stmt = $db->prepare($sqlstmt);
            $stmt->execute();
            return $stmt->fetchAll();
        } catch (Exception $e) {
            //self::WriteError($e->getMessage(), $sqlstmt);
            exit("error ! " . $e->getMessage());
        }
    }
    static function GetLocatie($id)
    {
        $sqlstmt = "select locatie.*,(select lp.loc_naam from locatie lp where lp.loc_id = locatie.loc_parent_id) parentnaam from locatie where loc_id = ?";
        try {
            $db = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();
            $stmt = $db->prepare($sqlstmt);
            $stmt->execute(array($id));
            return $stmt->fetchAll();
        } catch (\PDOException $e) {
            exit("error ! " . $e->getMessage());
        }
    }
    static function GetCampussen()
    {
        
        $sqlstmt = "select * from locatie where loc_parent_id is null order by loc_id asc";
       
        try {
            
            $db = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();
            
            $stmt = $db->prepare($sqlstmt);
            
            $stmt->execute();
            
            return $stmt->fetchAll();
            
        } catch (\PDOException $e) {
           
            exit("error ! " . $e->getMessage());
        }
    }
    static function GetLocaties()
    {

        $sqlstmt = "select loc_id,loc_naam from locatie";


        try {
            $db = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();
            $stmt = $db->prepare($sqlstmt);

            $stmt->execute();
             return $stmt->fetchAll(PDO::FETCH_KEY_PAIR);
        } catch (\PDOException $e) {
           // self::WriteError($e->getMessage(), $sqlstmt);
            exit("error ! " . $e->getMessage());
        }
    }

   
}
