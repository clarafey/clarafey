<?php

namespace clarafey\DAL\registrationDAL;

require_once 'dataBase.php';

use PDO;

class registrationDAL {

    //om tegen tegaan dat deze klasse wordt geinstantieerd.  
    private function __construct() {
        
    }
 



    static function updateAccount($gebruikersId, $password,$email) {
        try {
            
 
            $db = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();
            $stmt = $db->prepare('update gebruiker set geb_wachtwoord = ?,geb_email = ? where geb_id=?');
            $stmt->execute(array($password,$email,$gebruikersId));
            $affectedRows = $stmt->rowCount();
            if ($affectedRows == 1)
                return true;
            else
                return false;
        } catch (\PDOException $e) {
           return $e->getMessage();
        }
    }
    static function insertAccount($persNr, $password,$email,$voornaam,$achternaam) {
        try {
            
            
          
            $db = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();
            $stmt = $db->prepare('insert into gebruiker (geb_personeelscode,geb_wachtwoord,geb_email,geb_voornaam,geb_achternaam) values (?,?,?,?,?)');
            $stmt->execute(array($persNr, $password,$email,$voornaam,$achternaam));
           
            return $db->lastInsertId();
            
            } catch (\PDOException $e) {

            return $e->getMessage();
        }
    }

    
    static function SetLogonDetails($gebruikersId)
    {
        
          try {
            
           $cDate = new \DateTime('NOW');
           $formattedDate =  $cDate->format('Y-m-d H:i:s');
            $db = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();
            $stmt = $db->prepare('update gebruiker set geb_lastlogindatum = ? where geb_id = ?');
            $stmt->execute(array( $formattedDate, $gebruikersId));
           
            return $db->lastInsertId();
            
            } catch (\PDOException $e) {

            return $e->getMessage();
        }
    }
    
    static function GetUserByPersnr($personeelsNummer,$computerNaam) {
        $sqlstmt = 'select distinct p.VPERSCOD,p.VPERSNAAM,p.VPERSVRNM, gebruiker.* ,inv_loc_id 
            ,(select loc_id from dbo.GetLocationParents(inv_loc_id,2)) pav_id,(select loc_naam from dbo.GetLocationParents(inv_loc_id,2)) pav_naam
            
from orbis.dbo.BPRPERSO p 
JOIN orbis.dbo.BPRHCONT c
ON p.VPERSCOD = c.VPERSCOD
LEFT OUTER JOIN  gebruiker
ON p.VPERSCOD = gebruiker.geb_personeelscode
LEFT OUTER JOIN  inventaris
on ? = inv_label
 
where (
(GETDATE() between  c.VHCONTRDATB and c.VHCONTRDATE
or c.VHCONTRDATE is null)
) and p.VPERSCOD = ?';
        
      
        try {
            $db = \clarafey\DAL\database\DB::getDatabaseSQLServerClaraFey();
            $stmt = $db->prepare($sqlstmt);
            $stmt->execute(array($computerNaam,$personeelsNummer));
            $userRow = $stmt->fetchAll();
            if (count($userRow) == 0) {
                return null;
            } else {
                return $userRow[0];
            }
        } catch (\PDOException $e) {
            //self::WriteError($e->getMessage(), $sqlstmt);
            exit($e->getMessage());
        }
    }
}
