<?php
namespace clarafey\DAL\database;
use PDO;

 class DB {
   
    //de Personeelsdatabank Orbis
    static function getDatabaseSQLServerOrBis() {
        $db = new PDO( "sqlsrv:server=10.118.0.10 ; Database=orbis;","Gert","December2014" );
        return $db;
    }

    //Onze eigen ClaraFey-Databank
    static function getDatabaseSQLServerClaraFey() {
       $db = new PDO( 'sqlsrv:server=' . DB_HOST . ' ; Database='. DB_DB .';',DB_USER, DB_PASS );
       $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
       return $db;
    }
    //de databank voor onze Unit-Test
     static function getDatabaseSQLServerClaraFeyUnitTest() {
       $db = new PDO( 'sqlsrv:server=' . DB_HOST . ' ; Database='. DB_DB .';',DB_USER, DB_PASS );
       $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
       return $db;
    }
    //de Beokhoud-databank(Exact) Databank
    static function getDatabaseSQLServer263() {
       $db = new PDO( "sqlsrv:server=10.118.0.10 ; Database=263;","Gert","December2014" );
        return $db;
    }
    
    //deze functie schrijft een databank-fout weg in een log-bestand.
    static function writeError($namespace,$function,$personeelsNummer, $sql,$parameters,$errormessage)
    {   
        \file_put_contents('databaseErrorLog.txt',PHP_EOL.PHP_EOL."************ " .  date("Y-m-d H:i:s") .  " *** pers.nr : " . $personeelsNummer . " ***************",FILE_APPEND);
        \file_put_contents('databaseErrorLog.txt',PHP_EOL."Namespace : ".$namespace. "\tfunctie : ".$function,FILE_APPEND);
        \file_put_contents('databaseErrorLog.txt',PHP_EOL."Sql : ".$sql,FILE_APPEND);
        \file_put_contents('databaseErrorLog.txt',PHP_EOL."Parameters : ".$parameters,FILE_APPEND);
        \file_put_contents('databaseErrorLog.txt',PHP_EOL."Error : ".$errormessage,FILE_APPEND);
    }
    
}

